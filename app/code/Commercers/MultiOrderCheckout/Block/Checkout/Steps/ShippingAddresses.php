<?php

namespace Commercers\MultiOrderCheckout\Block\Checkout\Steps;

class ShippingAddresses extends \Commercers\MultiOrderCheckout\Block\Checkout\Steps
{
	public function getCountries(){
		$countries = $this->_country->toOptionArray();
		$allowedCountries = $this->_allowedCountries->getAllowedCountries();

		$options = [];
		foreach ($countries as $country) {
			if(in_array($country['value'],$allowedCountries)) {
				$options[] = [
					'optionValue' => $country['value'],
					'optionLabel' => $country['label']
				];
			}
		}

		return $options;
	}
}
