<?php

namespace Commercers\KlarnaPayment\Controller\Payment;
class AjaxExtraFee extends \Magento\Framework\App\Action\Action {

    /* @var $_cartModelFactory \Magento\Checkout\Model\CartFactory */
    protected $_cartModelFactory;

    /** @var \Commercers\KlarnaPayment\Helper\ConfigFactory */
    protected $_configKlarnaFactory;

    /** @var \Magento\Checkout\Model\SessionFactory */
    protected $_checkoutSessionFactory;

    /** @var \Magento\Checkout\Helper\CartFactory */
    protected $_cartFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\CartFactory $cartModelFactory,
        \Commercers\KlarnaPayment\Helper\ConfigFactory $configKlarnaFactory,
        \Magento\Checkout\Model\SessionFactory $checkoutSessionFactory,
        \Magento\Checkout\Helper\CartFactory $cartFactory
    ) {
        $this->_cartModelFactory = $cartModelFactory;
        $this->_configKlarnaFactory = $configKlarnaFactory;
        $this->_checkoutSessionFactory = $checkoutSessionFactory;
        $this->_cartFactory = $cartFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $paymentMethod = $params['paymentMethodActive'];
        // Check payment is klarna_payments;
        $methodPayment = $paymentMethod['method'];
        $checkMethodPayment   = 'klarna_pay';
        $resultCheck = false;
        $data['message'] = false;
        if(strpos($methodPayment, $checkMethodPayment) !== false){
            $resultCheck = true;
        }
        if ($resultCheck) {
           $this->order_add_begadi_fee_for_ratenkauf();
           $data['message'] = true;
        }else {
            $this->order_remove_begadi_fee_for_ratenkauf();
        }
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData($data);
        return $response;
    }

    public function order_add_begadi_fee_for_ratenkauf()
    {
        $cartHelper = $this->_cartFactory->create();
        $checkout = $this->_checkoutSessionFactory->create();
        $quote = $checkout->getQuote();

        $item = $this->_get_applied_fee_item($cartHelper);
        if ($item === null || $item->isDeleted()) {
            $total = $quote->getGrandTotal();
            $this->add_fee_to_cart_for_klarnaratenkauf($total);
        }
        return true;
    }

    public function order_remove_begadi_fee_for_ratenkauf()
    {
        $cartHelper = $this->_cartFactory->create();
        $item = $this->_get_applied_fee_item($cartHelper);
        if ($item !== null && !$item->isDeleted()) {
            $itemId = $item->getItemId();
                $cartHelper->getCart()->removeItem($itemId)->save();
        }
        return true;
    }

    public function _get_applied_fee_item($cartHelper)
    {
        $items = $cartHelper->getCart()->getItems();
        /* @var $_configKlarnaFactory Commercers\KlarnaPayment\Helper\Config */
        $configHelperKlarna = $this->_configKlarnaFactory->create();
        $products = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf();
        $product_array = explode(',', $products);
        foreach ($items as $item) {
            if (in_array($item->getProductId(), $product_array)) {
                return $item;
            }
        }
        return null;
    }

    public function add_fee_to_cart_for_klarnaratenkauf($total)
    {
        /* @var $_cartModelFactory \Magento\Checkout\Model\CartFactory */
        $cartModel = $this->_cartModelFactory->create();
        /* @var $_configKlarnaFactory Commercers\KlarnaPayment\Helper\Config */
        $configHelperKlarna = $this->_configKlarnaFactory->create();
        switch (true) {
            case ($total <= 100):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_hu();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 200):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_zw();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 300):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_dr();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 400):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_vi();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 500):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_fu();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 600):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_se();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 700):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_si();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 800):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_ac();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total <= 900):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_ne();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            case ($total > 900):
                $product = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf_ue();
                $cartModel->addProduct($product, 1);
                $cartModel->save();
                break;
            default:
                break;
        }
    }

}