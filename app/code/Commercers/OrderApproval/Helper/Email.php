<?php

namespace Commercers\OrderApproval\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order\Address\Renderer;

class Email extends AbstractHelper
{
    const XML_PATH_EMAIL_TEMPLATE_REMINDER_SUPERVISOR = 'orderapproval/approval_group/email_template_reminder_supervisor';

    const XML_PATH_EMAIL_TEMPLATE_SENT_BUYER = 'orderapproval/approval_group/email_send_buyer';

    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var UrlInterface
     */
    protected $_url;

    protected $_template;

    protected $_messageManager;

    protected $_urlInterface;

    /**
     * \Magento\Store\Model\App\Emulation
     */
    protected $_emulation;

    /**
     *  \Magento\Payment\Helper\DataFactory
     */
    protected $_helperPaymentFactory;

    protected $_ruleFactory;

    protected $_dataFactory;

    protected $_supervisorFactory;

    protected $orderRepository;

    protected $_order;

    /**
     * @var \Magento\Payment\Helper\Data
     */
    private $paymentHelper;

    /**
     * @var Renderer
     */
    protected $addressRenderer;
    protected $identityContainer;
    public function __construct(
        Renderer $addressRenderer,
        \Magento\Sales\Model\Order\Email\Container\CreditmemoIdentity $identityContainer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory,
        \Magento\Payment\Helper\DataFactory $helperPaymentFactory,
        \Magento\Store\Model\App\Emulation $emulationm,
        Context $context,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        UrlInterface $url,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        parent::__construct($context);
        $this->addressRenderer = $addressRenderer;
        $this->identityContainer = $identityContainer;
        $this->paymentHelper = $paymentHelper;
        $this->_order = $order;
        $this->orderRepository = $orderRepository;
        $this->_dataFactory = $dataFactory;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_helperPaymentFactory = $helperPaymentFactory;
        $this->_emulation = $emulationm;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->_url = $url;
        $this->_urlInterface = $urlInterface;
    }

    protected function getConfigValue($path,$storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    protected function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath,$this->_storeManager->getStore()->getId());
    }

    protected function generateTemplate($order,$orderApproval,$senderInfo,$receiverInfo,$supervisorName)
    {
        $storeId = $order->getStore()->getStoreId();
        $ruleId = $orderApproval['approval_rule_id'];
        if($ruleId){
            $orderApproval['approve_link'] =  $this->_urlInterface->getUrl('orderapproval/index/approve',
                [
                    '_store' => $storeId,
                    'approval_code' => $orderApproval['approval_code'],
                    'supervisor_name' => $supervisorName
                ]);
            $orderApproval['disapprove_link'] =  $this->_urlInterface->getUrl('orderapproval/index/disapprove',
                [
                    '_store' => $storeId,
                    'supervisor_name' => $supervisorName
                ]);
        }
        $approvalName = '';
        $typeEmail = true;
        if($orderApproval["approval_type"] == 2){
            $typeEmail = false;
        }
        if($orderApproval["approval_name"]){
            $approvalName = $orderApproval["approval_name"];
        }
        $statusItem = 'approved';
        $isApproved = true;
        if($orderApproval["status"] == 2){
            $statusItem = 'disapproved';
            $isApproved = false;
        }
        $template = $this->_transportBuilder->setTemplateIdentifier($this->_template)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->_storeManager->getStore()->getId()
                ]
            )
            ->setTemplateVars([
                'type_email' => $typeEmail,
                'order' => $order->getData(),
                'supervisor_name' =>$supervisorName,
                'approval_name' => $approvalName,
                'order_item_name' => $orderApproval["order_item_name"],
                'buyer_name' => $receiverInfo['name'],
                'status_item' => $statusItem,
                'is_approved' => $isApproved,
                'data' => $orderApproval,
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order,$storeId),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'shippingDescription' => $this->getShippingDescription($order),
                'created_at_formatted' => $order->getCreatedAtFormatted(1)
            ])
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'],$receiverInfo['name'])
            ->setReplyTo($senderInfo['email'],$senderInfo['name']);

        $approvalModel = $this->_dataFactory->create()->getCollection()
            ->addFieldToFilter('approval_code', array('eq' => $orderApproval['approval_code']));
        $approval = $approvalModel->getFirstItem();
        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
        $approval->save();
        //Set last send reminder at
        return $template;
    }

    public function sendEmail($template,$emailTemplateVariables,$order,$typeEmail,$supervisorName,$supervisorEmail)
    {
        if(!empty($emailTemplateVariables['approval_email'])){
            $this->_template = $this->getTemplateId($template);
            $this->_inlineTranslation->suspend();
            $senderInfo = [
                'email' => $this->getConfigValue('trans_email/ident_general/email',$this->_storeManager->getStore()->getId()),
                'name' => $this->getConfigValue('trans_email/ident_general/name',$this->_storeManager->getStore()->getId())
            ];
            if($typeEmail == 1){
                $receiverInfo = [
                    'email' => $emailTemplateVariables['buyer_email'],
                    'name' => $emailTemplateVariables['buyer_name']
                ];
            }elseif ($typeEmail == 2){
                $receiverInfo = [
                    'email' => $supervisorEmail,
                    'name' => $supervisorName
                ];
            }

            $this->generateTemplate($order,$emailTemplateVariables,$senderInfo,$receiverInfo,$supervisorName);
            try {
                $transport = $this->_transportBuilder->getTransport();
                $transport->sendMessage();
            } catch (\Exception $e) {
                echo $e->getMessage();exit;
            }
            $this->_inlineTranslation->resume();
        }
    }

    public function sendBuyerEmail($emailTempVariables,$order,$typeEmail,$supervisorName)
    {
        $this->sendEmail(self::XML_PATH_EMAIL_TEMPLATE_SENT_BUYER, $emailTempVariables,$order,$typeEmail,$supervisorName,$supervisorEmail=null);
    }

    public function sendReminderEmail($emailTempVariables,$order,$typeEmail,$supervisorName,$supervisorEmail)
    {
        $this->sendEmail(self::XML_PATH_EMAIL_TEMPLATE_REMINDER_SUPERVISOR, $emailTempVariables,$order,$typeEmail,$supervisorName,$supervisorEmail);
    }

    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    private function getPaymentHtml(\Magento\Sales\Api\Data\OrderInterface $order,$storeId)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $storeId
        );
    }


    private function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    private function getShippingDescription($order)
    {
        return $order->getShippingDescription();
    }


    private function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(),'html');
    }
}
