<?php

namespace Commercers\OrderApproval\Block\Email;


class Items extends \Magento\Framework\View\Element\Template{

    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $_order;

    protected $_urlInterface;

    protected $_dataFactory;

    protected $_orderRepository;

    protected $_productRepository;

    protected $_productFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\View\Element\Template\Context $context
    )
    {
        parent::__construct($context);
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_orderRepository = $orderRepository;
        $this->_dataFactory = $dataFactory;
        $this->_order = $order;
        $this->_urlInterface = $urlInterface;
    }

    public function orderModel($orderId)
    {
        $order = $this->_order->load($orderId);
        return $order->getData();
    }

    public function getItemsNeedApprovalInOrder($orderId)
    {
        $order = $this->_orderRepository->get($orderId);
        foreach ($order->getAllItems() as $item) {
            $productId = $item->getProductId();
            $orderApprovalRuleProductId = $this->getOrderApprovalRuleProductById($productId);
            if($orderApprovalRuleProductId){
                return true;
            }
        }
        return false;
    }

    public function getOrderApprovalRuleProductById($id)
    {
        $_product = $this->_productFactory->create()->load($id);
        // return $_product->getOrderApprovalRule();

        $storeId = 0; //Store ID
        $attributeValue = $_product->getResource()->getAttributeRawValue($_product->getId(),'order_approval_rule',$storeId); //change attribute_code
        return $attributeValue;
    }
    public function getUrlApproval($url,$arr = [])
    {
        return $this->_urlInterface->getUrl($url,$arr);
    }

    public function getQtyItemOrder($orderId,$orderItemId){
        $order = $this->_order->load($orderId);
        $orderItems = $order->getAllItems();
        $qty = 0;
        foreach ($orderItems as $item)
        {
            if($orderItemId == $item["item_id"]){
                $qty = $qty + $item["qty_ordered"];
                return $qty;
            }
        }
        return $qty;
    }
}
