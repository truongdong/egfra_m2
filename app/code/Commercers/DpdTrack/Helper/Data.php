<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/24/18
 * Time: 21:14
 */
namespace Commercers\DpdTrack\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;


class Data extends AbstractHelper {

	public function isEnabled()
	{
		return $this->getConfigValue('dpdtrack/general/enabled');
	}

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getOrderConfigStatus($statusCode = null){

		return $this->getConfigValue('dpdtrack/dpd_statuses/'.strtolower($statusCode));

	}

}