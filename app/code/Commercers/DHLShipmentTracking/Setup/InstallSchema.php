<?php

namespace Commercers\DHLShipmentTracking\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

	/**
	 * Installs DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		$data[] = ['status' => 'ZU', 'label' => 'COMPLETE - Delivered'];
		$data[] = ['status' => 'PO', 'label' => 'COMPLETE - In Delivery'];
		$data[] = ['status' => 'EE', 'label' => 'COMPLETE - Destination parcel center'];
		$data[] = ['status' => 'AE', 'label' => 'COMPLETE - Collection successful'];
		$data[] = ['status' => 'AA', 'label' => 'COMPLETE - Transport to the start parcel center'];
		$data[] = ['status' => 'ES', 'label' => 'COMPLETE - Delivery to PACKSTATION'];

		$setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

		$setup->getConnection()->insertArray(
			$setup->getTable('sales_order_status_state'),
			['status', 'state', 'is_default','visible_on_front'],
			[
				['ZU','complete', '0', '1'],
				['PO', 'complete', '0', '1'],
				['EE', 'complete', '0', '1'],
				['AE', 'complete', '0', '1'],
				['AA', 'complete', '0', '1'],
				['ES', 'complete', '0', '1']
			]
		);

		$installer->endSetup();
	}
}