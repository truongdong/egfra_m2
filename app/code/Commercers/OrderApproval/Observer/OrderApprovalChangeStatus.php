<?php
namespace Commercers\OrderApproval\Observer;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
class OrderApprovalChangeStatus implements ObserverInterface
{
    const XML_STATUS_ORDER_WAITING_FOR_APPROVAL = 'orderapproval/approval_group/orderstatus_waitingforapprove';
    const XML_STATUS_ORDER_APPROVED = 'orderapproval/approval_group/orderstatus_approved';
    const XML_STATUS_ORDER_DISAPPROVED = 'orderapproval/approval_group/orderstatus_disapproved';

    const XML_PATH_EMAIL_ENABLED = 'sales_email/order/enabled';

    protected $_request;
    protected $_layout;
    protected $_objectManager = null;
    protected $_customerGroup;
    private $logger;
    protected $_customerRepositoryInterface;

    protected $_helperOrderApprovalFactory;
    protected $_ruleFactory;
    protected $_dataFactory;

    protected $_orderFactory;
    protected $_orderItemFactory;
    protected $_supervisorFactory;
    protected $scopeConfig;
    protected $quoteItem;
    protected $orderItem;
    protected $quoteToOrder;
    protected $_dataCollectionFactory;
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */

    protected $_orderCommentSenderFactory;

    protected $_orderRepository;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;
    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;


    protected $creditmemoItemCreationFactory;

    /**
     * @var \Magento\Sales\Api\RefundOrderInterface
     */
    protected $refundOrder;

    protected $_creditmemoLoaderFactory;

    protected $_creditmemoManagementInterfaceFactory;

    protected $_invoiceRepositoryInterfaceFactory;

    protected $_stateFramework;

    protected $_orderManagementInterface;

    protected $_registry;

    protected $_helperEmailFactory;

    public function __construct(
        \Commercers\OrderApproval\Helper\EmailFactory $helperEmailFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Api\OrderManagementInterface $orderManagementInterface,
        \Magento\Sales\Api\CreditmemoManagementInterfaceFactory $creditmemoManagementInterfaceFactory,
        \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoaderFactory $creditmemoLoaderFactory,
        \Magento\Sales\Api\RefundOrderInterface $refundOrder,
        \Magento\Sales\Api\Data\CreditmemoItemCreationInterfaceFactory $creditmemoItemCreationFactory,
        \Magento\Sales\Api\InvoiceRepositoryInterfaceFactory $invoiceRepositoryInterfaceFactory,
        InvoiceSender $invoiceSender,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Magento\Quote\Model\Quote\Item\ToOrderItem $quoteToOrder,
        \Magento\Quote\Model\Quote\Item $quoteItem,
        \Magento\Sales\Model\Order\Item $orderItem,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderCommentSenderFactory $orderCommentSenderFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        EventManager $eventManager,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Magento\Sales\Model\Order\ItemFactory $orderItemFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory,
        \Commercers\OrderApproval\Helper\DataFactory $helperOrderApprovalFactory,
        \Magento\Framework\View\Element\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ){
        $this->_helperEmailFactory = $helperEmailFactory;
        $this->_registry = $registry;
        $this->_orderManagementInterface = $orderManagementInterface;
        $this->_creditmemoManagementInterfaceFactory = $creditmemoManagementInterfaceFactory;
        $this->_creditmemoLoaderFactory = $creditmemoLoaderFactory;
        $this->refundOrder = $refundOrder;
        $this->creditmemoItemCreationFactory = $creditmemoItemCreationFactory;
        $this->_invoiceRepositoryInterfaceFactory = $invoiceRepositoryInterfaceFactory;
        $this->invoiceSender = $invoiceSender;
        $this->_transaction = $transaction;
        $this->_invoiceService = $invoiceService;
        $this->_orderRepository = $orderRepository;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->quoteToOrder = $quoteToOrder;
        $this->quoteItem=$quoteItem;
        $this->orderItem=$orderItem;
        $this->quoteFactory = $quoteFactory;
        $this->_orderCommentSenderFactory = $orderCommentSenderFactory;
        $this->scopeConfig = $scopeConfig;
        $this->eventManager = $eventManager;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_orderItemFactory = $orderItemFactory;
        $this->_orderFactory = $orderFactory;
        $this->_dataFactory = $dataFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_helperOrderApprovalFactory = $helperOrderApprovalFactory;
        $this->_layout = $context->getLayout();
        $this->_request = $context->getRequest();
        $this->_objectManager = $objectManager;
        $this->logger = $logger;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */

    public function execute(EventObserver $observer)
    {
        $helperOrderApproval = $this->_helperOrderApprovalFactory->create();
        $supervisorName = $observer->getEvent()->getData('supervisor_name');
        $orderApproval = $observer->getEvent()->getData('order_approval');
        $orderApprovalRule = $this->_ruleFactory->create();
        $orderApprovalRule->load($orderApproval['approval_rule_id']);
        $status = $orderApproval->getStatus();
        $dataModel = $this->_dataFactory->create();
        $getOrderApprovalId = $orderApproval->getOrderId();
        $getOrderApprovalEmail = $orderApproval->getApprovalEmail();
        $orderItemModel = $this->_orderItemFactory->create();
        $quoteModel = $this->quoteFactory->create();
        if ($status == $dataModel::ORDER_APPROVAL_STATUS_APPROVED) {
            $orderModel = $this->_orderFactory->create();
            $order = $orderModel->load($orderApproval->getOrderId());

            $orderStatus = $this->getOrderStatusForState('processing_waiting_for_approval');
            $order->setStatus($orderStatus)->save();
            if($order){
                $storeId = $order->getStore()->getStoreId();
                if (!$helperOrderApproval->isEnabled($storeId)){
                    return;
                }
                $orderItemId = $orderApproval->getOrderItemId();

                $orderItem = $orderItemModel->load($orderItemId);
                if (!$orderItem->getId()) {
                    return;
                }
                // START Create Invoice for item approved
                if($order->canInvoice()) {
                    $itemsArray = [$orderItem->getItemId()=>$orderItem->getQtyOrdered()]; //here 80 is order item id and 2 is it's quantity to be invoice
                    $invoice = $this->_invoiceService->prepareInvoice($order, $itemsArray);
                    $invoice->register();
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();
                    $this->invoiceSender->send($invoice);
                    //send notification code
                    $order->addStatusHistoryComment(
                        __('Notified customer about invoice #%1.', $invoice->getId())
                    )
                        ->setIsCustomerNotified(true)
                        ->save();
                }
                // END Create Invoice for item approved

                // Start set comment history
                $approverName = $orderApprovalRule->getApproverName();
                if($supervisorId = $orderApproval->getSupervisorId()){
                    $supervisor = $this->_supervisorFactory->create()->load($supervisorId);
                    $approverName = $supervisor->getName();
                }
                $shippingMethod = $orderApproval->getShippingMethod();
                if ($shippingMethod) {
                    $orderComment = __('Order %1 is approved by %2 for SKU %3 and Shipping method : %4', $order->getIncrementId(), $approverName, $orderItem->getSku(), $shippingMethod);
                } else {
                    $orderComment = __('Order %1 is approved by %2 for SKU %3', $order->getIncrementId(), $approverName, $orderItem->getSku());
                }
                $order->addStatusHistoryComment($orderComment)->setIsVisibleOnFront(true)->setIsCustomerNotified(true)->save();
                // End set comment history

                //Start Sent email for buyer
                $approvalItem = $orderApproval->getData();
                $helperEmail = $this->_helperEmailFactory->create();
                $helperEmail->sendBuyerEmail($approvalItem,$order,$typeEmail = 1,$supervisorName);
                //End Sent email for buyer

            }
        } elseif ($status == $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED){
            $orderId = $orderApproval->getOrderId();//order id = 100
            $order = $this->_orderFactory->create()->load($orderId);
            // Set status default is WAITING FOR APPROVAL
            // Check Item order is disapprove and remove it out order
            $idItemInvoice = $orderApproval['order_item_id'];
            $orderItemInvoice = $orderItemModel->load($idItemInvoice);
            $productIdItemInvoice = $orderItemInvoice->getProductId();

            $lastOrderApprove = false;
            $shippingAmount = 0;
            $orderApprovalCollectionStatusDisapprove = $this->getOrderApprovalCollectionStatusDisapprove($dataModel,$orderId);
            if($orderApprovalCollectionStatusDisapprove == intval($order->getTotalItemCount())){
                $lastOrderApprove = true;
                $shippingAmount = null;
            }
            try {
                if($order->canInvoice()) {
                    $itemsArray = [$orderItemInvoice->getItemId()=>floatval($orderItemInvoice->getQtyOrdered())]; //here 80 is order item id and 2 is it's quantity to be invoice
                    $invoice = $this->_invoiceService->prepareInvoice($order, $itemsArray);
                    if(!$lastOrderApprove){
                        $shippingTaxAmount = $order->getShippingTaxAmount();
                        $baseShippingTaxAmount = $order->getBaseShippingTaxAmount();
                        $invoice->setGrandTotal($invoice->getSubtotalInclTax());
                        $invoice->setBaseGrandTotal($invoice->getBaseSubtotalInclTax());
                        $invoice->setTaxAmount($invoice->getTaxAmount() - $shippingTaxAmount);
                        $invoice->setBaseTaxAmount($invoice->getBaseTaxAmount() - $baseShippingTaxAmount);
                        $invoice->setShippingAmount(0);
                        $invoice->setBaseShippingAmount(0);
                        $invoice->setShippingTaxAmount(0);
                        $invoice->setBaseShippingTaxAmount(0);
                        $invoice->setShippingInclTax(0);
                        $invoice->setBaseShippingInclTax(0);
                    }
                    $invoice->register();
                    $invoice->getOrder()->setIsInProcess(true);
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();

                    //Create Credit Memo
                    $invoiceId = $invoice->getId();
                    $invoice = $this->_invoiceRepositoryInterfaceFactory->create()->get($invoiceId);
                    $invoiceItems = $invoice->getItems();
                    $items = [];
                    $orderId = $invoice->getOrderId();

                    foreach ($invoiceItems as $invoiceItem) {
                        if($invoiceItem->getOrderItem()->getParentItem()){
                            continue;
                        }
                        $items[$invoiceItem["order_item_id"]] = $invoiceItem["qty"];
                    }
                    $dataCreditMemo = array(
                        'items' => $items,
                        'do_offline' => true,
                        'comment_text' => '',
                        'shipping_amount' => $shippingAmount,
                        'adjustment_positive' => 0,
                        'adjustment_negative' => 0
                    );
                    $creditmemoLoader = $this->_creditmemoLoaderFactory->create();
                    $creditmemoLoader->setOrderId($orderId);
                    $creditmemoLoader->setCreditmemoId(null);
                    $creditmemoLoader->setCreditmemo($dataCreditMemo);
                    $creditmemoLoader->setInvoiceId($invoiceId);

                    $creditmemo = $creditmemoLoader->load();
                    if ($creditmemo) {
                        $doOffline = isset($dataCreditMemo['do_offline']) ? (bool)$dataCreditMemo['do_offline'] : false;
                        $this->_creditmemoManagementInterfaceFactory->create()->refund($creditmemo, $doOffline);
                        $this->_registry->unregister('current_creditmemo');
                    }
                }
            } catch (\Exception $e) {
                echo $e->getMessage();exit;
            }

            // Start set comment history
            $orderItemId = $orderApproval->getOrderItemId();
            $orderItem = $orderItemModel->load($orderItemId);
            $approverName = $orderApprovalRule->getApproverName();
            if($supervisorId = $orderApproval->getSupervisorId()){
                $supervisor = $this->_supervisorFactory->create()->load($supervisorId);
                $approverName = $supervisor->getName();
            }
            $shippingMethod = $orderApproval->getShippingMethod();
            if ($shippingMethod) {
                $orderComment = __('Order %1 is disapproved by %2 for SKU %3 and Shipping method : %4', $order->getIncrementId(), $approverName, $orderItem->getSku(), $shippingMethod);
            } else {
                $orderComment = __('Order %1 is disapproved by %2 for SKU %3', $order->getIncrementId(), $approverName, $orderItem->getSku());
            }
            $order->addStatusHistoryComment($orderComment)->setIsVisibleOnFront(true)->setIsCustomerNotified(true)->save();
            // End set comment history

            //Start Sent email for buyer
            $approvalItem = $orderApproval->getData();
            $helperEmail = $this->_helperEmailFactory->create();
            $helperEmail->sendBuyerEmail($approvalItem,$order,$typeEmail = 1,$supervisorName);
            //End Sent email for buyer

            if($lastOrderApprove){
                return;
            }
        }
//             START Set status order
        $orderId = $orderApproval->getOrderId();//order id = 100
        $order = $this->_orderFactory->create()->load($orderId);

        $orderApprovalCollectionStatusInit = $this->getOrderApprovalCollectionStatusInit($dataModel,$getOrderApprovalId);
        if (empty($orderApprovalCollectionStatusInit)) {
            $orderApprovalCollectionStatusNormals = $this->getOrderApprovalCollectionStatusNormal($dataModel,$getOrderApprovalId);
            foreach ($orderApprovalCollectionStatusNormals as $orderApprovalCollectionStatusNormal){
                $orderModel = $this->_orderFactory->create();
                $orderId = $orderApprovalCollectionStatusNormal["order_id"];//order id for which want to create invoice
                $order = $orderModel->load($orderId);
                $idItemInvoice = $orderApprovalCollectionStatusNormal['order_item_id'];
                $orderItemInvoice = $orderItemModel->load($idItemInvoice);
                if($order->canInvoice()) {
                    $itemsArray = [$orderItemInvoice->getItemId()=>$orderItemInvoice->getQtyOrdered()]; //here 80 is order item id and 2 is it's quantity to be invoice
                    $invoice = $this->_invoiceService->prepareInvoice($order, $itemsArray);
                    $invoice->register();
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();
                    $this->invoiceSender->send($invoice);
                    //send notification code
                    $order->addStatusHistoryComment(
                        __('Notified customer about invoice #%1.', $invoice->getId())
                    )
                        ->setIsCustomerNotified(true)
                        ->save();
                }
            }
            $orderStatus = $this->getOrderStatusForState('approved');
            $order->setStatus($orderStatus)->save();
            $order->addStatusHistoryComment(
                __('Notified status of order')
            )
                ->setIsCustomerNotified(true)
                ->save();
        }else{
            $orderStatus = $this->getOrderStatusForState('processing_waiting_for_approval');
            $order->setStatus($orderStatus)->save();
            $order->addStatusHistoryComment(
                __('Notified status of order')
            )
                ->setIsCustomerNotified(true)
                ->save();
        }
    }
    public function getOrderApprovalCollectionStatusDisapprove($dataModel,$getOrderApprovalId)
    {
        $orderApprovalCollection = $dataModel->getCollection()
            ->addFieldToFilter('order_id', array('eq' => $getOrderApprovalId))
            ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED))
        ;
        return $orderApprovalCollection->getSize();
    }
    public function getOrderApprovalCollectionStatusInit($dataModel,$getOrderApprovalId)
    {
        $orderApprovalCollection = $dataModel->getCollection()
            ->addFieldToFilter('order_id', array('eq' => $getOrderApprovalId))
            ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_INIT))
        ;
        return $orderApprovalCollection->getData();
    }
    public function getOrderApprovalCollectionStatusNormal($dataModel,$getOrderApprovalId)
    {
        $orderApprovalCollection = $dataModel->getCollection()
            ->addFieldToFilter('order_id', array('eq' => $getOrderApprovalId))
            ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_NORMAL))
        ;
        return $orderApprovalCollection->getData();
    }
    public function getOrderStatusForState($state)
    {
        if ($state) {
            switch ($state) {
                case 'pending_waiting_for_approval':
                    return $this->scopeConfig->getValue(self::XML_STATUS_ORDER_HAS_ITEM_APPROVAL);
                    break;
                case 'processing_waiting_for_approval':
                    return $this->scopeConfig->getValue(self::XML_STATUS_ORDER_WAITING_FOR_APPROVAL);
                    break;
                case 'approved':
                    return $this->scopeConfig->getValue(self::XML_STATUS_ORDER_APPROVED);
                    break;
                case 'disapproved':
                    return $this->scopeConfig->getValue(self::XML_STATUS_ORDER_DISAPPROVED);
                    break;
                case 'processing':
                    return \Magento\Sales\Model\Order::STATE_PROCESSING;
                    break;
            }
        }
        return false;
    }
}
