<?php

namespace Commercers\OnepageCheckout\Controller\Adminhtml\ShippingAddress;

class MassDelete extends \Magento\Backend\App\Action {

    protected $_filter;

    protected $_shippingAddressCollectionFactory;

    public function __construct(
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Commercers\OnepageCheckout\Model\ResourceModel\ShippingAddress\CollectionFactory $shippingAddressCollectionFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_filter            = $filter;
        $this->_shippingAddressCollectionFactory = $shippingAddressCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        try{
            $shippingAddressCollection = $this->_filter->getCollection($this->_shippingAddressCollectionFactory->create());
            foreach ($shippingAddressCollection as $item) {
                $item->delete();
            }
            $this->messageManager->addSuccess(__('Shipping Address Deleted Successfully.'));
        }catch(Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('onepagecheckout/shippingaddress/index'); //Redirect Path
    }
    protected function _isAllowed()
    {
        return true;
    }

}