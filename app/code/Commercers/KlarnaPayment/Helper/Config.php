<?php
namespace Commercers\KlarnaPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper {

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * system.xml Pfad zur Default Section
     */
    const SYS_XML_SECTION = 'klarna_installment_purchase';

    /**
     * system.xml Pfad zur Klarna-Ratenkauf Gruppe
     */
    const SYS_XML_PRODUCTS_KLARNA_FEE_GROUP = 'klarna_fee_group';

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * returns the raw value of a config field
     *
     * @param string $field the field (e.g. 'labelorders_expire_time')
     * @param string $group the fields group (e.g. SYS_XML_GROUP)
     * @param string $section the fields section (there usually is just one within this module)
     * @param string $store_id
     * @return mixed the config fields value
     */
    public function get_config_value($section, $group, $field , $store_id = null)
    {
        //get module config with $this->_scopeConfig->getValue('sectionName/groupName/fieldName');
        return $this->_scopeConfig->getValue($section . "/" . $group . "/" . $field,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function get_products_klarna_fee_value($field, $store_id = null)
    {
        return $this->get_config_value(static::SYS_XML_SECTION, static::SYS_XML_PRODUCTS_KLARNA_FEE_GROUP,$field, $store_id);
    }

    public function get_attributeset_for_fee_klarna($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_attributeset', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_hu($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_hu', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_zw($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_zw', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_dr($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_dr', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_vi($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_vi', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_fu($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_fu', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_se($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_se', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_si($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_si', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_ac($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_ac', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_ne($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_ne', $store_id);
    }

    public function get_products_for_fee_klarna_ratenkauf_ue($store_id = null)
    {
        return $this->get_products_klarna_fee_value('set_product_klarna_fee_ue', $store_id);
    }
}