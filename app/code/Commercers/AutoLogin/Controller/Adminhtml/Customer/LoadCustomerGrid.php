<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Controller\Adminhtml\Customer;

/**
 * Class LoadCustomerGrid
 * @package Commercers\AutoLogin\Controller\Adminhtml\Customer
 */
class LoadCustomerGrid extends \Magento\Framework\App\Action\Action{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $_resultRawFactory;

    /**
     * LoadCustomerGrid constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $rersultRawFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RawFactory $rersultRawFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultRawFactory = $rersultRawFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $resultPage = $this->_resultPageFactory->create();
        $result = $resultPage->getLayout()->renderElement('content');

        return $this->_resultRawFactory->create()->setContents($result);
    }

}