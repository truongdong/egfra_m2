<?php

namespace Commercers\HidePrice\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

	/**
	 * Installs DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		// Version 1.0.0
		if (!$installer->tableExists('commercers_hideprice')) {
			$table = $installer
				->getConnection()
				->newTable($installer->getTable('commercers_hideprice') )
				->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Id'
				)
				->addColumn(
					'customer_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'nullable' => false,
						'unsigned' => true,
						'default' => 0
					],
					'Customer ID'
				)
				->addColumn(
					'hide_price',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					[
						'nullable' => false,
						'unsigned' => true,
						'default' => 0
					],
					'Is Hide Price'
				)
			;
			$installer->getConnection()->createTable($table);
		}

		$installer->endSetup();
	}
}