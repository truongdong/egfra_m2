<?php
namespace Commercers\OrderApproval\Block\Supervisor;
use Magento\Framework\View\Element\Template;
class OverView extends Template
{
    protected $_customerSession;
    protected $_dataCollectionFactory;
    protected $_order;
    protected $_supervisorCollectionFactory;
    public function __construct(
        \Commercers\OrderApproval\Model\ResourceModel\Supervisor\CollectionFactory $supervisorCollectionFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        Template\Context $context
    ) {
        $this->_supervisorCollectionFactory = $supervisorCollectionFactory;
        $this->_order = $order;
        $this->_storeManager = $storeManager;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }
    public function getEmailCustomer()
    {
        return $this->_customerSession->getCustomer()->getEmail();
    }
    public function getNameCustomer()
    {
        return $this->_customerSession->getCustomer()->getName();
    }
    public function getAllItemNeedApproval()
    {
        $currEmailCustomer = $this->getEmailCustomer();
        $approvals = $this->_dataCollectionFactory->create();
        $approvals->addFieldToFilter('approval_email', array('like' => '%' . $currEmailCustomer. '%'))
            ->addFieldToFilter('status', array('eq' => \Commercers\OrderApproval\Model\Data::ORDER_APPROVAL_STATUS_INIT));
//        ->addFieldToFilter('status', array('neq' => \Commercers\OrderApproval\Model\Data::ORDER_APPROVAL_STATUS_NORMAL));
//            ->getSelect()->group('order_id')->order('last_send_reminder_at DESC');
        $itemApprovals = $approvals->getData();
        $arrItems = [];
        foreach ($itemApprovals as $item){
            $arrItems[$item['order_id']][] = $item;
        }
        return $arrItems;
    }
    public function getUrlApprovalItemOrder()
    {
        return $this->_storeManager->getStore()->getBaseUrl() . 'orderapproval/index/approvalitemorder';
    }
    public function getUrlMedia()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    public function getIncrementOrderItem($orderId)
    {
        $order = $this->_order->load($orderId);
        return $order->getIncrementId();
    }
    public function getQtyItemOrder($orderId,$orderItemId){
        $order = $this->_order->load($orderId);
        $orderItems = $order->getAllItems();
        $qty = 0;
        foreach ($orderItems as $item)
        {
            if($orderItemId == $item["item_id"]){
                $qty = $qty + $item["qty_ordered"];
                return $qty;
            }
        }
        return $qty;
    }
}