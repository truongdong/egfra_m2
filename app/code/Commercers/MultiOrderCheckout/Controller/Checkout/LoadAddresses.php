<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

class LoadAddresses extends AbstractIndex
{
	public function execute()
	{
		$result = [];
		try {
			$customerId = $this->_customerSession->getCustomerId();
			$addressList = $this->_addressListFactory->create()->getCollection()
				->addFieldToFilter('customer_id',$customerId)
			;
			$accountIds = [];
			foreach ($addressList as $address) {
				$accountIds[] = $address->getShippingAddressAccountId();
			}

			if(!empty($accountIds)) {
				$addressCollection = $this->_addressCollectionFactory->create()
				                                                     ->addFieldToFilter('account_id',['in',$accountIds])
				;
				foreach ( $addressCollection as $address ) {
					$result['options'][] = array (
						'value' => $address->getShippingaddressesId (),
						'label' =>  $address->getAccountName () . ', ' . $address->getShippingCompanyName () . ', ' . $address->getShippingStreet() . ', '  . $address->getShippingPostalCode () . ', ' . $address->getShippingCity() . ', ' .  $address->getShippingCountry () . ', ' . $address->getAccountId ()
					);
				}
				$result['is_error'] = false;
			} else {
				$result = [
					'is_error' => true,
					'message' => __('No account was found.')
				];
			}

		} catch (\Exception $e) {
			$result = [
				'is_error' => true,
				'message' => __('We are unable to process your request. Please, try again later.')
			];
		}

		return $this->_returnResultJson($result);
	}
}