<?php

namespace Commercers\OrderApproval\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;


class InstallData implements InstallDataInterface
{
    /**
     * Custom Processing Order-Status code
     */
    const ORDER_STATUS_PROCESSING_WAITING_FOR_APPROVAL_CODE = 'waiting_for_approval';
    /**
     * Custom Processing Order-Status label
     */
    const ORDER_STATUS_PROCESSING_WAITING_FOR_APPROVAL_LABEL = 'Waiting For Approval';

    /**
     * Custom Processing Order-Status code
     */
    const ORDER_STATUS_PROCESSING_APPROVAL_CODE = 'approved';
    /**
     * Custom Processing Order-Status label
     */
    const ORDER_STATUS_PROCESSING_APPROVAL_LABEL = 'Approved';

    /**
     * Status Factory
     *
     * @var StatusFactory
     */
    protected $statusFactory;
    /**
     * Status Resource Factory
     *
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;
    /**
     * InstallData constructor
     *
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        Config $eavConfig,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $this->addWaitingForApprovalProcessingStatus();
        $this->addWaitingForApprovalPendingStatus();
        $this->addWaitingForExportProcessingStatus();
        $this->addWaitingForExportPendingStatus();
        $setup->startSetup();
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY, 'order_approval_rule', [
                'type' => 'int',
                'label' => 'Muss freigegeben werden durch Profil',
                'input' => 'select',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 999,
                'system' => 0,
                'source' => '\Commercers\OrderApproval\Model\Source\Rule',
            ]
        );
        $attribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'order_approval_rule');
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $attribute->addData(
            array(
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer']
            )
        );
        $attribute->save();

        $customerSetup2 = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerSetup2->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY, 'order_shipping_approval_rule', [
                'type' => 'int',
                'label' => 'Approval for Shipping method',
                'input' => 'select',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 999,
                'system' => 0,
                'source' => '\Commercers\OrderApproval\Model\Source\Rule',
            ]
        );
        $attribute2 = $this->eavConfig->getAttribute(Customer::ENTITY, 'order_shipping_approval_rule');
        $customerEntity = $customerSetup2->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $attribute2->addData(
            array(
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer']
            )
        );
        $attribute2->save();

        $setup->endSetup();

    }
    protected function addWaitingForExportPendingStatus()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => __('pending_waiting_for_export'),
            'label' => __('PENDING - Waiting for export'),
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(Order::STATE_NEW, false, true);
    }

    protected function addWaitingForApprovalPendingStatus()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => __('pending_waiting_for_approval'),
            'label' => __('PENDING - Waiting for approval'),
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(Order::STATE_NEW, false, true);
    }

    protected function addWaitingForApprovalProcessingStatus()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => __('processing_waiting_for_approval'),
            'label' => __('PROCESSING - Waiting for approval'),
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(Order::STATE_PROCESSING, false, true);
    }
    protected function addWaitingForExportProcessingStatus()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => __('processing_waiting_for_export'),
            'label' => __('PROCESSING - Waiting for export'),
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(Order::STATE_PROCESSING, false, true);
    }
//    protected function addApprovalProcessingStatus()
//    {
//        /** @var StatusResource $statusResource */
//        $statusResource = $this->statusResourceFactory->create();
//        /** @var Status $status */
//        $status = $this->statusFactory->create();
//        $status->setData([
//            'status' => self::ORDER_STATUS_PROCESSING_APPROVAL_CODE,
//            'label' => self::ORDER_STATUS_PROCESSING_APPROVAL_LABEL,
//        ]);
//        try {
//            $statusResource->save($status);
//        } catch (AlreadyExistsException $exception) {
//            return;
//        }
//        $status->assignState(Order::STATE_PROCESSING, false, true);
//    }

}
