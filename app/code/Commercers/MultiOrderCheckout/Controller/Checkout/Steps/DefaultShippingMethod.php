<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout\Steps;

class DefaultShippingMethod extends \Commercers\MultiOrderCheckout\Controller\Checkout\AbstractIndex
{
	public function execute()
	{
		try {
			$method = $this->getRequest()->getParam('shipping_method');
			$checkoutSession = $this->_getCheckoutSession();
			$checkoutSession->setDefaultShippingMethod($method);

			$result = ['is_error' => false];
		} catch (\Exception $e) {
			$result = [
				'is_error' => false,
				'message' => $e->getMessage()
			];
		}

		return $this->_returnResultJson($result);
	}
}