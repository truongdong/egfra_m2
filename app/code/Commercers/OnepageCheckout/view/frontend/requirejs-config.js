var config = {
    map: {
        '*': {
            'Magento_Checkout/js/view/shipping': 'Commercers_OnepageCheckout/js/view/shipping-override',
            'Magento_Checkout/js/model/checkout-data-resolver': 'Commercers_OnepageCheckout/js/model/checkout-data-resolver-override',
            'Magento_Checkout/js/model/shipping-save-processor/default' : 'Commercers_OnepageCheckout/js/model/shipping-save-processor/default-override',
            'Magento_Checkout/js/view/shipping-information/address-renderer/default': 'Commercers_OnepageCheckout/js/view/shipping-information/address-renderer/default-override',
            'Magento_Payment/js/view/payment/method-renderer/free-method': 'Commercers_OnepageCheckout/js/view/payment/method-renderer/free-method-override'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/create-shipping-address': {
                'Commercers_OnepageCheckout/js/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Commercers_OnepageCheckout/js/set-shipping-information-mixin': true
            }
        }
    }
};
