<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\OrderApproval\Model\ResourceModel\Supervisor;

/**
 * CMS page collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'supervisor_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'commercers_orderapproval_supervisor_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'orderapproval_supervisor_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Commercers\OrderApproval\Model\Supervisor::class, \Commercers\OrderApproval\Model\ResourceModel\Supervisor::class);
        $this->_map['fields']['supervisor_id'] = 'main_table.supervisor_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

//    public function filterOrderApproval($customerId,$orderApprovalRuleId)
//    {
//        $this->getSelect()->join(
//            array('customers' => 'commercers_order_approval_supervisor_customer'),
//            'customers.supervisor_id = main_table.supervisor_id',
//            array()
//        );
//        $this->addFieldToFilter('customer_id', $customerId);
//        $this->getSelect()->where('main_table.rule_id = ?', $orderApprovalRuleId);
//        $result = $this->getSelect()->distinct();
//        return $result;
//        $this->getSelect()->join(
//            ['table1join'=>$this->getTable('commercers_order_approval_supervisor_customer')],
//            'main_table.supervisor_id = table1join.supervisor_id',
//            ['column1'=>'table1join.column1','column2'=>'table1join.column2']);
//    }
}
