<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class AssociatedCustomers
 * @package Commercers\AutoLogin\Controller\Customer
 */
class AssociatedCustomers extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Associated Customers'));

        return $resultPage;
    }
}