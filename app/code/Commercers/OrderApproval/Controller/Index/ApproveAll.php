<?php
namespace Commercers\OrderApproval\Controller\Index;

use Commercers\OrderApproval\Controller\Index\AbstractIndex;

class ApproveAll extends AbstractIndex {

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $codes = $params["approval_code"];
        $supervisorName = $params["supervisor_name"];
        $result = [];
        try {
            if ($codes) {
                $dataModel = $this->_dataFactory->create();
//                $codes = (unserialize(base64_decode($codes)));
                $orderId = $params["order_id"];

                $approvals = $this->_dataCollectionFactory->create();
                $approvals->addFieldToFilter('order_id', array('eq' => $orderId))
                    ->addFieldToFilter('approval_code', array('in' => (unserialize(base64_decode($codes))) ))
                    ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_INIT));
                $order = $this->_order->load($orderId);
                $storeId = $order->getStore()->getStoreId();
                $cancelledStatus = $dataModel->isXmlStatusOrderDisapproved();

                if ($order->getStatus() == $cancelledStatus) {
                    $result['approve_all'] = 2;
                }
                if (!$approvals->getSize()) {
                    $result['approve_all'] = 3;
                } else {
                    foreach ($approvals as $approval) {
                        if ($approval->getStatus() != $dataModel::ORDER_APPROVAL_STATUS_INIT) {
                            continue;
                        } else {
                            $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                            $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_APPROVED);
                            $approval->save();
                            $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                                'order_approval' => $approval,
                                'supervisor_name' => $supervisorName
                            ]);
                        }
                    }
                    $result['approve_all'] = 1;
                }
                $result['order_id'] = $orderId;
                $result['approval_code'] = $codes;
                $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_INIT;
            } else{
                $result['approve_all'] = 4;
            }
            return $this->redirectResult($result);
        } catch (\Exception $ex) {
            echo $ex->getMessage();exit;
            $result['approve_all'] = 4;
//            $this->_registry->register('approve_result', $result);
            return $this->redirectResult($result);
        }
        return $this->redirectResult($result);
    }
}
