<?php
namespace Commercers\OrderApproval\Controller\Index;

use Commercers\OrderApproval\Controller\Index\AbstractIndex;

class DisapproveAll extends AbstractIndex {

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $codes = $params["approval_code"];
        $supervisorName = $params["supervisor_name"];
        $result = [];

        try {
            if ($codes) {
                $dataModel = $this->_dataFactory->create();
//                $codes = (unserialize(base64_decode($codes)));
                $orderId = $params["order_id"];
                $approvals = $this->_dataCollectionFactory->create();
                $approvals->addFieldToFilter('order_id', array('eq' => $orderId))
                    ->addFieldToFilter('approval_code', array('in' => (unserialize(base64_decode($codes))) ))
                    ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_INIT));
                $order = $this->_order->load($orderId);
                //Cancel Order
                if($approvals->getSize() == intval($order->getTotalItemCount()) && $order->getState() == \Magento\Sales\Model\Order::STATE_NEW){
                    foreach ($approvals as $approval) {
                        // Start set comment history
                        $orderItemId = $approval->getOrderItemId();
                        $orderItem = $this->_orderItemFactory->create()->load($orderItemId);
                        $approverName = $approval->getApproverName();
                        if($supervisorId = $approval->getSupervisorId()){
                            $supervisor = $this->_supervisorFactory->create()->load($supervisorId);
                            $approverName = $supervisor->getName();
                        }
                        $shippingMethod = $approval->getShippingMethod();
                        if ($shippingMethod) {
                            $orderComment = __('Order %1 is disapproved by %2 for SKU %3 and Shipping method : %4', $order->getIncrementId(), $approverName, $orderItem->getSku(), $shippingMethod);
                        } else {
                            $orderComment = __('Order %1 is disapproved by %2 for SKU %3', $order->getIncrementId(), $approverName, $orderItem->getSku());
                        }
                        $order->addStatusHistoryComment($orderComment)->setIsVisibleOnFront(true)->setIsCustomerNotified(true)->save();
                        // End set comment history

                        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                        $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                        $approval->save();

                        //Start Sent email for buyer
                        $order = $this->_orderFactory->create()->load($orderId);
                        $approvalItem = $approval->getData();
                        $helperEmail = $this->_helperEmailFactory->create();
                        $helperEmail->sendBuyerEmail($approvalItem,$order,$typeEmail = 1,$supervisorName);
                        //End Sent email for buyer
                    }
                    try {
                        $result['approve_all'] = 6;
                        $result['order_id'] = $orderId;
                        $result['approval_code'] = $codes;
                        $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED;
                        $order->cancel()->save();

                        $order->addStatusHistoryComment(
                            __('Notified status of order')
                        )
                            ->setIsCustomerNotified(true)
                            ->save();

                        return $this->redirectResult($result);
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        if ($this->_objectManager->get('Magento\Checkout\Model\Session')->getUseNotice(true)) {
                            $this->messageManager->addNotice($e->getMessage());
                        } else {
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                $storeId = $order->getStore()->getStoreId();
                $cancelledStatus = $dataModel->isXmlStatusOrderDisapproved();

                $arrApprovals = [];
                $arrApprovals['order_id'] = $orderId;
                $arrApprovals['approval_code'] = $codes;
                $arrApprovals['status'] = $dataModel::ORDER_APPROVAL_STATUS_INIT;

                if ($order->getStatus() == $cancelledStatus) {
                    $result['approve_all'] = 2;
                }
                if (!$approvals->getSize()) {
                    $result['approve_all'] = 3;
                } else {
                    foreach ($approvals as $approval) {
                        if ($approval->getStatus() != $dataModel::ORDER_APPROVAL_STATUS_INIT) {
                            continue;
                        } else {
                            $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                            $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                            $approval->save();
                            $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                                'order_approval' => $approval,
                                'supervisor_name' => $supervisorName
                            ]);
                        }
                    }
                    $result['approve_all'] = 5;
                }
                $result['order_id'] = $orderId;
                $result['approval_code'] = $codes;
                $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_INIT;
            } else{
                $result['approve_all'] = 4;
            }
            return $this->redirectResult($result);
        } catch (\Exception $ex) {
            echo $ex->getMessage();exit;
            $result['approve_all'] = 4;
            $result['order_approval'] = $arrApproval;
//            $this->_registry->register('approve_result', $result);
            return $this->redirectResult($result);
        }
        return $this->redirectResult($result);
    }
}
