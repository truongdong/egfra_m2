<?php
namespace Commercers\DHLShipmentTracking\Model\ResourceModel;

class DHLShipmentTrackingLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('commercers_dhlshipmenttracking_log', 'id'); /* tên bảng , Id của bảng*/
    }

}
