<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\App\ObjectManager;
use Commercers\AutoLogin\Model\AutoLoginFactory;

/**
 * Class ForceSignIn
 * @package Commercers\AutoLogin\Controller\Customer
 */
class ForceSignIn extends \Magento\Framework\App\Action\Action {
    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;
    /**
     * @var SessionFactory
     */
    protected $_customerSessionFactory;
    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;
    /**
     * @var CookieMetadataManager
     */
    private $cookieMetadataManager;
    /**
     * @var AutoLoginFactory
     */
    protected $_autoLogin;

    /**
     * ForceSignIn constructor.
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param SessionFactory $customerSessionFactory
     * @param AutoLoginFactory $autoLogin
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        SessionFactory $customerSessionFactory,
        AutoLoginFactory $autoLogin
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_customerSessionFactory = $customerSessionFactory;
        $this->_autoLogin = $autoLogin;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        //echo 1;exit;
        $token = $this->_request->getParam('_token');
        $isBack = (boolean)$this->_request->getParam('is_back');
        $redirect  = $this->_request->getParam('redirect');
        if(isset($token) and trim($token) != ''){


            $autoLoginAccount = $this->_autoLogin->create()->load($token,'token');
            
            if($autoLoginAccount->getMainAccountId() OR $autoLoginAccount->getSubAccountId()){
                //Force sign out
                $this->_forceSignOut();
                if($isBack){
                    $autoLoginAccountId = $autoLoginAccount->getMainAccountId();
                }else{
                    $autoLoginAccountId = $autoLoginAccount->getSubAccountId();
                }
                /**
                 * @var $customer \Magento\Customer\Model\Customer
                 */
                $customer = $this->_customerFactory->create()->load($autoLoginAccountId);

                $this->_customerSessionFactory->create()->setCustomerAsLoggedIn($customer);
            }

            switch ($redirect){
                case 'homepage';
                    $url = $this->_url->getUrl();
                    break;
                default:
                    $url = $this->_url->getUrl('customer/account');
                    break;
            }
            return $this->_response->setRedirect($url)->sendResponse();
        }
        $redirectUrl = $this->_url->getUrl('customer/account');

        return $this->_response->setRedirect($redirectUrl)->sendResponse();
    }

    /**
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    protected function _forceSignOut(){
        $customerSession = $this->_customerSessionFactory->create();
        $customerSession->logout();

        if ($this->_getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->_getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->_getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }

    }

    /**
     * @return CookieMetadataManager|PhpCookieManager|mixed
     */
    private function _getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(PhpCookieManager::class);
        }
        return $this->cookieMetadataManager;
    }

    /**
     * @return CookieMetadataFactory|mixed
     */
    private function _getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(CookieMetadataFactory::class);
        }
        return $this->cookieMetadataFactory;
    }
}