<?php

namespace Commercers\OrderApproval\Controller\Product;

use Magento\Framework\App\Action\Action;

class GetAttribute extends Action{

    protected $_pageFactory;

    protected $_productRepository;
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_productRepository = $productRepository;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $skuProduct = $this->getRequest()->getParam('skuProduct');
        $data['message'] = false;
        $productHasOrderApprovalRule = $this->getOrderApprovalRuleProductBySku($skuProduct);
        if($productHasOrderApprovalRule) {
            $data['message'] = true;
        }
        try {
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData($data);
            return $response;
        } catch (Exception $e) {
        }
    }

    public function getOrderApprovalRuleProductBySku($sku)
    {
        $_product = $this->_productRepository->get($sku);

        $storeId = 0; //Store ID
        $attributeValue = $_product->getResource()->getAttributeRawValue($_product->getId(),'order_approval_rule',$storeId); //change attribute_code
        return $attributeValue;
    }

    public function getOrderApprovalRuleProductById($id)
    {
        $_product = $this->_productFactory->create()->load($id);
        // return $_product->getOrderApprovalRule();

        $storeId = 0; //Store ID
        $attributeValue = $_product->getResource()->getAttributeRawValue($_product->getId(),'order_approval_rule',$storeId); //change attribute_code
        return $attributeValue;
    }

}
