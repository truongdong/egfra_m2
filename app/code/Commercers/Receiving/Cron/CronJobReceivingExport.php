<?php

namespace Commercers\Receiving\Cron;

class CronJobReceivingExport 
{ 
    
    protected $_export;
    protected $_profilerFactory;

    public function __construct(
        \Commercers\Receiving\Service\Export $export,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->_profilerFactory = $profilerFactory;
        $this->_export = $export;
        $this->registry = $registry;
    }
    
    public function execute($schedule){
        $this->registry->register('stop_log_event', 1);
        $jobCode = $schedule->getJobCode();
        $profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
        $this->_export->execute($profiler);
    }
}