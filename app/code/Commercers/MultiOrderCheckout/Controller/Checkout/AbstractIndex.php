<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Commercers\MultiOrderCheckout\Model\ShippingMethods;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\StateException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Controller\Result\JsonFactory;
use Commercers\MultiOrderCheckout\Helper\Config as ConfigHelper;
use Magento\Multishipping\Helper\Data as MultishippingHelper;
use Psr\Log\LoggerInterface;
use Commercers\MultiOrderCheckout\Model\Checkout\Type\Multishipping;
use Commercers\MultiOrderCheckout\Model\AddressListFactory;
use Commercers\OnepageCheckout\Model\ResourceModel\ShippingAddress\CollectionFactory;

abstract class AbstractIndex extends \Magento\Multishipping\Controller\Checkout {

	/**
	 * @var PageFactory
	 */
	protected $_pageFactory;

	/**
	 * @var JsonFactory
	 */
	protected $_resultJsonFactory;

	/**
	 * @var ConfigHelper
	 */
	protected $_configHelper;

	/**
	 * @var MultishippingHelper
	 */
	protected $_multishippingHelper;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * @var Multishipping
	 */
	protected $multishipping;

	/**
	 * @var ShippingMethods
	 */
	protected $_shippingMethods;

	/**
	 * @var AddressListFactory
	 */
	protected $_addressListFactory;

	/**
	 * @var CollectionFactory
	 */
	protected $_addressCollectionFactory;

	protected $_customerFactory;
	public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
		Context $context,
		CustomerSession $customerSession,
		CustomerRepositoryInterface $customerRepository,
		AccountManagementInterface $accountManagement,
		PageFactory $pageFactory,
		JsonFactory $resultJsonFactory,
		ConfigHelper $configHelper,
		MultishippingHelper $multishippingHelper,
		LoggerInterface $logger,
		Multishipping $multishipping,
		ShippingMethods $shippingMethods,
		AddressListFactory $addressListFactory,
		CollectionFactory $addressCollectionFactory
	) {
	    $this->_customerFactory = $customerFactory;
		$this->_pageFactory = $pageFactory;
		$this->_resultJsonFactory = $resultJsonFactory;
		$this->_configHelper = $configHelper;
		$this->_multishippingHelper = $multishippingHelper;
		$this->logger = $logger;
		$this->multishipping = $multishipping;
		$this->_shippingMethods = $shippingMethods;
		$this->_addressListFactory = $addressListFactory;
		$this->_addressCollectionFactory = $addressCollectionFactory;

		parent::__construct(
			$context,
			$customerSession,
			$customerRepository,
			$accountManagement
		);
	}

	public function dispatch(RequestInterface $request)
	{
		$this->_request = $request;
		if ($this->_actionFlag->get('', 'redirectLogin')) {
			return parent::dispatch($request);
		}

		$action = $request->getActionName();

		$checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
		/**
		 * Catch index action call to set some flags before checkout/type_multishipping model initialization
		 */
		if ($action == 'index') {
			$checkoutSessionQuote->setIsMultiShipping(true);
			$checkoutSessionQuote->save();
			$this->_getCheckoutSession()->setCheckoutState(\Magento\Checkout\Model\Session::CHECKOUT_STATE_BEGIN);
		}

		if (!in_array($action, ['login', 'register'])) {
			$customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
			if (!$customerSession->authenticate($this->_getHelper()->getMSLoginUrl())) {
				$this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
			}

			if (!$this->_objectManager->get(
				\Magento\Multishipping\Helper\Data::class
			)->isMultishippingCheckoutAvailable()) {
				$error = $this->_getCheckout()->getMinimumAmountError();
				$this->messageManager->addError($error);
				$this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
				$this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
				return parent::dispatch($request);
			}
		}

		$result = $this->_preDispatchValidateCustomer();
		if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
			return $result;
		}

		if (!$result) {
			return $this->getResponse();
		}

		if ($this->_getCheckoutSession()->getCartWasUpdated(true)
		    &&
		    !in_array($action, ['index', 'login', 'register', 'addresses', 'success'])
		) {
			$this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
			$this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
			return parent::dispatch($request);
		}

		if ($action == 'success' && $this->_getCheckout()->getCheckoutSession()->getDisplaySuccess(true)) {
			return parent::dispatch($request);
		}

		try {
			$checkout = $this->_getCheckout();
		} catch (StateException $e) {
			$this->getResponse()->setRedirect($this->_getHelper()->getMSNewShippingUrl());
			$this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
			return parent::dispatch($request);
		}

		$quote = $checkout->getQuote();
		if (!$quote->hasItems() || $quote->getHasError() || $quote->isVirtual()) {
			$this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
			$this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
		}

		return \Magento\Checkout\Controller\Action::dispatch($request);
	}

	protected function _returnResultJson($result){
		return $this->_resultJsonFactory->create()->setData($result);
	}
}