<?php
namespace Commercers\SalesForceIntegrationSuite\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Contact extends AbstractDb {

    public function __construct(
        Context	$context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('commercers_salesforceintegrationsuite_contact', 'salesforceintegrationsuite_contact_id');
    }
}
