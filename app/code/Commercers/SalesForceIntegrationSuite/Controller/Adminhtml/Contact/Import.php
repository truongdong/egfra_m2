<?php

namespace Commercers\SalesForceIntegrationSuite\Controller\Adminhtml\Contact;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Result\PageFactory;

class Import extends \Magento\Backend\App\Action
{
    /**
     * @var $_resultPageFactory  \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Io\File $fileIo,
        \Magento\Backend\App\Action\Context $context
    ) {

        $this->_resultPageFactory = $resultPageFactory;
        $this->_fileSystem = $fileSystem;
        $this->_fileIo = $fileIo;
        parent::__construct($context);
    }

    public function execute()
    {
        $path = $this->_fileSystem->getDirectoryRead(DirectoryList::VAR_DIR)->getAbsolutePath('salesforce_contact_import');
        $this->_fileIo->mkdir($path, '0777', true);
        if (!is_writable($path)) {
            $this->messageManager->addNotice(__('Please make this directory path writable var/salesforce_contact_import'));
        }

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Commercers_SalesForceIntegrationSuite::salesforce_contact_import');
        $resultPage->getConfig()->getTitle()->prepend('Import SalesForceIntegrationSuite Contact');

        return $resultPage;
    }
}
