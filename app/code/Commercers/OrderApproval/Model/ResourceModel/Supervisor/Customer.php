<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\OrderApproval\Model\ResourceModel\Supervisor;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Customer extends AbstractDb {

    public function __construct(
        Context	$context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('commercers_order_approval_supervisor_customer', 'supervisor_customer_id');
    }
}
