<?php
namespace Commercers\CheckoutCustomerAttribute\Model\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Options extends AbstractSource
{
    public function getAllOptions()
    {
        return [
            '1' => [
                'label' => 'Eggers & Franke GmbH',
                'value' => '1'
            ],
            '2' => [
                'label' => 'Joh. Eggers Sohn GmbH',
                'value' => '2'
            ],
            '3' => [
                'label' => 'Ludwig von Kapff GmbH',
                'value' => '3'
            ],
            '4' => [
                'label' => 'Reidemeister&Ulrichs GmbH',
                'value' => '4'
            ],
            '5' => [
                'label' => 'Rotkäppchen-Mumm Sektkellereien GmbH',
                'value' => '5'
            ],
            '6' => [
                'label' => 'reserviert 1',
                'value' => '6'
            ],
            '7' => [
                'label' => 'reserviert-2',
                'value' => '7'
            ]
        ];
    }
}