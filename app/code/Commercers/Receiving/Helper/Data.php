<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\Receiving\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

class Data extends AbstractHelper
{
    /**
     * @var EncryptorInterface
     */
    protected $encryptor;
    protected $storeManager;
    protected $messageManager;
    /**
     * @param Context $context
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        CollectionFactory $collectionFactory,
        EncryptorInterface $encryptor
    )
    {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->_collectionFactory = $collectionFactory;
    }

    public function getOption($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'receiving/general/product_attribute',
            $scope
        );
    }
    public function getAllowNegative($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'receiving/general/allow_negative',
            $scope
        );
    }
    
    public function getProductAttributeShowOnSuccess($product){
        $attributes = $this->scopeConfig->getValue(
            'receiving/general/product_message_attribute');
        if ($attributes){
            $attributes = explode(',', $attributes);
        }
        if(is_array($attributes)){
            foreach($attributes as $attributeCode){
                if($attributeCode){
                    $label = $this->getAttribute($product,$attributeCode);
                    $attr = $attributeCode;
                    $value = $product->getData($attr);
                    if($attr == 'quantity_and_stock_status'){
                        $value = $value['qty'];
                    }
                    if($value)
                    $this->messageManager->addSuccess(__($label.':'.$value));
                }
            }
        }
    }
     public function getAttribute($product,$attributeCode){
        $label =   $product->getResource()->getAttribute($attributeCode)->getFrontendLabel();
        return $label;
     }
    
}