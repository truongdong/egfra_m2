<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/25/18
 * Time: 22:52
 */
namespace Commercers\DpdTrack\Model;

use Soapclient;
use SOAPHeader;

class DpdParcelTrack {

	/**
	 * @var \Commercers\DpdTrack\Helper\Data
	 */
	protected $_helper;

	/**
	 * @var \Commercers\DpdTrack\Logger\Logger
	 */
	private $_logger;

	/**
	 * @var \Commercers\DpdTrack\Model\DpdAuth
	 */
	protected $_auth;

	protected $_parcelStatusWsdl;
	protected $_soapHeaderUrl;
	protected $_options;


	public function __construct(
		\Commercers\DpdTrack\Helper\Data $helper,
		\Commercers\DpdTrack\Logger\Logger $logger,
		\Commercers\DpdTrack\Model\DpdAuth $dpdAuth
	) {
		$this->_helper = $helper;
		$this->_logger = $logger;

		$this->_auth = $dpdAuth->auth();


		$this->_parcelStatusWsdl = $this->_helper->getConfigValue('dpdtrack/webservice/parcel_life_cycle');
		$this->_soapHeaderUrl = $this->_helper->getConfigValue('dpdtrack/webservice/soap_header_url');
	}

	public function getStatus($parcelLabelNumber){
		$result = array();
		try {
			$client = new Soapclient($this->_parcelStatusWsdl);
			$header = new SOAPHeader($this->_soapHeaderUrl, 'authentication', $this->_auth);

			$client->__setSoapHeaders($header);
			$response = $client->getTrackingData(['parcelLabelNumber' => $parcelLabelNumber]);


			$check = (array)$response->trackingresult;
			if (empty($check)) {
				$this->_logger->err('Parcel '.$parcelLabelNumber.' not found');
			} else {
				foreach($response->trackingresult->statusInfo as $statusInfo){
					if ($statusInfo->isCurrentStatus){
						$result = array(
							'statusCode' => $statusInfo->status,
							'statusLabel' => $statusInfo->label->content,
							'statusDescription' => $statusInfo->description->content->content,
						);

					}
				}
			}
		}
		catch (\Exception $e){
			$this->_logger->err($e->getMessage());
		}

		return $result;
	}

}