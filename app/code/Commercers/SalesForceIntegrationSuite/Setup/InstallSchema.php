<?php

namespace Commercers\SalesForceIntegrationSuite\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('commercers_salesforceintegrationsuite_contact')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_salesforceintegrationsuite_contact')
            )
                ->addColumn(
                    'salesforceintegrationsuite_contact_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'auto_increment' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'length' => 11
                    ],
                    'Sales Force Integration Suite Contact Id'
                )
                ->addColumn(
                    'account_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Account Id'
                )
                ->addColumn(
                    'sf_contact_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Sf contact id'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Name'
                )
                ->addColumn(
                    'email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Email'
                )
                ->addColumn(
                    'is_primary',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Is primary'
                )

                ->addColumn(
                    'phone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Phone'
                )
                ->addColumn(
                    'prefix',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Prefix'
                )
                ->addColumn(
                    'firstname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'First name'
                )
                ->addColumn(
                    'lastname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Last name'
                )
                ->addColumn(
                    'description',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Description'
                )
                ->addColumn(
                    'home_phone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Home phone'
                )
                ->addColumn(
                    'mailing_country',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Mailing country'
                )
                ->addColumn(
                    'mailing_state',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Mailing state'
                )
                ->addColumn(
                    'mailing_postalcode',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Mailing postal code'
                )
                ->addColumn(
                    'mailing_city',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Mailing city'
                )
                ->addColumn(
                    'mailing_street',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Mailing street'
                )
                ->addIndex(
                    $installer->getTable('commercers_salesforceintegrationsuite_contact'),
                    $installer->getIdxName(
                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
                        ['salesforceintegrationsuite_contact_id', 'account_id','email'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['salesforceintegrationsuite_contact_id', 'account_id','email'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                );
//                ->addIndex(
//                    $installer->getIdxName(
//                        'commercers_salesforceintegrationsuite_contact',
//                        ['salesforceintegrationsuite_contact_id', 'account_id','email'],
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    ['salesforceintegrationsuite_contact_id', 'account_id','email'],
//                    ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
//                );
//
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'salesforceintegrationsuite_contact_id',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'salesforceintegrationsuite_contact_id'
//                )
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'account_id',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'account_id'
//                )
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'prefix',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'prefix'
//                )
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'firstname',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'firstname'
//                )
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'lastname',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'lastname'
//                )
//                ->addIndex(
//                    $setup->getIdxName(
//                        $installer->getTable('commercers_salesforceintegrationsuite_contact'),
//                        'email',
//                        AdapterInterface::INDEX_TYPE_FULLTEXT
//                    ),
//                    'email'
//                );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
