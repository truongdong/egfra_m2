<?php
namespace Commercers\OnepageCheckout\Block\Checkout;

class LayoutProcessor
{
    CONST ATTRIBUTE_CODE = 'egfra_division';
    protected $customerSession;
    protected $resultPageFactory;
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Model\Session $customerSession
    ){
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerSession = $customerSession;
    }
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        $jsLayout
    ) {
        $attributeId = 174;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
            ->setPositionOrder('asc')
            ->setAttributeFilter($attributeId)
            ->setStoreFilter()
            ->load();
        $opt_val = array();
        $allOptions=array();


        foreach ($attributeOptionAll->getData() as $key => $v)
        {
            // $allid = $v['attribute_id'];
            $opt_val['value'] = $v['option_id'];
            $opt_val['label'] = $v['value'];
            $allOptions[] = $opt_val;
        }
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children'][self::ATTRIBUTE_CODE] = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'rows' => 5
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . self::ATTRIBUTE_CODE,
            'label' => __('Egfra Division'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 1,
            'validation' => [
                'required-entry' => true
            ],
            'options' =>  $allOptions,
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => []
        ];

//        Contact_Email
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['shipping_email'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'rows' => 5
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . 'shipping_email',
            'label' => __('Email Address'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 0,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => '' // value field is used to set a default value of the attribute
        ];

        return $jsLayout;
    }
}