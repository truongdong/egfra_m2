<?php

namespace Commercers\OrderApproval\Model\Source;

class Rule extends \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend
{
    protected $_options = null;

    protected $_ruleCollectionFactory;

    public function __construct(
        \Commercers\OrderApproval\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
    ) {
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
    }
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = array();
            $this->_options[] = array(
                'label' => 'None',
                'value' => 0
            );
            $collection = $this->_ruleCollectionFactory->create()
                ->addFieldToFilter('active', 1);
            // if ($collection->getSize()) {
            foreach ($collection as $item) {
                $this->_options[] = array('value' => $item->getOrderApprovalRuleId(), 'label' => $item->getRuleName());
            }
            // }
        }
        return $this->_options;
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }
    public function getOptionText($value){
        foreach($this->getAllOptions() as $option){
            if($value == (int)$option['value']){
                return $option['label'];
            }
        }
        return '';
    }
}
