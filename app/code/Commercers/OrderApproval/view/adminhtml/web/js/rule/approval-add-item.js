define([
    'uiComponent',
    'jquery',
    'ko',
    'underscore',
    'mage/translate',
    'mage/url',
    "Commercers_OrderApproval/js/customers/charge-customer"
], function (Component, $, ko, _, $t, urlBuilder,chargeCustomer) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();
            this.formSupervisorsCollection = ko.observableArray();
            // this.gridCustomerData = ko.observable();
            this.loadCustomerData(this.supervisorsCollection);
            this.stopSaveRuleByEnterKey();
        },

        stopSaveRuleByEnterKey: function () {
            $(document).ready(function () {
                $("form#approval-rule-form").bind("keypress", function (e) {
                    if (e.keyCode == 13) {
                        return false;
                    }
                });
            });
        },
        getItemsJsonPrint: function(){
            var self = this;
            var consideredAttributes = ['supervisorId','supervisorName','supervisorEmail','supervisorStatus'];
            return ko.toJSON(self.formSupervisorsCollection, consideredAttributes, 2)
        },

        getItemsJson: function(){
            var self = this;
            var consideredAttributes = ['supervisorId','supervisorName','supervisorEmail','supervisorStatus'];
            return ko.toJSON(self.formSupervisorsCollection, consideredAttributes, null)
        },

        loadCustomerData: function (values) {
            var self = this;

            var mappedData = $.map(values, function (data,idx) {
                return new self.formSupervisorsModel(data,idx);
            });
            self.formSupervisorsCollection(mappedData);

            return this;
        },
        addSupervisorsModel: function () {
            var idx = this.formSupervisorsCollection().length;
            this.formSupervisorsCollection.push(new this.formSupervisorsModel({}, idx));
            this.getItemsJson(this.formSupervisorsCollection);
        },
        removeSupervisorsModel: function (supervisorsModel) {
            this.formSupervisorsCollection.remove(supervisorsModel);
        },


        formSupervisorsModel: function (data, idx=null) {
            var self = this; // this = formSupervisorsModel {}

            self.supervisorId = ko.observable();
            self.supervisorEmail = ko.observable().extend({
                required: "This is a required field."
            });
            self.supervisorName = ko.observable().extend({
                required: "This is a required field."
            });
            self.supervisorStatus = ko.observable();


            if(!data.supervisor_id){
                self.supervisorId('');
            }else{
                self.supervisorId(data.supervisor_id);
            }
            self.supervisorEmail(data.supervisor_email);
            self.supervisorName(data.supervisor_name);
            self.supervisorStatus(data.supervisor_status);
            self.gridCustomerData = ko.observable();

            var url = window.urlGetCustomerData;
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'supervisor_id': data.supervisor_id,
                    'idx': idx
                },
                dataType: 'json',
                success: function (response) {
                    self.gridCustomerData(response.grid);
                }
            });
        },
    });
});
