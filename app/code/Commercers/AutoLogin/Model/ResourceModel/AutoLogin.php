<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Model\ResourceModel;
/**
 * Class AutoLogin
 * @package Commercers\AutoLogin\Model\ResourceModel
 */
class AutoLogin extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * AutoLogin constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }
    protected function _construct()
    {
        $this->_init('commercers_auto_login', 'entity_id');
    }

}