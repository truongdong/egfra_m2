<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Item;

class CheckItems extends AbstractIndex
{
	public function execute()
	{
		try {
			$addressIds = $this->getRequest()->getPost('address_ids');

			if (!\is_array($addressIds)) {
				throw new LocalizedException(
					__('We are unable to process your request. Please, try again later.')
				);
			}

			$quote = $this->_getCheckoutSession()->getQuote();
			foreach ($quote->getAllItems() as $item) {
				$itemQty = $item->getQty() * count($addressIds);
				$this->updateItemQuantity($item, $itemQty);
			}

			if ($quote->getHasError()) {
				throw new LocalizedException(__($quote->getMessage()));
			}

			$result = [
				'is_error' => false
			];
		} catch (LocalizedException $e) {
			$result = [
				'is_error' => true,
				'message' => $e->getMessage()
			];
		} catch (\Exception $e) {
			$this->logger->critical($e->getMessage());
			$result = [
				'is_error' => true,
				'message' => __('We are unable to process your request. Please, try again later.')
			];
		}

		return $this->_returnResultJson($result);
	}

	private function collectItemsInfo(array $shippingInfo): array
	{
		$itemsInfo = [];
		foreach ($shippingInfo as $itemData) {
			if (!\is_array($itemData)) {
				continue;
			}
			foreach ($itemData as $quoteItemId => $data) {
				if (!isset($itemsInfo[$quoteItemId])) {
					$itemsInfo[$quoteItemId] = 0;
				}
				$itemsInfo[$quoteItemId] += (double)$data['qty'];
			}
		}

		return $itemsInfo;
	}

	private function updateItemQuantity(Item $item, $quantity)
	{
		if ($quantity > 0) {
			$item->setQty($quantity);
			if ($item->getHasError()) {
				throw new LocalizedException(__('The stock of article %1 is not sufficient to deliver to other recipients.',$item->getName()));
			}
		}
	}
}