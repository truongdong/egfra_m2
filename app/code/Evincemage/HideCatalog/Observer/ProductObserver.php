<?php
namespace Evincemage\HideCatalog\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductObserver implements ObserverInterface
{
    /**
     * Add filter to product collection
     *
     * @param EventObserver $observer
     * @return void
     */
    protected $response;
     public function __construct(
      \Magento\Framework\App\Response\Http $response,  
      \Evincemage\HideCatalog\Helper\Data $helper
         
    ) {
       $this->helper = $helper;
       $this->response = $response;
    }  
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $CustomerGroupID=$this->helper->getCustomerGroupId();
        $product = $observer->getProduct();
       if($this->helper->isModuleActive() ){
        $customeSetting = $product->getHideProductGroups();
        if($product->getHideProductGroups()){
           if($this->helper->CatalogLoadVisible() || (in_array($CustomerGroupID, $customeSetting) && !in_array(\Evincemage\HideCatalog\Helper\Data::USE_NONE, $customeSetting)) ){
               $this->response->setRedirect($this->helper->catalogRedirect());
           } 
        }else{
           if($this->helper->CatalogLoadVisible()){
               $this->response->setRedirect($this->helper->catalogRedirect());
           } 
        }
       }

    }
}