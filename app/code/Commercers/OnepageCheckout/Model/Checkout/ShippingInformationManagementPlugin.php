<?php
namespace Commercers\OnepageCheckout\Model\Checkout;

use Magento\Quote\Model\QuoteRepository;

class ShippingInformationManagementPlugin
{
    protected $quoteRepository;

    public function __construct(
        QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extensionAttributes = $addressInformation->getShippingAddress();
        $custAttributes = $extensionAttributes->getExtensionAttributes();
        $quote = $this->quoteRepository->getActive($cartId);
        $egfraDivision = $custAttributes->getEgfraDivision();
        $shippingAddressId = $custAttributes->getShippingaddressesAccountId();

        $quote->setEgfraDivision($egfraDivision);
        $quote->setShippingaddressesAccountId($shippingAddressId);
    }
}