<?php

namespace Commercers\MultiOrderCheckout\Block\Checkout\Steps;

use Magento\Sales\Model\Order\Shipment;
use Magento\Store\Model\ScopeInterface;

class DefaultShippingMethod extends \Commercers\MultiOrderCheckout\Block\Checkout\Steps
{
	public function getShippingMethods() {
		$address = $this->_getDefaultShippingAddress();
		$shippingMethods = $this->_shippingMethods->getShippingMethods($address);

		return $shippingMethods;
	}

	protected function _getDefaultShippingAddress() {
		if($shippingAddress = $this->_customerSession->getCustomer()->getDefaultShippingAddress()) {
			return $shippingAddress;
		}

		return null;
	}
}
