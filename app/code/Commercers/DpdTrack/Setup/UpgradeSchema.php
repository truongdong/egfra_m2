<?php

namespace Commercers\DpdTrack\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

		// Version 1.0.1
		if(version_compare($context->getVersion(), '1.0.1', '<')) {
			if (!$installer->tableExists('commercers_dpdtrack_log')) {
				$table = $installer
					->getConnection()
					->newTable($installer->getTable('commercers_dpdtrack_log') )
					->addColumn(
					   'id',
					   \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					   null,
					   [
					   	    'identity' => true,
					        'nullable' => false,
					        'primary'  => true,
					        'unsigned' => true,
					   ],
					   'ID'
					)
					->addColumn(
					   'order_id',
					   \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					   255,
					   [ 'nullable => false'],
					   'Order increment ID'
					)
					->addColumn(
					   'tracking_number',
					   \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					   255,
					   [],
					   'Shipment Tracking number'
					)
					->addColumn(
					   'status',
					   \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					   255,
					   [],
					   'Current Status'
					)
					->addColumn(
					   'created_at',
					   \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					   null,
					   [
	                        'nullable' => false,
	                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
					   ],
					   'Created At'
					)
					->addColumn(
						'updated_at',
						\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						null,
						[
							'nullable' => false,
							'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
						],
						'Updated At'
					)
				;
				$installer->getConnection()->createTable($table);

				$installer->getConnection()->addIndex(
					$installer->getTable('commercers_dpdtrack_log'),
					$setup->getIdxName(
						$installer->getTable('commercers_dpdtrack_log'),
						['order_id'],
						\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
					),
					['order_id'],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
				);
			}
		}

		// Version 1.0.2
		if(version_compare($context->getVersion(), '1.0.2', '<')) {
			if ( $installer->tableExists( 'commercers_dpdtrack_log' ) ) {
				$connection = $installer->getConnection()
					->addColumn(
						$installer->getTable('commercers_dpdtrack_log'),
						'increment_id',
						[
							'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
							'length' => 255,
							'nullable' => true,
							'comment' => 'Increment ID'
						]

					)
				;
			}
		}

		$installer->endSetup();
	}
}