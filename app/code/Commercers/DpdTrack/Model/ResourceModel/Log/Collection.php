<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/24/18
 * Time: 16:40
 */
namespace Commercers\DpdTrack\Model\ResourceModel\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'commercers_dpdtrack_log_collection';
	protected $_eventObject = 'log_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Commercers\DpdTrack\Model\Log', 'Commercers\DpdTrack\Model\ResourceModel\Log');
	}

}