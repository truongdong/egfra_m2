<?php
namespace Evincemage\HideCatalog\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    protected $_customerGroup;
    public function __construct(
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'hide_product_groups',
            [
                'type' => 'varchar',
                'backend' => 'Evincemage\HideCatalog\Model\Product\Attribute\Backend\Options',
                'frontend' => '',
                'label' => 'Hide from Groups',
                'input' => 'multiselect',
                'source' => 'Evincemage\HideCatalog\Model\Config\Source\Options',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'group' => 'General',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'hide_category_groups',
            [
                'type' => 'varchar',
                'label' => 'Hide from Groups',
                'input' => 'multiselect',
                'required' => false,
                'backend' => 'Evincemage\HideCatalog\Model\Category\Attribute\Backend\Options',
                'frontend' => '',
                'source' => 'Evincemage\HideCatalog\Model\Config\Source\Options',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
                'default' => \Evincemage\HideCatalog\Helper\Data::USE_NONE,
                'user_defined' => true,
                'visible' => true
            ]
        );
    }
}