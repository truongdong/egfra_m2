<?php

namespace Commercers\OnepageCheckout\Controller\ShippingAddress;

use Magento\Framework\App\Action\Action;

class SearchResult extends Action{
    const XML_PATH_FILTERBY_CUSTOMER_GROUP = 'shippingaddresses/settings/filter_by_customer_group';
    const XML_PATH_EMAIL_IMPORT_ADDRESSES_IS_GLOBAL = 'shippingaddresses/import_addresses/use_as_global';

    protected $_pageFactory;

    protected $_shippingAddressCollectionFactory;
    public function __construct(
        \Commercers\OnepageCheckout\Model\ResourceModel\ShippingAddress\CollectionFactory $shippingAddressCollectionFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_shippingAddressCollectionFactory = $shippingAddressCollectionFactory;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $dataSearch = $this->getRequest()->getParams();
        $shippingAddressConllection = $this->getShippingAddressCollection($dataSearch);
        $data['message'] = false;
        $data['options'] = [];
        if(!empty($shippingAddressConllection->getData())){
            $data['message'] = true;
            foreach ( $shippingAddressConllection as $address ) {
                $data['options'][] = array (
                    'value' => $address->getShippingaddressesId (),
                    'label' =>  $address->getAccountName () . ', ' . $address->getShippingCompanyName () . ', ' . $address->getShippingStreet() . ', '  . $address->getShippingPostalCode () . ', ' . $address->getShippingCity() . ', ' .  $address->getShippingCountry () . ', ' . $address->getAccountId ()
                );
            }
        }
        try {
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData($data);
            // create select object
//            $response = Mage::getSingleton ( 'core/layout' )->createBlock ( 'core/html_select' )->setName ( 'commercers_shipping_address_id' )->setId ( 'shipping-address-select' )->setClass('address-select address-multiple' )->addData($options )->setValue($selectedValue)->setOptions($options);

            return $response;
        } catch (Exception $e) {
        }
    }

    public function getShippingAddressCollection($dataSearch){
        $shippingAddressCollection = $this->_shippingAddressCollectionFactory->create();
        $searchCustomerNumber = $dataSearch["searchCustomerNumber"];
        $searchCustomerName = $dataSearch["searchCustomerName"];
        $searchPostalcode = $dataSearch["searchPostalcode"];
        $searchCity = $dataSearch["searchCity"];

        // prepare the filter criteria
        if ($searchCustomerNumber !== false && strlen ( trim ( $searchCustomerNumber ) ) > 0) {
            $shippingAddressCollection->addFieldToFilter ( 'main_table.account_id', array (
                'like' => "%" . trim ( $searchCustomerNumber ) . "%"
            ) );
        }

        if ($searchCustomerName !== false && strlen ( trim ( $searchCustomerName ) ) > 0) {
            $shippingAddressCollection->addFieldToFilter ( 'account_name', array (
                'like' => "%" . trim ( $searchCustomerName ) . "%"
            ) );
        }

        if ($searchCity !== false && strlen ( trim ( $searchCity ) ) > 0) {
            $shippingAddressCollection->addFieldToFilter ( 'shipping_city', array (
                'like' => "%" . trim ( $searchCity ) . "%"
            ) );
        }

        if ($searchPostalcode !== false && strlen ( trim ( $searchPostalcode ) ) > 0) {
            $shippingAddressCollection->addFieldToFilter ( 'shipping_postal_code', array (
                'like' => "%" . trim ( $searchPostalcode ) . "%"
            ) );
        }
//        if ($excludeBranchOffice != false) {
//            $shippingAddressesCollection->getSelect ()->where ( "`branch_office` IS NOT NULL");
//        }
//        echo $shippingAddressCollection->getSelect();exit;
        $shippingAddressCollection->getSelect();

        return $shippingAddressCollection;
    }
}
