<?php
namespace Evincemage\HideCatalog\Model\Config\Source\Mode;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

class Product
{
    /**
     * Return the mode options for the product configuration
     *
     * @return array
     */
    public function __construct(\Evincemage\HideCatalog\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
    public function toOptionArray()
    {
        return array(
            array(
                'value' => \Evincemage\HideCatalog\Helper\Data::MODE_SHOW_BY_DEFAULT,
                'label' => __('Show products by default')
            ),
            array(
                'value' => \Evincemage\HideCatalog\Helper\Data::MODE_HIDE_BY_DEFAULT,
                'label' => __('Hide products by default')
            )
        );
    }
}