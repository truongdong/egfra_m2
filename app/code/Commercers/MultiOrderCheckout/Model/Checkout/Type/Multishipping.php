<?php
namespace Commercers\MultiOrderCheckout\Model\Checkout\Type;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Directory\Model\AllowedCountries;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Psr\Log\LoggerInterface;
class Multishipping extends \Magento\Multishipping\Model\Checkout\Type\Multishipping
{
    /**
     * @var \Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor
     */
    private $shippingAssignmentProcessor;
    protected $_addressValidateFailed = [];
    public function setShippingItemsInformation($info)
    {
        if (is_array($info)) {
            $quote = $this->getQuote();
            $addresses = $quote->getAllShippingAddresses();
            foreach ($addresses as $address) {
                $quote->removeAddress($address->getId());
            }
            foreach ($info as $addressId => $itemData) {
                foreach ($itemData as $quoteItemId =>  $data) {
                    $this->_addShippingItem($quoteItemId, $data);
                }
            }
            if(!empty($this->_addressValidateFailed)) {
                throw new \Magento\Framework\Exception\ValidatorException(
                    __('Verify the shipping address information and continue.',array_unique($this->_addressValidateFailed))
                );
            }
            $this->prepareShippingAssignment($quote);
            /**
             * Delete all not virtual quote items which are not added to shipping address
             * MultishippingQty should be defined for each quote item when it processed with _addShippingItem
             */
            foreach ($quote->getAllItems() as $_item) {
                if (!$_item->getProduct()->getIsVirtual() && !$_item->getParentItem() && !$_item->getMultishippingQty()
                ) {
                    $quote->removeItem($_item->getId());
                }
            }
            $billingAddress = $quote->getBillingAddress();
            if ($billingAddress) {
                $quote->removeAddress($billingAddress->getId());
            }
            $customerDefaultBillingId = $this->getCustomerDefaultBillingAddress();
            if ($customerDefaultBillingId) {
                $quote->getBillingAddress()->importCustomerAddressData(
                    $this->addressRepository->getById($customerDefaultBillingId)
                );
            }
            foreach ($quote->getAllItems() as $_item) {
                if (!$_item->getProduct()->getIsVirtual()) {
                    continue;
                }
                if (isset($itemsInfo[$_item->getId()]['qty'])) {
                    $qty = (int)$itemsInfo[$_item->getId()]['qty'];
                    if ($qty) {
                        $_item->setQty($qty);
                        $quote->getBillingAddress()->addItem($_item);
                    } else {
                        $_item->setQty(0);
                        $quote->removeItem($_item->getId());
                    }
                }
            }
            $this->save();
            $this->_eventManager->dispatch('checkout_type_multishipping_set_shipping_items', ['quote' => $quote]);
        }
        return $this;
    }
    protected function _addShippingItem($quoteItemId, $data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerAddress = $objectManager->create('Magento\Customer\Model\Data\Address');
        $qty = isset($data['qty']) ? (int)$data['qty'] : 1;
        $addressData = isset($data['address']) ? $data['address'] : false;
        $quoteItem = $this->getQuote()->getItemById($quoteItemId);
        if ($addressData && $quoteItem) {
            /**
             * Skip item processing if qty 0
             */
            if ($qty === 0) {
                return $this;
            }
            $quoteItem->setMultishippingQty((int)$quoteItem->getMultishippingQty() + $qty);
            $quoteItem->setQty($quoteItem->getMultishippingQty());
            $quoteAddress = null;
            foreach ($this->getQuote()->getAddressesCollection() as $address) {
                if (!$address->isDeleted() &&
                    $address->getAddressType() == Address::TYPE_SHIPPING &&
                    $address->getCommercersShippingaddressId() == $addressData['address_id']
                ) {
                    $quoteAddress = $address;
                }
            }
            if (!$quoteAddress) {
                $customerAddress->setCustomerId($this->_customerSession->getId());
                $customerAddress->setRegion(null);
                $customerAddress->setRegionId(0);
                $customerAddress->setCountryId($addressData['country_id']);
                $customerAddress->setStreet([$addressData['street']]);
                $customerAddress->setCompany($addressData['company']);
                $customerAddress->setTelephone($addressData['telephone']);
                $customerAddress->setFax($addressData['fax']);
                $customerAddress->setPostcode($addressData['postcode']);
                $customerAddress->setCity($addressData['city']);
                $customerAddress->setFirstname($addressData['firstname']);
                $customerAddress->setLastname($addressData['lastname']);
                $quoteAddress = $this->_addressFactory->create()->importCustomerAddressData($customerAddress);
                $quoteAddress->setCommercersShippingaddressId($addressData['address_id']);
                $this->getQuote()->addShippingAddress($quoteAddress);
            }
            $quoteAddress->setCustomerAddressId(0);
            $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);
            if ($quoteAddressItem) {
                $quoteAddressItem->setQty((int)($quoteAddressItem->getQty() + $qty));
            } else {
                $quoteAddress->addItem($quoteItem, $qty);
            }
            $addressValidation = $quoteAddress->validate();
            if ($addressValidation !== true) {
                $this->_addressValidateFailed[] = $addressData['address_id'];
            }
            /**
             * Require shipping rate recollect
             */
            $quoteAddress->setCollectShippingRates((bool)$this->getCollectRatesFlag());
        }
        return $this;
    }
    private function prepareShippingAssignment($quote)
    {
        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cartExtension = $objectManager->create('Magento\Quote\Api\Data\CartExtensionFactory');
        }
        /** @var \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment */
        $shippingAssignment = $this->getShippingAssignmentProcessor()->create($quote);
        $shipping = $shippingAssignment->getShipping();
        $shipping->setMethod(null);
        $shippingAssignment->setShipping($shipping);
        $cartExtension->setShippingAssignments([$shippingAssignment]);
        return $quote->setExtensionAttributes($cartExtension);
    }
    private function getShippingAssignmentProcessor()
    {
        if (!$this->shippingAssignmentProcessor) {
            $this->shippingAssignmentProcessor = ObjectManager::getInstance()
                ->get(\Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor::class);
        }
        return $this->shippingAssignmentProcessor;
    }
    public function updateQuoteCustomerShippingAddress($addressId)
    {
        try {
            $address = $this->addressRepository->getById($addressId);
            // phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
        } catch (\Exception $e) {
            //
        }
        if (isset($address)) {
            $quoteAddress = $this->getQuote()->getShippingAddressByCustomerAddressId($addressId);
            $quoteAddress->setCollectShippingRates(true)->importCustomerAddressData($address);
            $this->totalsCollector->collectAddressTotals($this->getQuote(), $quoteAddress);
            $this->quoteRepository->save($this->getQuote());
        }
        return $this;
    }
}
