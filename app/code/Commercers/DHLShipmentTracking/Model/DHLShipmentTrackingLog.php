<?php
namespace Commercers\DHLShipmentTracking\Model;

class DHLShipmentTrackingLog extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Commercers\DHLShipmentTracking\Model\ResourceModel\DHLShipmentTrackingLog'); /*định nghĩa lớp ResourceModel liên kết*/
    }
}
