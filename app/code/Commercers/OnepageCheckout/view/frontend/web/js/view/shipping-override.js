/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/shipping-rate-service',
    'mage/url',
    'Magento_Checkout/js/action/select-shipping-method'
], function (
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    rateRegistry,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t,
    setShippingInformationAction,
    shippingRateService,
    urlBuilder,
    selectShippingMethodAction
) {
    'use strict';

    var popUp = null;

    return Component.extend({
        defaults: {
            template: 'Commercers_OnepageCheckout/address',
            shippingMethodListTemplate: 'Magento_Checkout/shipping-address/shipping-method-list',
            shippingMethodItemTemplate: 'Magento_Checkout/shipping-address/shipping-method-item',
            shippingFormTemplate: 'Commercers_OnepageCheckout/shipping-address/form'
        },
        visible: ko.observable(!quote.isVirtual()),
        errorValidationMessage: ko.observable(false),
        isCustomerLoggedIn: customer.isLoggedIn,
        isFormPopUpVisible: formPopUpState.isVisible,
        isFormInline: addressList().length === 0,
        isNewAddressAdded: ko.observable(false),
        saveInAddressBook: 0,
        quoteIsVirtual: quote.isVirtual(),
        selectedAddressId: ko.observable(),
        /**
         * @return {exports}
         */
        initialize: function () {
            var self = this,
                hasNewAddress,
                fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';
            this._super();

            self.searchInputEnter();

            if (!quote.isVirtual()) {
                stepNavigator.registerStep(
                    'shipping',
                    '',
                    $t('Shipping'),
                    this.visible, _.bind(this.navigate, this),
                    10
                );
            }
            checkoutDataResolver.resolveShippingAddress();

            hasNewAddress = addressList.some(function (address) {
                return address.getType() == 'new-customer-address'; //eslint-disable-line eqeqeq
            });

            this.isNewAddressAdded(hasNewAddress);

            this.isFormPopUpVisible.subscribe(function (value) {
                if (value) {
                    self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
                }
            });

            quote.shippingMethod.subscribe(function () {
                self.errorValidationMessage(false);
            });

            registry.async('checkoutProvider')(function (checkoutProvider) {
                var shippingAddressData = checkoutData.getShippingAddressFromData();

                if (shippingAddressData) {
                    checkoutProvider.set(
                        'shippingAddress',
                        $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                    );
                }
                checkoutProvider.on('shippingAddress', function (shippingAddrsData) {
                    checkoutData.setShippingAddressFromData(shippingAddrsData);
                });
                shippingRatesValidator.initFields(fieldsetName);
            });

            this.cartAgeStatus = ko.observable(window.checkoutConfig.cart_rating);

            return this;
        },
        submitFilterAddress: function() {
            var check = 0;
            $('.shippingaddresses-filter').each(function(i, e) {
                if($(e).val() != ''){
                    check = 1;
                    return false;
                }else{
                    check = 0;
                }
            });
            if(check == 1){
                $('.submit-filter-address').removeAttr('disabled');
            }else{
                $('.submit-filter-address').attr('disabled','disabled');
            }
        },

        searchFilterAddresses: function() {
            var self = this;
            var data = [];
            data['searchCustomerNumber'] = $('#customer-number').val();
            data['searchCustomerName'] = $('#customer-name').val();
            data['searchPostalcode'] = $('#customer-postal-code').val();
            data['searchCity'] = $('#customer-city').val();
            self.ajaxSearchResult(data);
        },

        ajaxSearchResult: function(data) {
            var url = urlBuilder.build("onepagecheckout/shippingaddress/searchresult");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'searchCustomerNumber':  data['searchCustomerNumber'],
                    'searchCustomerName':  data['searchCustomerName'],
                    'searchPostalcode':  data['searchPostalcode'],
                    'searchCity':  data['searchCity'],
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#loading-mask').show();
                },
                complete: function(){
                    $('#loading-mask').hide();
                },
                success: function (response) {
                    var html = '';
                    $('#shipping-address-select').html("");
                    if(response.message){
                        $('#sa-notfound').hide();
                        var options = response.options;
                        $.each(options, function(index) {
                            html += '<option value="'+ options[index].value +'">'+options[index].label+'</option>' ;
                        });
                    }else{
                        $('#sa-notfound').show();
                    }
                    $('#shipping-address-select').append(html);
                }
            });
        },

        searchInputEnter : function (){
            var self = this;
            $(document).keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                var fieldEmpty = true;
                if($('#customer-number').val() != '' || $('#customer-name').val() != '' || $('#customer-city').val() != '' || $('#customer-postal-code').val() != ''){
                    fieldEmpty = false;
                }
                if(keycode == '13' && !fieldEmpty){
                    self.searchFilterAddresses();
                }
            });

        },
        fillAddressForm: function()
        {
            var self = this;
            var addressId = $('#shipping-address-select').val();
            $('input[name="firstname"]').val('');
            $('input[name="lastname"]').val('');
            $('input[name="shipping_email"]').val('');
            $('input[name="telephone"]').val('');
            self.ajaxGetAddress(addressId);
        },
        ajaxGetAddress: function(addressId)
        {
            $("#btn-save-contact").removeAttr("disabled");
            $('#shipping-new-address-parent-form').show();
            var url = urlBuilder.build("onepagecheckout/shippingaddress/getAddress");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'addressId':  addressId
                },
                dataType: 'json',
                success: function (response) {
                    if(response.message){
                        var emailAddress = '';
                        var emailContact = '';
                        var accountAddress = '';
                        var shippingCompanyNameAddress = '';
                        var companyAddress = '';
                        var cityAddress = '';
                        var postcodeAddress = '';
                        var faxAddress = '';
                        var telephoneAddress = '';
                        var stressAddress = '';
                        var dataShipping = response.dataShipping;
                        if(dataShipping.email != ''){
                            emailAddress = dataShipping.email;
                            emailContact = dataShipping.email;
                        }
                        if(dataShipping.account != ''){
                            accountAddress = dataShipping.account;
                        }
                        if(dataShipping.shipping_company_name != ''){
                            shippingCompanyNameAddress = dataShipping.shipping_company_name;
                        }
                        if(dataShipping.company != ''){
                            companyAddress = dataShipping.company;
                        }
                        if(dataShipping.city != ''){
                            cityAddress = dataShipping.city;
                        }
                        if(dataShipping.postcode != ''){
                            postcodeAddress = dataShipping.postcode;
                        }
                        if(dataShipping.fax != ''){
                            faxAddress = dataShipping.fax;
                        }
                        if(dataShipping.telephone != ''){
                            telephoneAddress = dataShipping.telephone;
                        }
                        if(dataShipping.street != ''){
                            stressAddress = dataShipping.street;
                        }

                        $('select[name="shipping_address_select_alt2"]').find('option').remove();
                        var dataContactAccount = response.dataContactAccount;

                        if(dataContactAccount != "" && dataContactAccount.length > 1){
                            $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                value: 0,
                                text : "Bitte wählen"
                            }));
                            var existPrimary = 0;
                            $.each(dataContactAccount, function (i, item) {
                                if(item['is_primary'] == 1){
                                    $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                        value: item['salesforceintegrationsuite_contact_id'],
                                        text : item['firstname'] + " " + item['lastname'],
                                        selected:true
                                    }));
                                    if(item['email'] != ''){
                                        $('input[name="shipping_email"]').val(item['email']).change();
                                        $('input[id="emails-id"]').val(item['email']).change();
                                        $('input[name="firstname"]').val(item['firstname']).change();
                                        $('input[name="lastname"]').val(item['lastname']).change();
                                        existPrimary = 1;
                                    }
                                    if(item['phone'] != null){
                                        $('input[name="telephone"]').val(item['phone']).change();
                                    }

                                    if(item['shipping_company_name'] != null){
                                        $('input[name="company"]').val(item['shipping_company_name']).change();
                                    }

                                }else{
                                    $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                        value: item['salesforceintegrationsuite_contact_id'],
                                        text : item['firstname'] + " " + item['lastname']
                                    }));
                                }
                                $('select[name="shipping_address_select_alt2"]').change(function (e) {
                                    $("#btn-save-contact").removeAttr("disabled");
                                    var salesforceintegrationsuiteContactId = $(this).val();
                                    if(salesforceintegrationsuiteContactId == 0){
                                        $('input[name="shipping_email"]').val(emailAddress).change();
                                        $('input[id="emails-id"]').val(emailAddress).change();
                                    }
                                    if(salesforceintegrationsuiteContactId == item['salesforceintegrationsuite_contact_id']){
                                        $('input[name="firstname"]').val(item['firstname']).change();
                                        $('input[name="lastname"]').val(item['lastname']).change();
                                        $('input[name="shipping_email"]').val(item['email']).change();
                                        $('input[id="emails-id"]').val(item['email']).change();
                                        if(item['phone'] != null)
                                            $('input[name="telephone"]').val(item['phone']).change();
                                        if(item['shipping_company_name'] != null)
                                            $('input[name="company"]').val(item['shipping_company_name']).change();
                                    }
                                });
                            });
                        }else if(dataContactAccount != "" && dataContactAccount.length == 1){
                            var existPrimary = 0;
                            $.each(dataContactAccount, function (i, item) {
                                if(item['email'] != '')
                                    emailContact = item['email'];
                                $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                    value: item['salesforceintegrationsuite_contact_id'],
                                    text : item['firstname'] + " " + item['lastname']
                                }));
                                $('input[name="firstname"]').val(item['firstname']).change();
                                $('input[name="lastname"]').val(item['lastname']).change();
                                if(item['phone'] != null)
                                    $('input[name="telephone"]').val(item['phone']).change();
                                if(item['shipping_company_name'] != null)
                                    $('input[name="company"]').val(item['shipping_company_name']).change();
                            });
                        }else{
                            $('select[name="shipping_address_select_alt2"]').find('option').remove();
                        }
                        if(dataContactAccount != ''){
                            if(emailContact != ''){
                                $('input[name="shipping_email"]').val(emailContact).change();
                                $('input[id="emails-id"]').val(emailContact).change();
                            }else if(existPrimary != 1){
                                $('input[id="emails-id"]').val(emailAddress).change();
                            }
                        }else if(existPrimary != 1){
                            $('input[id="emails_id"]').val(emailAddress).change();
                        }
                        if(dataShipping){
                            $('input[id="emails-id"]').val(emailAddress).change();
                            $('input[name="commercers_account_number"]').val(accountAddress).change();
                            $('input[id="shipping-company-name"]').val(shippingCompanyNameAddress).change();
                            $('input[name="company"]').val(companyAddress).change();
                            $('input[name="city"]').val(cityAddress).change();
                            $('input[name="street[0]"]').val(stressAddress).change();
                            $('input[name="postcode"]').val(postcodeAddress).change();
                            $('input[name="fax"]').val(faxAddress).change();
                            $('input[name="telephone"]').val(telephoneAddress).change();
                        }

                    }
                }
            });
            $('#btn-shipping-address').removeAttr('disabled');
            $('#btn-shipping-address').css('cursor','pointer');
        },

        saveContactAddress: function(){
            var self = this;
            var data = [];
            data['shippingAccount'] = $('input[name="commercers_account_number"]').val();
            data['lastname'] = $('input[name="lastname"]').val();
            data['firstname'] = $('input[name="firstname"]').val();
            data['email'] = $('input[name="shipping_email"]').val();
            self.ajaxAddressContact(data);
        },

        ajaxAddressContact: function(data) {
            var url = urlBuilder.build("salesforceintegrationsuite/contact/save");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'account_id':  data['shippingAccount'],
                    'lastname':  data['lastname'],
                    'firstname':  data['firstname'],
                    'email':  data['email']
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#loading-mask').show();
                },
                complete: function(){
                    $('#loading-mask').hide();
                },
                success: function (response) {
                    if(response.message){
                        $('#message-save-contact .text-message').html(response.message);
                        $("#message-save-contact").css("opacity",1);
                        setTimeout(function() {
                            $("#message-save-contact").css("opacity",0);
                        }, 3000);
                    }
                }
            });
        },

        /**
         * Navigator change hash handler.
         *
         * @param {Object} step - navigation step
         */
        navigate: function (step) {
            var self = this;

            if (!quote.billingAddress()) {
                step.isVisible(false);
                stepNavigator.setHash('billing');
            } else {
                step && step.isVisible(true);
            }
        },


        /**
         * Save new shipping address
         */
        // saveNewAddress: function () {
        //     var addressData,
        //         newShippingAddress;
        //
        //     this.source.set('params.invalid', false);
        //     this.triggerShippingDataValidateEvent();
        //
        //     if (!this.source.get('params.invalid')) {
        //         addressData = this.source.get('shippingAddress');
        //         // if user clicked the checkbox, its value is true or false. Need to convert.
        //         addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;
        //
        //         // New address must be selected as a shipping address
        //         newShippingAddress = createShippingAddress(addressData);
        //         selectShippingAddress(newShippingAddress);
        //
        //         checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
        //         checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
        //         this.isNewAddressAdded(true);
        //     } else {
        //         this.isNewAddressAdded(false);
        //     }
        // },
        saveNewAddress: function () {
            var addressData,
                newShippingAddress;

            this.source.set('params.invalid', false);
            this.triggerShippingDataValidateEvent();

            if (!this.source.get('params.invalid')) {
                addressData = this.source.get('shippingAddress');
                // if user clicked the checkbox, its value is true or false. Need to convert.
                addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

                // New address must be selected as a shipping address
                newShippingAddress = createShippingAddress(addressData);
                selectShippingAddress(newShippingAddress);
                checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
                this.isNewAddressAdded(true);
            }
        },

        /**
         * Shipping Method View
         */
        rates: shippingService.getShippingRates(),
        isLoading: shippingService.isLoading,
        isSelected: ko.computed(function () {
            return quote.shippingMethod() ?
                quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                null;
        }),

        /**
         * @param {Object} shippingMethod
         * @return {Boolean}
         */
        selectShippingMethod: function (shippingMethod) {
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);

            return true;
        },

        /**
         * Set shipping information handler
         */
        setShippingInformation: function () {
            var self = this;
            self.saveNewAddress();
            if (this.validateShippingInformation()) {
                quote.billingAddress(null);
                checkoutDataResolver.resolveBillingAddress();
                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var shippingAddressData = checkoutData.getShippingAddressFromData();

                    if (shippingAddressData) {
                        checkoutProvider.set(
                            'shippingAddress',
                            $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                        );
                    }
                });
                setShippingInformationAction().done(
                    function () {
                        stepNavigator.next();
                    }
                );
            }
        },
        // setShippingInformation: function () {
        //     var self = this;

        //     self.saveNewAddress();
        //     if (!this.isNewAddressAdded()) {
        //         return;
        //     }
        //     if (this.validateShippingInformation()) {
        //         setShippingInformationAction().done(
        //             function () {
        //                 stepNavigator.next();
        //             }
        //         );
        //     }
        // },

        /**
         * @return {Boolean}
         */
        validateShippingInformation: function () {
            var divisionVal = $('select[name="custom_attributes[egfra_division]"]').val(),
                firstnameVal = $('input[name="firstname"]').val(),
                lastnameVal = $('input[name="lastname"]').val(),
                street0Val = $('input[name="street[0]"]').val(),
                postcodeVal = $('input[name="postcode"]').val(),
                cityVal = $('input[name="city"]').val(),
                shippingAddress,
                addressData,
                loginFormSelector = 'form[data-role=email-with-possible-login]',
                emailValidationResult = customer.isLoggedIn(),
                field,
                country = registry.get(this.parentName + '.shippingAddress.shipping-address-fieldset.country_id'),
                countryIndexedOptions = country.indexedOptions,
                option = countryIndexedOptions[quote.shippingAddress().countryId],
                messageContainer = registry.get('checkout.errors').messageContainer;
            if (!quote.shippingMethod()) {
                this.errorValidationMessage(
                    $t('The shipping method is missing. Select the shipping method and try again.')
                );
                return false;
            }
            if (!customer.isLoggedIn()) {
                $(loginFormSelector).validation();
                emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
            }
            if (this.isFormInline) {
                // this.source.set('params.invalid', false);
                // this.triggerShippingDataValidateEvent();
                // if (emailValidationResult &&
                //     this.source.get('params.invalid')
                // ) {
                //     this.focusInvalid();
                //     return false;
                // }
                if (emailValidationResult &&
                    this.source.get('params.invalid') ||
                    !quote.shippingMethod()['method_code'] ||
                    !quote.shippingMethod()['carrier_code']
                ) {
                    this.focusInvalid();

                    return false;
                }
                shippingAddress = quote.shippingAddress();
                addressData = addressConverter.formAddressDataToQuoteAddress(
                    this.source.get('shippingAddress')
                );
                //Copy form data to quote shipping address object
                for (field in addressData) {
                    if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }
                if (customer.isLoggedIn()) {
                    shippingAddress['save_in_address_book'] = 1;
                }
                selectShippingAddress(shippingAddress);
            } else if (customer.isLoggedIn() &&
                option &&
                option['is_region_required'] &&
                !quote.shippingAddress().region
            ) {
                messageContainer.addErrorMessage({
                    message: $t('Please specify a regionId in shipping address.')
                });
                return false;
            }
            if (!emailValidationResult) {
                $(loginFormSelector + ' input[name=username]').focus();
                return false;
            }
            if(divisionVal == 0){
                return false;
            }
            if(!firstnameVal || !lastnameVal || !street0Val || !postcodeVal || !cityVal){
                return false;
            }
            return true;
        },
        /**
         * Trigger Shipping data Validate Event.
         */
        triggerShippingDataValidateEvent: function () {
            this.source.trigger('shippingAddress.data.validate');

            if (this.source.get('shippingAddress.custom_attributes')) {
                this.source.trigger('shippingAddress.custom_attributes.data.validate');
            }
        },
        canUseBillingAddress: ko.computed(function () {
            return quote.billingAddress();
        }),
    });
});
