<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/24/18
 * Time: 16:31
 */
namespace Commercers\DpdTrack\Model;

class Log extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

	const CACHE_TAG = 'commercers_dpdtrack_log';

	protected $_cacheTag = 'commercers_dpdtrack_log';

	protected $_eventPrefix = 'commercers_dpdtrack_log';

	protected function _construct()
	{
		$this->_init('Commercers\DpdTrack\Model\ResourceModel\Log');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}