<?php

namespace Commercers\DHLShipmentTracking\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

		// Version 1.0.0
		if(version_compare($context->getVersion(), '1.0.0', '<')) {
			if (!$installer->tableExists('commercers_dhlshipmenttracking_log')) {
				$table = $installer
					->getConnection()
					->newTable($installer->getTable('commercers_dhlshipmenttracking_log') )
					->addColumn(
                              'id',
                              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                              null,
                              [
                                   'identity' => true,
                                   'nullable' => false,
                                   'primary'  => true,
                                   'unsigned' => true,
                              ],
                              'ID'
                         )
                         ->addColumn(
                              'order_id',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                              255,
                              [ 'nullable => false'],
                              'Order increment ID'
                         )
                         ->addColumn(
                              'tracking_number',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                              255,
                              [],
                              'Shipment Tracking number'
                         )
                         ->addColumn(
                              'status',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                              255,
                              [],
                              'Current Status'
                         )
                         ->addColumn(
                              'created_at',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                              null,
                              [
                              'nullable' => false,
                              'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                              ],
                              'Created At'
                         )
                         ->addColumn(
                              'updated_at',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                              null,
                              [
                                   'nullable' => false,
                                   'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                              ],
                              'Updated At'
                         )
                         ->addColumn(
                              'increment_id',
                              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                              255,
                              [
                                   'nullable' => true
                              ],
                              'Increment ID'
                         )
                         ->setComment('Commercers DHL Table');
				$installer->getConnection()->createTable($table);

				$installer->getConnection()->addIndex(
					$installer->getTable('commercers_dhlshipmenttracking_log'),
					$setup->getIdxName(
						$installer->getTable('commercers_dhlshipmenttracking_log'),
						['order_id'],
						\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
					),
					['order_id'],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
				);
			}
		}
		$installer->endSetup();
	}
}