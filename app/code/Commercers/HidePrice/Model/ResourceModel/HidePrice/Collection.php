<?php

namespace Commercers\HidePrice\Model\ResourceModel\HidePrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'commercers_hideprice_collection';
	protected $_eventObject = 'hideprice_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Commercers\HidePrice\Model\HidePrice', 'Commercers\HidePrice\Model\ResourceModel\HidePrice');
	}

}