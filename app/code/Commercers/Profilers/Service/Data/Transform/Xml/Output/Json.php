<?php

namespace Commercers\Profilers\Service\Data\Transform\Xml\Output;

class Json {
    
    public function __construct(
       
        \Commercers\Profilers\Service\Data\Parser\Xml $xml    
    ) {
        
        $this->xmlService = $xml;
    }
    
    public function execute($fileName){
        
        //need to have a header
        
        $data = $this->toArray($fileName);
        //print_r($data);
        //echo ($this->xmlService->arrayToXml($data));exit;
        return $this->xmlService->arrayToXml($data);
    }
    
    public function toArray($fileName){
        $content  = file_get_contents($fileName);
        //var_dump($content);
        //echo $filename; exit;
        //return [ 'data' => json_decode($content, true)];
        return ['data' => json_decode($content, true)] ;
    }
    
    
}