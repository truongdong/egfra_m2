<?php

namespace Commercers\OrderApproval\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\ProductFactory as ProductResourceFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Supervisor extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{

    const CACHE_TAG = 'commercers_orderapproval_supervisor';

    protected $_cacheTag = 'commercers_orderapproval_supervisor';

    protected $_eventPrefix = 'commercers_orderapproval_supervisor';

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProductResourceFactory
     */
    protected $_productResourceFactory;
    protected $_date;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        ProductFactory $productFactory,
        ProductResourceFactory $productResourceFactory,
        DateTime $date,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_productResourceFactory = $productResourceFactory;
        $this->_date = $date;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    protected function _construct()
    {
        $this->_init('Commercers\OrderApproval\Model\ResourceModel\Supervisor');
    }
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
