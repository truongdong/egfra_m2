define([
    'uiRegistry',
    'uiComponent',
    'jquery',
    'ko',
    'underscore',
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    'Commercers_MultiOrderCheckout/js/checkout/steps',
], function(uiRegistry, Component, $, ko, _, $t, alert, steps) {
    'use strict';
    return Component.extend({
        isVisible: ko.observable(true),
        initialize: function () {
            this._super();
            steps.registerStep(
                'shipping',
                this.isVisible,
                _.bind(this.navigate, this),
                1
            );
        },
        navigate: function (step) {
            this.isVisible(true);
        },
        next: function () {
            var self = this;
            $.ajax({
                url : self.next_url,
                data : $(self.form).serialize(),
                type : 'post',
                dataType : 'json',
                showLoader: true,
                success: function (resp) {
                    if(resp.is_error) {
                        alert({
                            content: resp.message
                        });
                    } else {
                        steps.navigateTo('shipping-addresses');
                    }
                }
            });
        },
        back: function () {
        }
    });
});
