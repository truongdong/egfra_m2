<?php

namespace Commercers\OnepageCheckout\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('commercers_shippingaddresses')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_shippingaddresses')
            )
                ->addColumn(
                    'shippingaddresses_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'auto_increment' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'length' => 11
                    ],
                    'Identity'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 11
                    ],
                    'Store Id'
                )
                ->addColumn(
                    'account_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Account Id'
                )
                ->addColumn(
                    'account_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Account Name'
                )
                ->addColumn(
                    'shipping_city',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Shipping city'
                )
                ->addColumn(
                    'shipping_country',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Shipping country'
                )
                ->addColumn(
                    'shipping_postal_code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping postal code'
                )

                ->addColumn(
                    'shipping_region',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping region'
                )
                ->addColumn(
                    'shipping_street',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping street'
                )
                ->addColumn(
                    'shipping_telephone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping telephone'
                )
                ->addColumn(
                    'shipping_email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping email'
                )
                ->addColumn(
                    'shipping_fax',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping fax'
                )
                ->addColumn(
                    'branch_office',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Branch office'
                )
                ->addColumn(
                    'shipping_company_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping company name'
                )
                ->addColumn(
                    'shipping_delivery_contact_phone',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping delivery contact phone'
                )
                ->addColumn(
                    'shipping_delivery_times',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping delivery times'
                )
                ->addColumn(
                    'further_information',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Further information'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shippingaddresses_id',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shippingaddresses_id'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'account_id',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'account_id'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'account_name',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'account_name'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_city',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_city'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_country',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_country'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_postal_code',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_postal_code'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_street',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_street'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_region',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_region'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_telephone',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_telephone'
                )
                ->addIndex(
                    $setup->getIdxName(
                        $installer->getTable('commercers_shippingaddresses'),
                        'shipping_email',
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    'shipping_email'
                );
            $installer->getConnection()->createTable($table);
        }

        //Quote table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote'),
                'shippingaddresses_account_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'shippingaddresses account id'
                ]
            );

        //Order table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order'),
                'shippingaddresses_account_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'shippingaddresses account id'
                ]
            );

        $installer->endSetup();
    }
}
