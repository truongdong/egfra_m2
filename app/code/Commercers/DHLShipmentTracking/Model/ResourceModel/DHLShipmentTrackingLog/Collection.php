<?php
namespace Commercers\DHLShipmentTracking\Model\ResourceModel\DHLShipmentTrackingLog;

class Collection extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init("Commercers\DHLShipmentTracking\Model\DHLShipmentTrackingLog","Commercers\DHLShipmentTracking\Model\ResourceModel\DHLShipmentTrackingLog");
    }
}
