<?php
namespace Commercers\OrderApproval\Cron;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\ManagerInterface as EventManager;
class AutoInvoiceOrder
{
    const XML_INVOICE_ORDER_AFTER_XDAY = 'orderapproval/cron_auto_invoice/invoice_order_after_xday';
    const XML_IS_ENABLE_OR_DISABLE_INVOICE_ORDER_AFTER_XDAY = 'orderapproval/cron_auto_invoice/yesno_invoice_order';
    protected $_pageFactory;
    protected $_productRepository;
    protected $_dataCollectionFactory;
    protected $_scopeConfig;
    protected $_dataFactory;
    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $_order;
    protected $_orderItemFactory;
    protected $_supervisorFactory;
    protected $_orderFactory;
    protected $_helperEmailFactory;
    public function __construct(
        \Commercers\OrderApproval\Helper\EmailFactory $helperEmailFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Magento\Sales\Model\Order\ItemFactory $orderItemFactory,
        EventManager $eventManager,
        \Magento\Sales\Model\Order $order,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_helperEmailFactory = $helperEmailFactory;
        $this->_orderFactory = $orderFactory;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_orderItemFactory = $orderItemFactory;
        $this->eventManager = $eventManager;
        $this->_order = $order;
        $this->_dataFactory = $dataFactory;
        $this->resultFactory = $resultFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->_pageFactory = $pageFactory;
    }

    public function execute()
    {
        if (!$this->_scopeConfig->getValue(self::XML_IS_ENABLE_OR_DISABLE_INVOICE_ORDER_AFTER_XDAY)){
            return;
        }
        $dataCollection = $this->_dataCollectionFactory->create();
        $dataCollection->addFieldToFilter('status', array('eq' => \Commercers\OrderApproval\Model\Data::ORDER_APPROVAL_STATUS_INIT));
        $approvals = $dataCollection->getData();
        $ordersApprovals = [];
        if($approvals){
            $params = [];
            foreach($approvals as $approval){
                if($approval["order_id"]) {
                    $hours = floor((time() - strtotime($approval['last_send_reminder_at'])) / 86400);
                    if ($hours >= floor($this->_scopeConfig->getValue(self::XML_INVOICE_ORDER_AFTER_XDAY,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE))) {
                        $params[$approval["order_id"]][] = $approval["approval_code"];
                    }
                }
            }
            if($params){
                foreach ($params as $key => $val){
                    $orderId = $key;
                    $valApprovalCodes = $val;
                    $this->invoiceOrderApproval($orderId,$valApprovalCodes);
                }
            }else{
                return false;
            }
        }
    }

    public function invoiceOrderApproval($orderId,$valApprovalCodes)
    {
        $supervisorName = $this->getSalesName();
        $result = [];
        try {
            if ($valApprovalCodes) {
                $dataModel = $this->_dataFactory->create();
//                $codes = (unserialize(base64_decode($codes)));

                $approvals = $this->_dataCollectionFactory->create();
                $approvals->addFieldToFilter('order_id', array('eq' => $orderId))
                    ->addFieldToFilter('approval_code', array('in' => $valApprovalCodes ))
                    ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_INIT));

                $order = $this->_order->load($orderId);
                //Cancel Order
                if($approvals->getSize() == intval($order->getTotalQtyOrdered()) && $order->getState() == \Magento\Sales\Model\Order::STATE_NEW){
                    foreach ($approvals as $approval) {
                        // Start set comment history
                        $orderItemId = $approval->getOrderItemId();
                        $orderItem = $this->_orderItemFactory->create()->load($orderItemId);
                        $approverName = $approval->getApproverName();
                        if($supervisorId = $approval->getSupervisorId()){
                            $supervisor = $this->_supervisorFactory->create()->load($supervisorId);
                            $approverName = $supervisor->getName();
                        }
                        $shippingMethod = $approval->getShippingMethod();
                        if ($shippingMethod) {
                            $orderComment = __('Order %1 is disapproved by %2 for SKU %3 and Shipping method : %4', $order->getIncrementId(), $approverName, $orderItem->getSku(), $shippingMethod);
                        } else {
                            $orderComment = __('Order %1 is disapproved by %2 for SKU %3', $order->getIncrementId(), $approverName, $orderItem->getSku());
                        }
                        $order->addStatusHistoryComment($orderComment)->setIsVisibleOnFront(true)->setIsCustomerNotified(true)->save();
                        // End set comment history

                        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                        $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                        $approval->save();

                        //Start Sent email for buyer
                        $order = $this->_orderFactory->create()->load($orderId);
                        $approvalItem = $approval->getData();
                        $helperEmail = $this->_helperEmailFactory->create();
                        $helperEmail->sendBuyerEmail($approvalItem,$order,$typeEmail = 1,$supervisorName);
                        //End Sent email for buyer
                    }
                    try {
                        $result['approve_all'] = 6;
                        $result['order_id'] = $orderId;
                        $result['approval_code'] = $valApprovalCodes;
                        $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED;
                        $order->cancel()->save();

                        $order->addStatusHistoryComment(
                            __('Notified status of order')
                        )
                            ->setIsCustomerNotified(true)
                            ->save();
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        if ($this->_objectManager->get('Magento\Checkout\Model\Session')->getUseNotice(true)) {
                            $this->messageManager->addNotice($e->getMessage());
                        } else {
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                $storeId = $order->getStore()->getStoreId();
                $cancelledStatus = $dataModel->isXmlStatusOrderDisapproved();

                $arrApprovals = [];
                $arrApprovals['order_id'] = $orderId;
                $arrApprovals['approval_code'] = $valApprovalCodes;
                $arrApprovals['status'] = $dataModel::ORDER_APPROVAL_STATUS_INIT;

                if ($order->getStatus() == $cancelledStatus) {
                    $result['approve_all'] = 2;
                }
                if (!$approvals->getSize()) {
                    $result['approve_all'] = 3;
                } else {
                    foreach ($approvals as $approval) {
                        if ($approval->getStatus() != $dataModel::ORDER_APPROVAL_STATUS_INIT) {
                            continue;
                        } else {
                            $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                            $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                            $approval->save();
                            $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                                'order_approval' => $approval,
                                'supervisor_name' => $supervisorName
                            ]);
                        }
                    }
                    $result['approve_all'] = 5;
                }
                $result['order_id'] = $orderId;
                $result['approval_code'] = $valApprovalCodes;
                $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_INIT;
            } else{
                $result['approve_all'] = 4;
            }
        } catch (\Exception $ex) {
        }
    }

    public function getSalesName()
    {
        return $this->_scopeConfig->getValue(
            'trans_email/ident_sales/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
