<?php

namespace Commercers\MultiOrderCheckout\Block\Checkout\Steps;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Api\CustomerRepositoryInterface;
class EgfraDivision extends Template
{
    protected $_customerSession;
    protected $_customerRepository;
    protected $_optionsEgfraDivision;
    public function __construct(
        \Commercers\CheckoutCustomerAttribute\Model\Entity\Attribute\Source\Options $optionsEgfraDivision,
        CustomerRepositoryInterface $customerRepository,
        CustomerSession $customerSession,
        Context $context,
        array $data = []
    ) {
        $this->_optionsEgfraDivision = $optionsEgfraDivision;
        $this->_customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }
    public function getEgfraDivision(){
        $optionsEgfraDivision =  $this->_optionsEgfraDivision->getAllOptions();
        $customerId  = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerRepository->getById($customerId);
        $valOptionsEgfraDivision = $customer->getCustomAttribute('egfra_division')->getValue();
        $arrValOptionsEgfraDivision = explode(",",$valOptionsEgfraDivision);
        $options = [];
        foreach ($optionsEgfraDivision as $optionEgfraDivision) {
            if(in_array($optionEgfraDivision['value'],$arrValOptionsEgfraDivision)) {
                $options[] = [
                    'optionValue' => $optionEgfraDivision['value'],
                    'optionLabel' => $optionEgfraDivision['label']
                ];
            }
        }
        return $options;
    }
}
