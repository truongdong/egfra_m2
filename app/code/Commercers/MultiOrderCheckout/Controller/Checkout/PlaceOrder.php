<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Magento\Framework\Exception\LocalizedException;
use Magento\Multishipping\Model\Checkout\Type\Multishipping\State;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote\Address;

class PlaceOrder extends AbstractIndex
{
	public function execute()
	{
		try {
			if ($shipToInfo = $this->getRequest()->getPost('ship')) {
				$addressData = [];
				$quote = $this->_getCheckoutSession()->getQuote();
				foreach ($shipToInfo as $shipInfo) {
					foreach ($quote->getAllItems() as $item) {
						$itemQty = $item->getQty();
						$addressData[$shipInfo['address_id']][$item->getId()] = [
							'qty' => $itemQty,
							'address' => $shipInfo
						];
					}

				}

				$this->_getCheckout()->setShippingItemsInformation($addressData);
			}

			$methods = $this->getRequest()->getPost('methods');
			$shippingMethods = [];
			foreach ($this->_getCheckoutSession()->getQuote()->getAddressesCollection() as $address) {
				if (!$address->isDeleted() &&
				    $address->getAddressType() == Address::TYPE_SHIPPING
				) {
					foreach ($methods as $method) {
						if($address->getCommercersShippingaddressId() == $method['addressId']) {
							$shippingMethods[$address->getId()] = $method['method'];
						}
					}
				}
			}
			$this->_getCheckout()->setShippingMethods($shippingMethods);

			$payment['method'] = 'banktransfer';
			$this->_getCheckout()->setPaymentMethod($payment);
			$this->_getCheckout()->createOrders();

			$result = [
				'is_error' => false,
				'redirect' => $this->_url->getUrl('multiorder/checkout/success')
			];
		} catch (\Magento\Framework\Exception\ValidatorException $e)  {
			$addressIds = $e->getParameters();
			$result = [
				'is_error' => true,
				'error' => 'address_validate',
				'address_id' => $addressIds,
				'message' =>  $e->getMessage()
			];
		} catch (\Magento\Framework\Exception\LocalizedException $e) {
			$result = [
				'is_error' => true,
				'message' =>  $e->getMessage()
			];
		} catch (\Exception $e) {
			$result = [
				'is_error' => true,
				'message' =>  __('Data saving problem'),
				'redirect' => $this->_url->getUrl('checkout/cart')
			];
		}

		return $this->_returnResultJson($result);
	}
}