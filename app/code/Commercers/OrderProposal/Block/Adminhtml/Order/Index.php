<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Block\Adminhtml\Order;

class Index extends \Magento\Backend\Block\Widget\Form\Container{
    public function __construct(\Magento\Backend\Block\Widget\Context $context, array $data = array()) {
        parent::__construct($context, $data);
    }
    public function _construct() {
        $this->setId('order_proposal_index');
        $this->buttonList->update('save', 'label', __('Submit Order'));
    }
}