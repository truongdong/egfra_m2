<?php
/**
 * Commercers Vietnam
 */
namespace Commercers\OnepageCheckout\Service\Checkout;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Customer\Model\SessionFactory as CustomerSessionFactory;
use Magento\Customer\Model\CustomerFactory;
USE Commercers\OnepageCheckout\Helper\Data;

class ConfigProvider implements ConfigProviderInterface{

    /**
     * @var CustomerSessionFactory
     */
    protected $_customerSession;

    /**
     * @var ScopeConfigInterface
     */
    protected $config;

    /**
     * @var LayoutInterface
     */
    protected $layout;
    protected $_customerFactory;

    public function __construct(
        CustomerSessionFactory $customerSession,
        ScopeConfigInterface $config,
        LayoutInterface $layout,
        CustomerFactory $customerFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->config = $config;
        $this->layout = $layout;
        $this->_customerFactory = $customerFactory;
    }

    public function getConfig()
    {
        $customer = $this->_customerFactory->create()->load($this->_customerSession->create()->getCustomerId());
        $attribute = $customer->getData(Data::EGFRA_DIVISION);

        $attribute = array();
        $attribute[] = array('label' => '123', 'value' => '456');
        $attribute[] = array('label' => '789', 'value' => '111');
        $result[] = $attribute;
//        var_dump($attribute);exit;
        return [
            'egfra_division' => $attribute
        ];
    }

}
