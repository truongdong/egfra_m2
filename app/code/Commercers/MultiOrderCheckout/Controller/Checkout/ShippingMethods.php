<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Item;

class ShippingMethods extends AbstractIndex
{
	public function execute()
	{
		try {
			$address = $this->_request->getParam('address');
			$shippingMethods = $this->_shippingMethods->getShippingMethods($address);

			$result = [
				'is_error' => false,
				'methods' => $shippingMethods
			];
		} catch (LocalizedException $e) {
			$result = [
				'is_error' => true,
				'message' => $e->getMessage()
			];
		} catch (\Exception $e) {
			$this->logger->critical($e->getMessage());
			$result = [
				'is_error' => true,
				'message' => __('We are unable to process your request. Please, try again later.')
			];
		}

		return $this->_returnResultJson($result);
	}
}