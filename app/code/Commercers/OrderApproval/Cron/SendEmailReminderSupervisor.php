<?php
namespace Commercers\OrderApproval\Cron;

class SendEmailReminderSupervisor
{
    const XML_REMINDER_DELAY = 'orderapproval/cron_send_reminder_email/reminder_delay';
    const XML_IS_SEND_REMINDER = 'orderapproval/cron_send_reminder_email/send_reminder';

    protected $_orderCollectionFactory;
    protected $_dataCollectionFactory;
    protected $_helperEmailFactory;
    protected $_orderFactory;
    protected $_scopeConfig;
    protected $_helperDataFactory;
    public function __construct(
        \Commercers\OrderApproval\Helper\DataFactory $helperDataFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Commercers\OrderApproval\Helper\EmailFactory $helperEmailFactory,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Action\Context $context
    )
    {
        $this->_helperDataFactory = $helperDataFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderFactory = $orderFactory;
        $this->_helperEmailFactory = $helperEmailFactory;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }

    public function execute()
    {
        $dataCollection = $this->_dataCollectionFactory->create();
        $dataCollection->addFieldToFilter('status', array('eq' => \Commercers\OrderApproval\Model\Data::ORDER_APPROVAL_STATUS_INIT));
        $approvals = $dataCollection->getData();
        $reminderApprovals = [];
        $orders = [];
        foreach($approvals as $approval){
            if($approval["order_id"]){
                if (!$this->_scopeConfig->getValue(self::XML_IS_SEND_REMINDER)){
                    continue;
                }
                try {
                    $hours = floor((time() - strtotime($approval['last_send_reminder_at'])) / 86400);
                    if ($hours >= floor($this->_scopeConfig->getValue(self::XML_REMINDER_DELAY))) {
                        if (!isset($reminderApprovals[$approval["order_id"]])) {
                            $reminderApprovals[$approval["order_id"]] = array();
                        }
                        $reminderApprovals[$approval["order_id"]][] = $approval;
                    }
                } catch (Exception $ex) {
                    echo $ex->getMessage();exit;
                }
            }
            // Do your email code
        }

//        foreach ($reminderApprovals as $orderId => $approvalItems) {
//            $order = $this->_orderFactory->create()->load($orderId);
//            $helperEmail = $this->_helperEmailFactory->create();
//
//            foreach ($approvalItems as $approvalItem) {
//                // Set Data Order Necessary
//                $helperEmail->sendReminderEmail($approvalItem,$order,$typeEmail = 1);
//            }
//
//        }
        foreach ($reminderApprovals as $orderId => $approvalItems) {
            $order = $this->_orderFactory->create()->load($orderId);
            foreach ($approvalItems as $approvalItem) {

                $supervisorInformation = [];
                $arrApprovalName = explode(",", $approvalItem["approval_name"]);
                $arrApprovalEmail = explode(",", $approvalItem["approval_email"]);
                foreach ($arrApprovalEmail as $key => $approvalEmail){
                    $supervisorInformation["email"] = $approvalEmail;
                    $supervisorInformation["name"] = $arrApprovalName[$key];
                    $helperEmail = $this->_helperEmailFactory->create();
                    $helperEmail->sendReminderEmail($approvalItem,$order,$typeEmail = 2,$supervisorInformation["name"],$supervisorInformation["email"]);
                }
            }

        }
        return;
    }
}
