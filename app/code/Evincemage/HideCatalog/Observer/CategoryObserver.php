<?php
namespace Evincemage\HideCatalog\Observer;

use Magento\Framework\Event\ObserverInterface;

class CategoryObserver implements ObserverInterface
{
    /**
     * Add filter to product collection
     *
     * @param EventObserver $observer
     * @return void
     */
    protected $response;
    public function __construct(\Magento\Framework\App\Response\Http $response, \Evincemage\HideCatalog\Helper\Data $helper)
    {
        $this->helper   = $helper;
        $this->response = $response;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $CustomerGroupID = $this->helper->getCustomerGroupId();
        $category        = $observer->getEvent()->getCategory();
        if ($this->helper->isModuleActive()) {
            $customeSetting = $category->getHideCategoryGroups();
            if ($category->getHideCategoryGroups()) {
                if ($this->helper->CategoryLoadVisible() || (in_array($CustomerGroupID, $customeSetting) && !in_array(\Evincemage\HideCatalog\Helper\Data::USE_NONE, $customeSetting))) {
                    $this->response->setRedirect($this->helper->catalogRedirect());
                }
            }else{
              if ($this->helper->CategoryLoadVisible()) {
                    $this->response->setRedirect($this->helper->catalogRedirect());
                }  
            }
        }
    }
} 