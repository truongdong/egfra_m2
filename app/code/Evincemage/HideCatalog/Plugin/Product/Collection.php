<?php
namespace Evincemage\HideCatalog\Plugin\Product;

class Collection
{

	public function __construct(
		\Magento\Framework\App\Response\Http $response,
		\Evincemage\HideCatalog\Helper\Data $helper,
		\Magento\Catalog\Model\ResourceModel\Eav\Attribute $eavAttribute,
		\Magento\Framework\App\ResourceConnection $resource
	)
	{
		$this->helper = $helper;
		$this->response = $response;
		$this->_eavAttribute = $eavAttribute;
		$this->resource = $resource;
	}
	public function aroundSetOrder(\Magento\Catalog\Model\ResourceModel\Product\Collection $collection, \Closure $proceed)
	{
		$CustomerGroupID = $this->helper->getCustomerGroupId();
		if ($this->helper->isModuleActive()) {
			if ($this->helper->CatalogLoadVisible()) {
				$CustomerGroupID = $this->helper->ConfigProductCollectionsetting();
				$collection->addAttributeToFilter('entity_id', array(
					'eq' => 0
				));
			} else {

				$attribute = $this->_eavAttribute->loadByCode('catalog_product','hide_product_groups');
				$table = $attribute->getBackend()->getTable();
				$select = $this->resource->getConnection()->select();
				$select->from($table,'entity_id')
				       ->where('attribute_id = ? AND FIND_IN_SET('.$CustomerGroupID.',value)', $attribute->getId());

				$result = $this->resource->getConnection()->fetchCol($select);
				// echo "<pre>";print_r( $result);exit;
				if($result){
					$collection->addAttributeToFilter('entity_id',array('nin'=>$result));
				}
			}

		}
		return $collection;
	}
} 