<?php

/**
 *  Commercers Vietnam
 *  Nhan Dep Trai
 */

namespace Commercers\Receiving\Service;

use Magento\Framework\Event\ObserverInterface;

class Receiving implements ObserverInterface {

    const USE_SECOND_ATTRIBUTE = 'receiving/general/use_second_attribute';
    const SECOND_ATTRIBUTE = 'receiving/general/second_attribute';
    const SET_STATUS_TO_ACTIVE = 'receiving/general/set_status_to_active';
    const NEGATIVE_STOCK_TEXT = 'receiving/general/negative_stock_text';
    const MESSAGE_IF_STOCK_WAS_NEGATIVE = 'receiving/general/message_if_stock_was_negative';
    const SET_PRODUCT_IN_STOCK = 'receiving/general/set_product_in_stock';
    const NEGATIVE_STOCK_FORMAT = 'receiving/general/negative_stock_format';
    const PRINTED_ATTRIBUTE = 'receiving/general/printed_attribute';

    protected $_stocklogFactory;
    protected $product;
    protected $_productFactory;
    protected $_stockRegistry;
    private $eventManager;
    protected $messageManager;
    private $responseFactory;
    private $url;
    protected $resource;
    protected $helperData;
    protected $scopeConfig;
    protected $fileFactory;

    public function __construct(
    \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Backend\Model\Auth\Session $authSession, \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Commercers\Receiving\Helper\Data $helperData, \Magento\Catalog\Model\Product $product, \Magento\Framework\App\ResourceConnection $resource, \Magento\Framework\Event\Manager $eventManager, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \Magento\Framework\Registry $registry
    ) {
        $this->_stockRegistry = $stockRegistry;
        $this->authSession = $authSession;
        $this->scopeConfig = $scopeConfig;
        $this->fileFactory = $fileFactory;
        $this->_productFactory = $productFactory;
        $this->messageManager = $messageManager;
        $this->eventManager = $eventManager;
        $this->helperData = $helperData;
        $this->resource = $resource;
        $this->product = $product;
        $this->registry = $registry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $this->registry->register('stop_log_event', 1);
        $chooseSecondAttribute = $this->scopeConfig->getValue(self::USE_SECOND_ATTRIBUTE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $secondAttribute = $this->scopeConfig->getValue(self::SECOND_ATTRIBUTE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $setStatusToActive = $this->scopeConfig->getValue(self::SET_STATUS_TO_ACTIVE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $messageIfStockWasNegative = $this->scopeConfig->getValue(self::MESSAGE_IF_STOCK_WAS_NEGATIVE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $negativeStockText = $this->scopeConfig->getValue(self::NEGATIVE_STOCK_TEXT, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $setProductInStock = $this->scopeConfig->getValue(self::SET_PRODUCT_IN_STOCK, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        $printedAttribute = $this->scopeConfig->getValue(self::PRINTED_ATTRIBUTE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        if ($chooseSecondAttribute == 0) {
            $attr = $observer->getAttr();
            $attrSelect = $observer->getAttrselect();
            $getValues = $this->_productFactory->create()->loadByAttribute($attrSelect, $attr);
            // var_dump($getValues->getData('status'));exit;
            if ($getValues == false) {
                $this->messageManager->addError(__('Can not find a product for ' . $attrSelect . ': ' . $attr . ''));
            }
            else {
                $skuProduct = $getValues->getData('sku');
                if ($attr == '') {
                    $this->messageManager->addError(__('SKU is not empty'));
                }
                else {
                    $this->save($observer, $skuProduct, $attrSelect, $setStatusToActive, $messageIfStockWasNegative, $negativeStockText, $setProductInStock);
                }
            }
        }
        else {
            $secondAttribute = $this->scopeConfig->getValue(self::SECOND_ATTRIBUTE, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $attr = $observer->getAttr();
            $attrSelect = $observer->getAttrselect();

            $getValues = $this->_productFactory->create()->loadByAttribute($attrSelect, $attr);

            if ($getValues == false) {
                $getValues = $this->_productFactory->create()->loadByAttribute($secondAttribute, $attr);
                if ($getValues == false) {
                    $this->messageManager->addError(__('Can not find a product for ' . $attrSelect . '/' . $secondAttribute . ': ' . $attr . ''));
                }
                else {
                    $skuProduct = $getValues->getData('sku');
                    if ($attr == '') {
                        $this->messageManager->addError(__('SKU is not empty'));
                    }
                    else {
                        $this->save($observer, $skuProduct, $attrSelect, $setStatusToActive, $messageIfStockWasNegative, $negativeStockText, $setProductInStock);
                    }
                }
            }
            else {
                $skuProduct = $getValues->getData('sku');
                if ($attr == '') {
                    $this->messageManager->addError(__('SKU is not empty'));
                }
                else {
                    $this->save($observer, $skuProduct, $attrSelect, $setStatusToActive, $messageIfStockWasNegative, $negativeStockText, $setProductInStock);
                }
            }
        }
    }

    public function save(\Magento\Framework\Event\Observer $observer, $skuProduct, $attrSelect, $setStatusToActive, $messageIfStockWasNegative, $negativeStockText, $setProductInStock) {
        $attr = $observer->getSku();
        $qty = $observer->getQty();
        $orderNumber = $observer->getOrdernumber();
        $deliOrderNumber = $observer->getDeliordernumber();
        try {
            $stockItem = $this->_stockRegistry->getStockItemBySku($skuProduct);
            $intQty = (int) $qty;
            $floatQty = (float) $qty;
            $qty = ($intQty == $floatQty) ? $intQty : $floatQty;
            if ($stockItem->getIs_qty_decimal() == 0 && is_float($qty)) {
                $this->messageManager->addError(__('Quantity is integer only'));
            }
            else {
                $id = $stockItem->getId();
                $product = $this->_productFactory->create()->load($id);
//                print_r($product->getData());exit;
                if ($setStatusToActive == 1 || $setProductInStock == 1) {
                    if ($setStatusToActive == 1 && $setProductInStock == 1) {
                        $this->setStatusToActive($product);
                        $this->setProductInStock($product);
                        return $this->saveProduct($observer, $product, $stockItem, $qty, $attr, $orderNumber, $deliOrderNumber, $messageIfStockWasNegative, $negativeStockText);
                    }
                    if ($setStatusToActive == 1) {
                        $this->setStatusToActive($product);
                        return $this->saveProduct($observer, $product, $stockItem, $qty, $attr, $orderNumber, $deliOrderNumber, $messageIfStockWasNegative, $negativeStockText);
                    }
                    if ($setProductInStock == 1) {
                        $this->setProductInStock($product);
                        return $this->saveProduct($observer, $product, $stockItem, $qty, $attr, $orderNumber, $deliOrderNumber, $messageIfStockWasNegative, $negativeStockText);
                    }
                }
                else {
                    $this->saveProduct($observer, $product, $stockItem, $qty, $attr, $orderNumber, $deliOrderNumber, $messageIfStockWasNegative, $negativeStockText);
                }
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->messageManager->addError(__('Can not find a product for ' . $attrSelect . ': ' . $attr . ''));
        }
    }

    public function setStatusToActive($product) {
        $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $product->save();
    }

    public function setProductInStock($product) {
        $stockItem = $this->_stockRegistry->getStockItemBySku($product->getSku());
        $stockItem->setData('is_in_stock', '1'); // lấy qty cũ
        $stockItem->save();
    }

    public function saveProduct(\Magento\Framework\Event\Observer $observer, $product, $stockItem, $qty, $attr, $orderNumber, $deliOrderNumber, $messageIfStockWasNegative, $negativeStockText) {
        $attr = $observer->getAttr();
        $nameProduct = $product->getName();
        $stockQty = $stockItem->getQty();
        $orderQty = $stockQty + $qty;
        $stockItem->setQty($orderQty);
        $diff = $qty;
        $stockItem->save();
        //Send data Events
        $eventData = array(
            'product' => $product,
            'oldcount' => $stockQty,
            'newcount' => $orderQty,
            'sku' => $attr,
            'diff' => $diff,
            'ordernumber' => $orderNumber,
            'delivernoteid' => $deliOrderNumber,
            'type' => 1,
            'user'=>$this->authSession->getUser()->getUsername(),
            'posex' => '00010',
            'is_import' => 0
        );
        $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);
        //Messege
       //print_r($product->getData('quantity_and_stock_status'));exit;

        $this->helperData->getProductAttributeShowOnSuccess($product);
        if ($orderQty <= 0 && $messageIfStockWasNegative == 1) {
            if ($negativeStockText) {
                $this->messageManager->addWarning(__($negativeStockText));
                $this->messageManager->addSuccess(__('Product: ' . $nameProduct . '-----------Entered Number: ' . $qty . '----------Current Magento stock: ' . $orderQty . ''));
            }
            else {
                $this->messageManager->addSuccess(__('Product: ' . $nameProduct . '-----------Entered Number: ' . $qty . '----------Current Magento stock: ' . $orderQty . ''));
            }
        }
        else {
            $this->messageManager->addSuccess(__('Product: ' . $nameProduct . '-----------Entered Number: ' . $qty . '----------Current Magento stock: ' . $orderQty . ''));
        }
    }

    public function printedAttribute(\Magento\Framework\Event\Observer $observer, $product) {
        $nameProduct = $product->getName();
        $qty = $observer->getQty();
        $orderNumber = $observer->getOrdernumber();
        $deliOrderNumber = $observer->getDeliordernumber();
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
        $style->setFont($font, 15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 30;
        $pageTopalign = 850; //default PDF page height
        $this->y = 850 - 100; //print table row from page top – 100px
        //Draw table header row’s
        $style->setFont($font, 16);
        $page->setStyle($style);
        $page->drawRectangle(30, $this->y + 10, $page->getWidth() - 30, $this->y + 70, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $style->setFont($font, 15);
        $page->setStyle($style);
        $page->drawText(__("Cutomer Details"), $x + 5, $this->y + 50, 'UTF-8');
        $style->setFont($font, 11);
        $page->setStyle($style);
        $page->drawText(__("Name : %1", "John Smith"), $x + 5, $this->y + 33, 'UTF-8');
        $page->drawText(__("Email : %1", "test@example.com"), $x + 5, $this->y + 16, 'UTF-8');

        $style->setFont($font, 12);
        $page->setStyle($style);
        $page->drawText(__("PRODUCT_NAME"), $x + 30, $this->y - 10, 'UTF-8');
        $page->drawText(__("COUNT"), $x + 150, $this->y - 10, 'UTF-8');
        $page->drawText(__("ORDER_NUMBER"), $x + 250, $this->y - 10, 'UTF-8');
        $page->drawText(__("DELIVERY_NOTE_ID"), $x + 380, $this->y - 10, 'UTF-8');
        $page->drawText(__("TEST : "), $x + 30, $this->y - 80, 'UTF-8');

        $style->setFont($font, 10);
        $page->setStyle($style);
        $page->drawText($nameProduct, $x + 30, $this->y - 30, 'UTF-8');
        $page->drawText($qty, $x + 150, $this->y - 30, 'UTF-8');
        $page->drawText($orderNumber, $x + 250, $this->y - 30, 'UTF-8');
        $page->drawText($deliOrderNumber, $x + 380, $this->y - 30, 'UTF-8');
        $page->drawText('SKU', $x + 70, $this->y - 80, 'UTF-8');
        $page->drawRectangle(30, $this->y - 62, $page->getWidth() - 30, $this->y + 10, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(30, $this->y - 62, $page->getWidth() - 30, $this->y - 100, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $style->setFont($font, 15);
        $page->setStyle($style);
        $page->drawText(__("CommercersVN"), ($page->getWidth() / 2) - 50, $this->y - 200);

        $fileName = 'CommercersReceiving.pdf';

        $this->fileFactory->create(
                $fileName, $pdf->render(), \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
                'application/pdf'
        );
    }

}
