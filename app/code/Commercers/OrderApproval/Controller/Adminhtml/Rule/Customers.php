<?php

namespace Commercers\OrderApproval\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\Registry;

use Commercers\OrderApproval\Model\SupervisorFactory;

class Customers extends Action
{
    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var SupervisorFactory
     */
    protected $_supervisorFactory;


    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        SupervisorFactory $supervisorFactory,
        Context $context,
        LayoutFactory $resultLayoutFactory,
        Registry $registry
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_registry = $registry;
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $getParams = $this->getRequest()->getParams();
        $supervisorId = $this->getRequest()->getParam('supervisor_id');
        $modelSupervisor = $this->_supervisorFactory->create();

        if ($supervisorId) {
            $modelSupervisor->load($supervisorId);
        }

        $this->_registry->register('approval_supervisor_data', $modelSupervisor);
        $resultPage = $this->resultPageFactory->create();

        $idx = $getParams['idx'];
        $block = $resultPage->getLayout()
            ->getBlock('admin.approval.rule.customer.data')
            ->setData('idx',$idx)
            ->setData('supervisor_id',$supervisorId);
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        /* @var $serializer \Magento\Backend\Block\Widget\Grid\Serializer */
        $serializer = $this->_view->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Grid\Serializer::class,
            'customers_grid_serializer',
            [
                'data' =>[
//                    'input_names' => 'in_customers',
                    'grid_block' => $block,
                    'callback' => 'getSelectedCustomers',
                    'input_element_name' => 'customers_grid_value_'.$idx,
                    'reload_param_name' => 'customers_grid_value_'.$idx
                ]
            ]
        );
        try {
            $response = ['success' => 'true',
                'grid' => $block->toHtml().$serializer->toHtml()
            ];
        } catch (\Exception $e) {
            $response = ['error' => 'true', 'message' => $e->getMessage()];
        }
        return $resultJson->setData($response);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Commercers_OrderApproval::index');
    }

}
