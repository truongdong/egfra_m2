<?php

namespace Commercers\MultiOrderCheckout\Model;

class AddressList extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

	const CACHE_TAG = 'commercers_multiorder_checkout_addresslist';

	protected $_cacheTag = 'commercers_multiorder_checkout_addresslist';

	protected $_eventPrefix = 'commercers_multiorder_checkout_addresslist';

	public function __construct(
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$resource,
			$resourceCollection
		);
	}

	protected function _construct() {
		$this->_init('Commercers\MultiOrderCheckout\Model\ResourceModel\AddressList');
	}

	public function getIdentities() {
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues() {
		$values = [];

		return $values;
	}
}