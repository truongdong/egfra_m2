<?php

namespace Commercers\HidePrice\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Controller\Result\JsonFactory;
use Commercers\HidePrice\Model\HidePriceFactory;
class Index extends Action {
	/**
	 * @var CustomerSession
	 */
	protected $_customerSession;

	/**
	 * @var JsonFactory
	 */
	protected $_resultJsonFactory;

    /**
	 * @var HidePriceFactory
	 */
	protected $_hidePriceFactory;

	public function __construct(
		Context $context,
		CustomerSession $customerSession,
		JsonFactory $resultJsonFactory,
        HidePriceFactory $hidePriceFactory
	) {
		$this->_customerSession = $customerSession;
		$this->_resultJsonFactory = $resultJsonFactory;
        $this->_hidePriceFactory = $hidePriceFactory;

		return parent::__construct($context);
	}

    public function execute() {
        $isHidePrice = $this->getRequest()->getParam('hide_price');
        $customerId = $this->_customerSession->getCustomerId();
        $hidePrice = $this->_hidePriceFactory->create()->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
        ;
        if($hidePrice->count()) {
            $hidePrice = $hidePrice->getFirstItem();
        } else {
            $hidePrice = $this->_hidePriceFactory->create();
        }
        try {
            $hidePrice->setHidePrice($isHidePrice);
            $hidePrice->setCustomerId($customerId);
            $hidePrice->save();
            $result = [
                'is_error' => false,
                'message' => __('Success')
            ];
        } catch(\Exception $e) {
            $result = [
                'is_error' => true,
                'message' => $e->getMessage()
            ];
        }

        return $this->_returnResultJson($result);
    }

	protected function _returnResultJson($result){
		return $this->_resultJsonFactory->create()->setData($result);
	}
}