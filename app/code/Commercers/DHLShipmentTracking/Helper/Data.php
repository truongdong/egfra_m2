<?php

namespace Commercers\DHLShipmentTracking\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper {

	public function isEnabled()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/enable');
     }
     
     public function getCredentials()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/credentials');
     }

     public function getCigUser()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/cig_user');
     }

     public function getCigPassword()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/cig_password');
     }

     public function getTntUser()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/tnt_user');
     }

     public function getTntPassword()
	{
		return $this->getConfigValue('dhl_shipment_tracking/general/tnt_password');
	}
	
	public function getOrderConfigStatus($statusCode = null){
		return $this->getConfigValue('dhl_shipment_tracking/general/'.strtolower($statusCode));
	}

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}


}