<?php
namespace Evincemage\HideCatalog\Model\Config\Source;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
class HiddenEntityHandling
{
    const HIDDEN_ENTITY_HANDLING_NOROUTE = '404';
    const HIDDEN_ENTITY_HANDLING_REDIRECT = '302';
    
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::HIDDEN_ENTITY_HANDLING_NOROUTE,
                'label' => __('Show 404 Page')
            ),
            array(
                'value' => self::HIDDEN_ENTITY_HANDLING_REDIRECT,
                'label' => __('Redirect to target route')
            )
        );
    }
}