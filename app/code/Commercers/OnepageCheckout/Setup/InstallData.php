<?php
namespace Commercers\OnepageCheckout\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerMetadataInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'shipping_email',
            [
                'type'         => 'varchar',
                'label'        => 'Email Address',
                'input'        => 'text',
                'required'     => false,
                'visible'      => true,
                'user_defined' => true,
                'position'     => 999,
                'system'       => 0,
            ]
        );
        $sampleAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'shipping_email');
        // more used_in_forms ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']
        $sampleAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $sampleAttribute->save();

        $eavSetup2 = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup2->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'shipping_account_number',
            [
                'type'         => 'varchar',
                'label'        => 'shipping account number',
                'input'        => 'text',
                'required'     => false,
                'visible'      => true,
                'user_defined' => true,
                'position'     => 999,
                'system'       => 0,
            ]
        );
        $sampleAttribute2 = $this->eavConfig->getAttribute(Customer::ENTITY, 'shipping_account_number');
        // more used_in_forms ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']
        $sampleAttribute2->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $sampleAttribute2->save();

        $eavSetup3 = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup3->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'shipping_address_select_alt2',
            [
                'type'         => 'varchar',
                'label'        => 'shipping address select alt2',
                'input'        => 'text',
                'required'     => false,
                'visible'      => true,
                'user_defined' => true,
                'position'     => 999,
                'system'       => 0,
            ]
        );
        $sampleAttribute3 = $this->eavConfig->getAttribute(Customer::ENTITY, 'shipping_address_select_alt2');
        // more used_in_forms ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']
        $sampleAttribute3->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $sampleAttribute3->save();
        $setup->endSetup();
    }
}
