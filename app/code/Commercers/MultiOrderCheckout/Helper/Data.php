<?php

namespace Commercers\MultiOrderCheckout\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

	public function __construct(
		Context $context
	) {
		parent::__construct($context);
	}
}