<?php

namespace Commercers\ExportInvoice\Controller\Adminhtml\Manual;

class Export extends \Magento\Backend\App\Action {

    protected $_invoiceCollectionFactory;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_invoiceCollectionFactory = $invoiceCollectionFactory;
        parent::__construct($context);
    }

    public function execute() {
        $invoiceCollection = $this->_getInvoiceCollection();
        $arrInvoice = [];
        foreach ($invoiceCollection as $invoice){
            $invoiceId = $invoice["entity_id"];
            $incrementId = $invoice["increment_id"];
            $orderId = $invoice["order_id"];
            $arrInvoice[$invoiceId]["incrementId"] = $incrementId;
            $arrInvoice[$invoiceId]["orderId"] = $orderId;
        }
        var_dump($arrInvoice);exit;

//        $invoiceCollection = $this->_getInvoiceCollection();
//        var_dump($invoiceCollection);exit;
    }

    protected function _getInvoiceCollection(){
        $collection = $this->_invoiceCollectionFactory->create();
        $collection->addAttributeToSelect('*');
//        echo $collection->getSelect();exit;
        return $collection->getData();
    }
}