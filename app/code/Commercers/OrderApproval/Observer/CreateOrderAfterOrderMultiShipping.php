<?php

namespace Commercers\OrderApproval\Observer;
use Commercers\OrderApproval\Model\ResourceModel\Supervisor\Collection;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Captcha\Observer\CaptchaStringResolver;
use Magento\Framework\Exception\LocalizedException;

class CreateOrderAfterOrderMultiShipping implements ObserverInterface
{
    const APPROVAL_TYPE_NORMAL = 0;
    const  APPROVAL_TYPE_CHECK = 1;
    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $order;
    /*
    * @var  \Commercers\OrderApproval\Helper\DataFactory $helperDataFactory
     */
    protected $_helperDataFactory;
    /*
    * @var      \Magento\Customer\Model\Session $customerSession
    */
    protected $_customerSession;
    /**
     * Rule Model
     *
     * @var \Commercers\OrderApproval\Model\RuleFactory
     */
    protected $_ruleFactory;
    /**
     * Rule Model
     *
     * @var \Commercers\OrderApproval\Model\DataFactory
     */
    protected $_dataOrderApprovalFactory;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $_resourceProduct;
    protected $_supervisorCollectionFactory;
    protected $messageManager;
    protected $_productRepository;
    protected $scopeConfig;
    protected $shipConfig;
    protected $_productFactory;

    protected $orderRepository;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config $shipConfig,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Commercers\OrderApproval\Model\ResourceModel\Supervisor\CollectionFactory $supervisorCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product $resourceProduct,
        \Commercers\OrderApproval\Model\DataFactory $dataOrderApprovalFactory,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Commercers\OrderApproval\Helper\DataFactory $helperDataFactory,
        \Magento\Sales\Model\Order $order
    )
    {
        $this->orderRepository = $orderRepository;
        $this->_productFactory = $productFactory;
        $this->_dataFactory = $dataFactory;
        $this->shipConfig = $shipConfig;
        $this->scopeConfig = $scopeConfig;
        $this->_productRepository = $productRepository;
        $this->_supervisorCollectionFactory = $supervisorCollectionFactory;
        $this->_resourceProduct = $resourceProduct;
        $this->_dataOrderApprovalFactory = $dataOrderApprovalFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_customerSession = $customerSession;
        $this->_helperDataFactory = $helperDataFactory;
        $this->order = $order;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderIds = $observer->getData('order_ids');
        foreach ($orderIds as $orderId){
            $order =  $this->orderRepository->get($orderId);
            //get Order All Item
            $returnItems = array();
            foreach ($order->getAllVisibleItems() as $item) {
                if ($item->getHasChildren()) {
                    foreach ($item->getChildrenItems() as $child) {
                        $returnItems[] = $child;
                    }
                } else {
                    $returnItems[] = $item;
                }
            }
            $items = $returnItems;
            $helperOrderApproval = $this->_helperDataFactory->create();
            $storeId = $order->getStoreId();
            if (!$helperOrderApproval->isEnabled($storeId)){
                return;
            }
            $createdApprovals = [];
            $isSupervisorEnable = $helperOrderApproval->isEnabledSupervisorApproval();
            if ($isSupervisorEnable && $this->_customerSession->isLoggedIn()) {
                $customer = $this->_customerSession->getCustomer();
                $customerId = $customer->getId();
                $orderApprovalRuleId = $customer->getData('order_approval_rule');
                if ($orderApprovalRuleId) {
                    foreach ($items as $item) {
                        $orderApprovalRule = $this->getOrderApprovalRuleProductById($item['product_id']);

                        $dataOrderApprovalSupervisorCollection = $this->filterOrderApprovalSupervisor($customerId,$orderApprovalRuleId);

                        $strSupervisorEmail = '';
                        $strSupervisorName = '';
                        $arrSupervisorEmail = [];
                        $arrSupervisorName = [];
                        foreach ($dataOrderApprovalSupervisorCollection as $dataOrderApprovalSuperVisor){
                            array_push($arrSupervisorEmail,$dataOrderApprovalSuperVisor["supervisor_email"]);
                            array_push($arrSupervisorName,$dataOrderApprovalSuperVisor["supervisor_name"]);
                        }
                        $strSupervisorEmail = implode(",", $arrSupervisorEmail);
                        $strSupervisorName = implode(",", $arrSupervisorName);
                        $orderApprovalRuleId = $dataOrderApprovalSupervisorCollection[0]["rule_id"];
                        $rule = $this->_ruleFactory->create()->load($orderApprovalRuleId);
                        $orderApproval = $this->_dataOrderApprovalFactory->create();
                        $dataModel = $this->_dataFactory->create();
                        $status = $dataModel::ORDER_APPROVAL_STATUS_NORMAL;
                        if($orderApprovalRule === $orderApprovalRuleId){
                            $status = $dataModel::ORDER_APPROVAL_STATUS_INIT;
                        }
                        $approvalData = array(
                            'order_id' => $order->getId(),
                            'order_item_name' => $item->getName(),
                            'order_item_id' => $item->getId(),
                            'order_approval_rule' => $orderApprovalRule,
                            'approval_code' => uniqid('approval_'),
                            'approval_email' => $strSupervisorEmail,
                            'approval_name' => $strSupervisorName,
                            'status' => $status,
                            'approval_rule_id' => $orderApprovalRuleId,
                            'created_at' => date('Y-m-d H:i:s'),
                            'last_send_reminder_at' => date('Y-m-d H:i:s'),
                            'customer_id' => $customer->getId(),
                            'approval_type' => 1,
                            'buyer_email' => $customer->getEmail(),
                            'buyer_name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
                        );
                        $orderApproval->addData($approvalData);
                        $orderApproval->save();
                        if (!isset($createdApprovals[$orderApprovalRuleId])) {
                            $createdApprovals[$orderApprovalRuleId] = array();
                        }
                        $createdApprovals[$orderApprovalRuleId][] = $approvalData;
                    }
                }
            }
            $isSkuEnabled = $helperOrderApproval->isEnabledApprovalBySku($storeId);
            if ($isSkuEnabled) {
                $customer = $this->_customerSession->getCustomer();
                $customerId = $customer->getId();
                foreach ($items as $item) {
                    try {
                        $productId = $item->getProductId();
                        $orderApprovalRuleProductId = 0;
                        if($this->getOrderApprovalRuleProductById($productId)){
                            $orderApprovalRuleProductId = $this->getOrderApprovalRuleProductById($productId);
                        }
                        $dataOrderApprovalSupervisorCollection = $this->filterOrderApprovalSupervisor($customerId,$orderApprovalRuleProductId);
                        $strSupervisorEmail = '';
                        $strSupervisorName = '';
                        $arrSupervisorEmail = [];
                        $arrSupervisorName = [];
                        foreach ($dataOrderApprovalSupervisorCollection as $dataOrderApprovalSuperVisor){
                            array_push($arrSupervisorEmail,$dataOrderApprovalSuperVisor["supervisor_email"]);
                            array_push($arrSupervisorName,$dataOrderApprovalSuperVisor["supervisor_name"]);
                        }

//                    if(count($arrSupervisorEmail) > 1){
//                        $strSupervisorEmail = implode(",", $arrSupervisorEmail);
//                        $strSupervisorName = implode(",", $arrSupervisorName);
//                    }else{
//                        $strSupervisorEmail = $arrSupervisorEmail;
//                        $strSupervisorName = $arrSupervisorName;
//                    }
                        $strSupervisorEmail = implode(",", $arrSupervisorEmail);
                        $strSupervisorName = implode(",", $arrSupervisorName);

                        if($dataOrderApprovalSupervisorCollection){
                            $orderApprovalRuleId = $dataOrderApprovalSupervisorCollection[0]["rule_id"];
                        }

                        $ruleModel = $this->_ruleFactory->create();
                        $rule = $ruleModel->load($orderApprovalRuleId);
                        $willAdd = true;
                        // Here will check if has add in $isSupervisorEnable will don't add here
                        if (!isset($createdApprovals[$orderApprovalRuleId])) {
                            $createdApprovals[$orderApprovalRuleId] = [];
                        } else {
                            foreach ($createdApprovals[$orderApprovalRuleId] as $addedItem) {
                                if ($addedItem['order_item_id'] == $item->getId() && $addedItem['approval_rule_id'] == $orderApprovalRuleId) {
                                    $willAdd = false;
                                }
                            }
                        }
                        if ($willAdd) {
                            $dataModel = $this->_dataFactory->create();
                            $orderApprovalModel = $this->_dataOrderApprovalFactory->create();
                            $status = $dataModel::ORDER_APPROVAL_STATUS_NORMAL;
                            if($orderApprovalRuleProductId == $orderApprovalRuleId && $orderApprovalRuleId != 0 && $orderApprovalRuleId != 0){
                                $status = $dataModel::ORDER_APPROVAL_STATUS_INIT;
                            }
                            $approvalData = array(
                                'order_id' => $order->getId(),
                                'order_item_name' => $item->getName(),
                                'order_item_id' => $item->getId(),
                                'order_approval_rule' => $orderApprovalRuleProductId,
                                'approval_code' => uniqid('approval_'),
                                'approval_email' => $strSupervisorEmail,
                                'approval_name' => $strSupervisorName,
                                'status' => $status,
                                'approval_rule_id' => $orderApprovalRuleId,
                                'created_at' => date('Y-m-d H:i:s'),
                                'last_send_reminder_at' => date('Y-m-d H:i:s'),
                                'customer_id' => $customer->getId(),
                                'approval_type' => 2,
                                'buyer_email' => $customer->getEmail(),
                                'buyer_name' => $customer->getFirstname() . ' ' . $customer->getLastname()
                            );
                            $orderApprovalModel->addData($approvalData);
                            $orderApprovalModel->save();
                            if (!isset($createdApprovals[$orderApprovalRuleId])) {
                                $createdApprovals[$orderApprovalRuleId] = array();
                            }
                            $createdApprovals[$orderApprovalRuleId][] = $approvalData;
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage();exit;
                    }
                }
            }
            if (count($createdApprovals)) {
                $hasProductApproval = false;
                foreach ($createdApprovals as $ruleId => $approvals) {
                    foreach ($approvals as $approval){
                        if($approval["approval_rule_id"] === $approval["order_approval_rule"]){
                            $hasProductApproval = true;
                            continue;
                        }
                    }
                }
                $orderStatus = null;
                if($hasProductApproval){
                    foreach ($createdApprovals as $ruleId => $approvals) {
                        $receiverInformation = [];
                        $arrApprovalInformation = [];
                        $arrApprovalInformation["name"] = '';
                        $arrApprovalInformation["email"] = '';
                        foreach ($approvals as $ruleIdApproval => $approval) {
                            if(!empty($approval["approval_name"])){
                                $arrApprovalInformation["name"] = $approval["approval_name"];
                            }
                            if(!empty($approval["approval_email"])){
                                $arrApprovalInformation["email"] = $approval["approval_email"];
                            }
                        }
                        $arrApprovalName = explode(",", $arrApprovalInformation["name"]);
                        $arrApprovalEmail = explode(",", $arrApprovalInformation["email"]);
                        foreach ($arrApprovalEmail as $key => $approvalEmail){
                            $receiverInformation["email"] = $approvalEmail;
                            $receiverInformation["name"] = $arrApprovalName[$key];
                            $helperOrderApproval = $this->_helperDataFactory->create();
                            // Set Data Order Necessary
                            $helperOrderApproval->sendApprovalEmail($approvals,$order,$receiverInformation);
                        }
                        if (!$orderStatus) {
                            $orderApproval = $this->_dataOrderApprovalFactory->create();
                            $orderApproval->setOrder($order);
                            $orderStatus = $orderApproval->getOrderStatusForState('pending_waiting_for_approval');
                            $order->setStatus($orderStatus)->save();
                        }
                    }
                }
            }
        }
    }
    public function getOrderApprovalRuleProductById($id)
    {
        $_product = $this->_productFactory->create()->load($id);
        // return $_product->getOrderApprovalRule();

        $storeId = 0; //Store ID
        $attributeValue = $_product->getResource()->getAttributeRawValue($_product->getId(),'order_approval_rule',$storeId); //change attribute_code
        return $attributeValue;
    }
    protected function filterOrderApprovalSupervisor($customerId,$orderApprovalRuleId)
    {
        $supervisorsCollection = $this->_supervisorCollectionFactory->create();
        $supervisorsCollection->getSelect()->join(
            array('customers' => 'commercers_order_approval_supervisor_customer'),
            'main_table.supervisor_id = customers.supervisor_id',
            array()
        );
        $supervisorsCollection->addFieldToFilter('customer_id', $customerId);
        $supervisorsCollection->getSelect()->where('main_table.rule_id = ?', $orderApprovalRuleId);
        return $supervisorsCollection->getData();
    }
}
