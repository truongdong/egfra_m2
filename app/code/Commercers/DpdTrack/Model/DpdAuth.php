<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/25/18
 * Time: 22:52
 */
namespace Commercers\DpdTrack\Model;

use Soapclient;

class DpdAuth {

	/**
	 * @var \Commercers\DpdTrack\Helper\Data
	 */
	protected $_helper;

	/**
	 * @var \Commercers\DpdTrack\Logger\Logger
	 */
	private $_logger;

	protected $_loginWsdl;
	protected $_options;


	public function __construct(
		\Commercers\DpdTrack\Helper\Data $helper,
		\Commercers\DpdTrack\Logger\Logger $logger
	) {
		$this->_helper = $helper;
		$this->_logger = $logger;

		$this->_loginWsdl = $this->_helper->getConfigValue('dpdtrack/webservice/login');
		$this->_options = array(
			'delisId' => $this->_helper->getConfigValue('dpdtrack/webservice/delisid'),
			'password' => $this->_helper->getConfigValue('dpdtrack/webservice/password'),
			'messageLanguage' => 'de_DE',
		);
	}

	public function auth(){
		$auth = null;
		try {
			$client = new Soapclient($this->_loginWsdl);
			$auth = $client->getAuth($this->_options);
			$auth->return->messageLanguage = $this->_options['messageLanguage'];
		}
		catch (\Exception $e){
			$this->_logger->err($e->getMessage());
		}

		return $auth->return;
	}

}