<?php

namespace Commercers\MultiOrderCheckout\Model;

use Magento\Directory\Model\Currency;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Sales\Model\Order\Shipment;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Store\Model\ScopeInterface;
use Commercers\MultiOrderCheckout\Helper\Config as ConfigHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ShippingMethods
{
	/**
	 * @var RateRequest
	 */
	protected $_rateRequest;

	/**
	 * @var Currency|null
	 */
	protected $_baseCurrency = null;

	/**
	 * @var CurrencyFactory
	 */
	protected $_currencyFactory;

	/**
	 * @var ConfigHelper
	 */
	protected $_configHelper;

	/**
	 * @var PriceHelper
	 */
	protected $_priceHelper;

	/**
	 * @var CheckoutSession
	 */
	protected $_checkoutSession;

	/**
	 * @var ShippingConfig
	 */
	protected $_shippingConfig;

	/**
	 *
	 * @var StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	public function __construct(
		RateRequest $rateRequest,
		CurrencyFactory $currencyFactory,
		ConfigHelper $configHelper,
		PriceHelper $priceHelper,
		CheckoutSession $checkoutSession,
		ShippingConfig $shippingConfig,
		StoreManagerInterface $storeManager,
		ScopeConfigInterface $scopeConfig
	) {
		$this->_rateRequest = $rateRequest;
		$this->_currencyFactory = $currencyFactory;
		$this->_configHelper = $configHelper;
		$this->_priceHelper = $priceHelper;
		$this->_checkoutSession = $checkoutSession;
		$this->_shippingConfig = $shippingConfig;
		$this->_storeManager = $storeManager;
		$this->scopeConfig = $scopeConfig;
	}

	public function getShippingMethods($address = null) {
		$storeId = $this->getStoreId();
		$websiteId = $this->getWebsiteId();
		$quote = $this->getQuote();

		$origCountryId = $this->_configHelper->getConfigValue(Shipment::XML_PATH_STORE_COUNTRY_ID, $storeId);
		$origState = $this->_configHelper->getConfigValue(Shipment::XML_PATH_STORE_REGION_ID, $storeId);
		$origCity = $this->_configHelper->getConfigValue(Shipment::XML_PATH_STORE_CITY, $storeId);
		$origPostcode = $this->_configHelper->getConfigValue(Shipment::XML_PATH_STORE_ZIP, $storeId);
		$this->_rateRequest
			->setWebsiteId($websiteId)
			->setStoreId($storeId)
			->setBaseCurrency($this->getBaseCurrency())
			->setAllItems($this->getQuote()->getAllItems())
			->setOrigCountryId($origCountryId)
			->setOrigRegionId($origState)
			->setOrigPostcode($origPostcode)
			->setOrigCity($origCity)
		;

		if($address) {
			$this->_rateRequest
				->setDestCountryId($address['country_id'])
				->setDestRegionId(0)
				->setDestRegionCode(null)
				->setDestPostcode($address['postcode'])
				->setDestCity($address['city'])
				->setDestStreet($address['street'])
			;
		} else {
			$this->_rateRequest
				->setDestCountryId($origCountryId)
				->setDestRegionId($origState)
				->setDestPostcode($origPostcode)
				->setDestCity($origCity)
			;
		}

		$taxInclude = $this->scopeConfig->getValue(
			'tax/calculation/price_includes_tax',
			ScopeInterface::SCOPE_STORE,
			$storeId
		);

		$baseSubtotal = 0;
		$baseSubtotalWithDiscount = 0;
		$weight = 0;
		$baseSubtotalInclTax = 0;
		$baseSubtotalWithDiscountInclTax = 0;
		$taxAmount = 0;
		foreach ($quote->getAllItems() as $item) {
			$baseSubtotal += $taxInclude ? $item->getRowTotalInclTax() : $item->getRowTotal();
			$baseSubtotalInclTax += $item->getRowTotalInclTax();
			$baseSubtotalWithDiscount += $baseSubtotal - $item->getDiscountAmount();
			$baseSubtotalWithDiscountInclTax += $baseSubtotalInclTax - $item->getDiscountAmount();
			$taxAmount += $item->getTaxAmount();
			$weight += $item->getWeight();
		}

		$this->_rateRequest->setPackageValue($baseSubtotal);
		$this->_rateRequest->setPackageValueWithDiscount($baseSubtotalWithDiscount);
		$this->_rateRequest->setPackageWeight($weight);
		$this->_rateRequest->setPackageQty($quote->getItemsQty());
		$this->_rateRequest->setBaseSubtotalInclTax($baseSubtotalInclTax);
		$this->_rateRequest->setBaseSubtotalWithDiscountInclTax($baseSubtotalWithDiscountInclTax + $taxAmount);
		$carriers = $this->_shippingConfig->getActiveCarriers();

		$shippingMethods = [];

		foreach ($carriers as $code => $carrier) {
			$rates = $carrier->collectRates($this->_rateRequest)->getAllRates();
			foreach ($rates as $rate) {
				if(isset($rate['error_message'])) {
					continue;
				}
				$rate['price_formatted'] = $this->_priceHelper->currency($rate['price'], true, false);
				$rate['cost_formatted'] = $this->_priceHelper->currency($rate['cost'], true, false);

				$shippingMethods[] = $rate->getData();
			}
		}

		return $shippingMethods;
	}

	public function getBaseCurrency()
	{
		if ($this->_baseCurrency === null) {
			$this->_baseCurrency = $this->_currencyFactory->create()->load($this->_storeManager->getStore()->getCurrentCurrency()->getCode());
		}

		return $this->_baseCurrency;
	}

	protected function getQuote() {
		return $this->_checkoutSession->getQuote();
	}

	protected function getStoreId(){
		return $this->_storeManager->getStore()->getId();
	}

	protected function getWebsiteId(){
		return $this->_storeManager->getWebsite()->getId();
	}
}
