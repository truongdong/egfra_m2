define([
    'jquery',
    'uiComponent',
    'ko',
    'underscore',
    'Commercers_MultiOrderCheckout/js/checkout/steps'
], function (
    $,
    Component,
    ko,
    _,
    steps
) {
    'use strict';

    return Component.extend({
        isVisible: ko.observable(true),
        availableOptions: ko.observableArray([]),
        selectedItem: ko.observable(),
        /**
         * Init component
         */
        initialize: function () {
            var self = this;
            var valEgfraDivision = $('select#egfra-division').val();
            if(valEgfraDivision == 0){
                self.isVisible(true);
            }else{
                $("#egfra-division .field-note").css("display","none");
                self.isVisible(false);
                steps.navigateTo('shipping');
            };
            $('select#egfra-division').change(function (e) {
                var valEgfraDivision = $(this).val();
                if(valEgfraDivision == 0){
                    $("#egfra-division .field-note").css("display","block");
                    self.isVisible(true);
                }else{
                    $("#egfra-division .field-note").css("display","none");
                    self.isVisible(false);
                    steps.navigateTo('shipping');
                };
            });
            this._super();
            steps.registerStep(
                'egfra-division',
                this.isVisible,
                _.bind(this.navigate, this),
                1
            );
        },
        navigate: function (step) {
            this.isVisible(false);
        },
    });
});