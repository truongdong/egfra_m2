<?php

namespace Commercers\MultiOrderCheckout\Model\ResourceModel;

class AddressList extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	) {
		parent::__construct($context);
	}

	protected function _construct() {
		$this->_init('commercers_multiorder_address_list', 'id');
	}
}