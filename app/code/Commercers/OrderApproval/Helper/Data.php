<?php

namespace Commercers\OrderApproval\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order\Address\Renderer;

class Data extends AbstractHelper
{
    const XML_PATH_EMAIL_TEMPLATE = 'orderapproval/approval_group/email_template';
    const XML_PATH_EMAIL_TEMPLATE_PRODUCT = 'orderapproval/approval_group/email_template_product';
    const XML_PATH_EMAIL_TEMPLATE_SUPERVISOR = 'orderapproval/approval_group/email_template_supervisor';
    const XML_PATH_EMAIL_TEMPLATE_SHIPPING = 'orderapproval/approval_group/email_template_shipping';
    const XML_PATH_IS_ENABLED = 'orderapproval/general/enabled';
    const XML_PATH_EMAIL_IDENTITY = 'sales_email/order/identity';

    const XML_PATH_ENABLE_SUPERVISOR_APPROVAL = 'orderapproval/general/supervisor_enabled';
    const XML_PATH_ENABLE_APPROVAL_BY_SKU = 'orderapproval/general/sku_enabled';


    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var UrlInterface
     */
    protected $_url;

    protected $_template;

    protected $_messageManager;

    protected $_urlInterface;

    /**
     * \Magento\Store\Model\App\Emulation
     */
    protected $_emulation;

    /**
     *  \Magento\Payment\Helper\DataFactory
     */
    protected $_helperPaymentFactory;

    protected $_ruleFactory;

    protected $_dataFactory;

    protected $_supervisorFactory;

    protected $orderRepository;

    protected $_order;

    /**
     * @var \Magento\Payment\Helper\Data
     */
    private $paymentHelper;

    /**
     * @var Renderer
     */
    protected $addressRenderer;
    protected $identityContainer;
    public function __construct(
        Renderer $addressRenderer,
        \Magento\Sales\Model\Order\Email\Container\CreditmemoIdentity $identityContainer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory,
        \Magento\Payment\Helper\DataFactory $helperPaymentFactory,
        \Magento\Store\Model\App\Emulation $emulationm,
        Context $context,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        UrlInterface $url,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        parent::__construct($context);
        $this->addressRenderer = $addressRenderer;
        $this->identityContainer = $identityContainer;
        $this->paymentHelper = $paymentHelper;
        $this->_order = $order;
        $this->orderRepository = $orderRepository;
        $this->_dataFactory = $dataFactory;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_helperPaymentFactory = $helperPaymentFactory;
        $this->_emulation = $emulationm;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->_url = $url;
        $this->_urlInterface = $urlInterface;
    }

    protected function getConfigValue($path,$storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    protected function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath,$this->_storeManager->getStore()->getId());
    }

    protected function generateTemplate($order,$orderApprovals,$senderInfo,$receiverInfo)
    {
        $storeId = $order->getStore()->getStoreId();
        $orderApprovalByRules = [];
        foreach ($orderApprovals as $orderApproval) {
            $ruleId = $orderApproval['approval_rule_id'];
            if($ruleId){
                $orderApproval['approve_link'] =  $this->_urlInterface->getUrl('orderapproval/index/approve',
                    [
                        '_store' => $storeId,
                        'approval_code' => $orderApproval['approval_code'],
                        'supervisor_name' => $receiverInfo['name']
                    ]);
                $orderApproval['disapprove_link'] =  $this->_urlInterface->getUrl('orderapproval/index/disapprove',
                    [
                        '_store' => $storeId,
                        'approval_code' => $orderApproval['approval_code'],
                        'supervisor_name' => $receiverInfo['name']
                    ]);
                if (!isset($orderApprovalByRules[$ruleId])) {
                    $orderApprovalByRules[$ruleId] = array();
                }
                $orderApprovalByRules[$ruleId][] = $orderApproval;
            }
        }
        $approvalName = '';
        $typeEmail = true;
        foreach ($orderApprovalByRules as $ruleId => $orderApproval){
            if($orderApproval[0]["approval_type"] == 2){
                $typeEmail = false;
            }
            if($orderApproval[0]["approval_name"]){
                $approvalName = $orderApproval[0]["approval_name"];
                continue;
            }
        }
        $template = $this->_transportBuilder->setTemplateIdentifier($this->_template)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->_storeManager->getStore()->getId()
                ]
            )
            ->setTemplateVars([
                'type_email' => $typeEmail,
                'order' => $order->getData(),
                'supervisor_name' => $receiverInfo['name'],
                'supervisor_email' => $receiverInfo['email'],
                'data' => $orderApproval,
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order,$storeId),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'shippingDescription' => $this->getShippingDescription($order),
                'created_at_formatted' => $order->getCreatedAtFormatted(1)
            ])
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'],$receiverInfo['name'])
            ->setReplyTo($senderInfo['email'],$senderInfo['name']);
        return $template;
    }

    public function sendEmail($template,$emailTemplateVariables,$order,$receiverInformation)
    {
        if(!empty($emailTemplateVariables[0]['approval_email'])){
            $this->_template = $this->getTemplateId($template);
            $this->_inlineTranslation->suspend();
            $senderInfo = [
                'email' => $this->getConfigValue('trans_email/ident_general/email',$this->_storeManager->getStore()->getId()),
                'name' => $this->getConfigValue('trans_email/ident_general/name',$this->_storeManager->getStore()->getId())
            ];
            $receiverInfo = [
                'email' => $receiverInformation["email"],
                'name' => $receiverInformation["name"]
            ];
            $this->generateTemplate($order,$emailTemplateVariables,$senderInfo,$receiverInfo);
            try {
                $transport = $this->_transportBuilder->getTransport();
                $transport->sendMessage();
            } catch (\Exception $e) {
                echo $e->getMessage();exit;
            }
            $this->_inlineTranslation->resume();
        }
//        var_dump($emailTemplateVariables);
//        $result = [
//            'error' => false
//        ];

    }

    public function sendApprovalEmail($emailTempVariables,$order,$receiverInformation)
    {
        $this->sendEmail(self::XML_PATH_EMAIL_TEMPLATE_SUPERVISOR,$emailTempVariables,$order,$receiverInformation);
    }

    public function isEnabledSupervisorApproval($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLE_SUPERVISOR_APPROVAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function isEnabledApprovalBySku($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLE_APPROVAL_BY_SKU,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    private function getPaymentHtml(\Magento\Sales\Api\Data\OrderInterface $order,$storeId)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $storeId
        );
    }


    private function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    private function getShippingDescription($order)
    {
        return $order->getShippingDescription();
    }


    private function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(),'html');
    }
}
