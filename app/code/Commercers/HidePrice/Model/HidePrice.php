<?php

namespace Commercers\HidePrice\Model;

use Magento\Store\Model\StoreManagerInterface;

class HidePrice extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

	const CACHE_TAG = 'commercers_hideprice';

	protected $_cacheTag = 'commercers_hideprice';

	protected $_eventPrefix = 'commercers_hideprice';

	protected function _construct() {
		$this->_init('Commercers\HidePrice\Model\ResourceModel\HidePrice');
	}

	public function getIdentities() {
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues() {
		$values = [];

		return $values;
	}
}