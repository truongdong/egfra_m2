/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    $( "select[name='custom_attributes[egfra_division]']" ).select(function() {
        alert( "Handler for .select() called." );
    });

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            shippingAddress['customAttributes']['egfra_division'] = $("[name='custom_attributes[egfra_division]'] option:selected").val();
            shippingAddress['extension_attributes']['egfra_division'] = $("[name='custom_attributes[egfra_division]'] option:selected").val();
            return originalAction();
        });
    };
});