<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/24/18
 * Time: 14:52
 */
namespace Commercers\DpdTrack\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

	/**
	 * Installs DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		$data[] = ['status' => 'DPD_ACCEPTED', 'label' => 'COMPLETE - Parcel handed to DPD'];
		$data[] = ['status' => 'DPD_AT_SENDING_DEPOT', 'label' => 'COMPLETE - Arrived at DPD'];
		$data[] = ['status' => 'DPD_ON_THE_ROAD', 'label' => 'COMPLETE - On the way to Destination'];
		$data[] = ['status' => 'DPD_AT_DELIVERY_DEPOT', 'label' => 'COMPLETE - Arrived at Delivery Depot'];
		$data[] = ['status' => 'DPD_DELIVERED', 'label' => 'COMPLETE - Delivered'];

		$setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

		$setup->getConnection()->insertArray(
			$setup->getTable('sales_order_status_state'),
			['status', 'state', 'is_default','visible_on_front'],
			[
				['DPD_ACCEPTED','complete', '0', '1'],
				['DPD_AT_SENDING_DEPOT', 'complete', '0', '1'],
				['DPD_ON_THE_ROAD', 'complete', '0', '1'],
				['DPD_AT_DELIVERY_DEPOT', 'complete', '0', '1'],
				['DPD_DELIVERED', 'complete', '0', '1']
			]
		);

		$installer->endSetup();
	}
}