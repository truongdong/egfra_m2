<?php

namespace Commercers\MultiOrderCheckout\Model\ResourceModel\AddressList;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'commercers_multiorder_checkout_adddresslist_collection';
	protected $_eventObject = 'multiorder_checkout_adddresslist_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Commercers\MultiOrderCheckout\Model\AddressList', 'Commercers\MultiOrderCheckout\Model\ResourceModel\AddressList');
	}

}