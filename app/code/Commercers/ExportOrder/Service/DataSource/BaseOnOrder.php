<?php

namespace Commercers\ExportOrder\Service\DataSource;

class BaseOnOrder extends \Commercers\ExportOrder\Service\DataSource\Order
{



    protected function exportCollectionToData($orderCollection){
       // $orderCollection->addFieldToFilter('increment_id',['eq'=>'1000000004']);
        //echo $orderCollection->getSelect();exit;
        $data['orders']['order'] = array();
        if($orderCollection->getSize()){
            
            foreach ($orderCollection as $order) {
                $orderData = $this->dataArrayConvert->toFlatArray($order, [], \Magento\Sales\Api\Data\OrderInterface::class);
                $orderData['updated_at_timestamp'] = strtotime($orderData['updated_at']);
                $orderId = $order->getId();
                foreach ($order->getAllItems() as $item) {
                    /**
                     * Do not export bundle product
                     */
                    if($item->getData('product_type') == 'bundle' OR $item->getData('product_type') == 'downloadable') continue;
                    $itemId = $item->getItemId();
                    
                    if($this->isChildProduct($itemId,$orderId)){
                        $itemData = $this->getChildItemData($itemId,$orderId);
                        unset($itemData[0]['product_options']);
                    }else{
                        $itemData = $this->dataArrayConvert->toFlatArray($item, [], \Magento\Sales\Api\Data\OrderItemInterface::class);
                    }
                    $orderData = $orderData;
                    $orderData['shipping'] = $order->getShippingAddress()->getData();
                    
                    /** 
                     * Here we allow some plug-ins working here
                     */
                    $this->extendData($itemData);
                    if(isset($itemData['qty_ordered']))
                        $itemData['qty'] = $itemData['qty_ordered'];
                    $orderData['items']['item'][]   = $itemData;
                }
                $data['orders']['order'][] = $orderData;
                $this->_proceededIds[] = $order->getId();

            }
        }
        
        return $data;
    }

    protected function extendData(&$itemData){
        if(!count($this->dataExtensions)){
            return false;
        }
        foreach($this->dataExtensions as $dataExtension){
            $extension = $this->objectManager->create($dataExtension);
            $extension->execute($itemData);
        }
    }
    protected function isChildProduct($itemId,$orderId){
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('sales_order_item');
        $sql = "SELECT `parent_item_id` FROM {$tableName} WHERE {$tableName}.order_id = {$orderId} AND {$tableName}.item_id = {$itemId}";
        $result = $connection->fetchOne($sql);
        if((int)$result){
            return true;
        }
        return false;
    }
    protected function getChildItemData($itemId,$orderId){
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('sales_order_item');
        $sql = "SELECT *  FROM {$tableName} WHERE {$tableName}.order_id = {$orderId} AND {$tableName}.item_id = {$itemId}";
        $result = $connection->fetchAll($sql);
        return $result;
    }


}
