<?php

namespace Commercers\OrderProposal\Services\Import;

class Csv {

    const XML_PATH_CSV_DELIMITER = 'section_tracking_import/group_commercers_import/enabled_commercers_import_csv_delimiter';

    protected $orderProposal;
    protected $scopeConfig;
    protected $io;

    public function __construct(
            \Commercers\OrderProposal\Services\Import\Resource\Save $orderProposal, 
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
            \Magento\Framework\Filesystem\Io\File $io
    ) {
        $this->orderProposal = $orderProposal;
        $this->scopeConfig = $scopeConfig;
        $this->io = $io;
    }

    public function execute($filePath, $fileName, $profiler, $doneFolder) {
        $csvDelimiter = $this->scopeConfig->getValue(self::XML_PATH_CSV_DELIMITER, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
        /**
         * get Data Csv
         */
        $dataCsv = $this->readCsvFile($filePath, $csvDelimiter);
        foreach ($dataCsv as $valueCsv) {
            $headerCsv = $valueCsv[0];
            $rows[] = $valueCsv[1];
        }
        /**
         * get mapping
         */
        $mapping = $this->getMappings($profiler['mapping']);
        foreach ($mapping as $key => $values) {
            $result[] = array($key, $values);
        }
        $params = [
            'header' => $headerCsv,
            'rows' => $rows,
            'mapping' => $result
        ];
        $cleanRow = null;//$this->mappingCsv->convert($params);
        $this->orderProposal->execute($cleanRow);
        /**
         * move file
         */
        $this->moveFile($doneFolder, $filePath, $fileName);
    }

    public function readCsvFile($pathFile, $csvDelimiter) {
        /**
         * Read CSV file
         */
        $dataCsv = array();
        $file = fopen($pathFile, 'r', '"');
        $header = fgetcsv($file, 3000, $csvDelimiter);
        while ($row = fgetcsv($file, 3000, $csvDelimiter)) {
            $dataCount = count($row);
            if ($dataCount < 1) {
                continue;
            }
            $dataCsv[] = array($header, $row);
        }
        return $dataCsv;
    }

    public function moveFile($doneFolder, $pathFile, $fileName) {
        $newFileName = $doneFolder . DIRECTORY_SEPARATOR . $fileName;
        $this->io->mv($pathFile, $newFileName);
    }

    public function getMappings($mapping){
        if(!is_null($mapping)){
           $mapping = preg_split('/\\r\\n|\\r|\\n/',$mapping);
          
            $value = array();
            foreach ($mapping as $_mapping){
                $attribute = explode('=',trim($_mapping));
                $value[$attribute[0]] = $attribute[1];
            }

            return $value;
       }
       return false;
    }

}
