<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer;

use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * Class AssociatedCustomers
 * @package Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer
 */
class AssociatedCustomers extends \Magento\Backend\Block\Template implements \Magento\Ui\Component\Layout\Tabs\TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'tabs/customer/associated_customer.phtml';
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * AssociatedCustomers constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     * @param JsonHelper|null $jsonHelper
     * @param DirectoryHelper|null $directoryHelper
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = [], ?JsonHelper $jsonHelper = null, ?DirectoryHelper $directoryHelper = null, \Magento\Framework\Registry $registry)
    {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
        $this->_coreRegistry = $registry;
    }

    /**
     * @return mixed|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(\Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabLabel()
    {
        return __('Associated Accounts');
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabTitle()
    {
        return __('Associated Accounts');
    }

    /**
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }
    /**
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * @return false
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }
}