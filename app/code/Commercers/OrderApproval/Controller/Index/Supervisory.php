<?php

namespace Commercers\OrderApproval\Controller\Index;

use Magento\Framework\App\Action\Context;

class Supervisory extends \Magento\Framework\App\Action\Action
{

    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}