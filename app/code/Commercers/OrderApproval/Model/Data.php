<?php

namespace Commercers\OrderApproval\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\ProductFactory as ProductResourceFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Data extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{

    const ORDER_APPROVAL_STATUS_INIT = 0;
    const ORDER_APPROVAL_STATUS_APPROVED = 1;
    const ORDER_APPROVAL_STATUS_DISAPPROVED = 2;
    const ORDER_APPROVAL_STATUS_NORMAL = 3;

    const XML_STATUS_ORDER_WAITING_FOR_APPROVAL = 'orderapproval/approval_group/orderstatus_waitingforapprove';
    const XML_STATUS_ORDER_HAS_ITEM_APPROVAL = 'orderapproval/approval_group/orderstatus_hasitemapproval';

    const XML_STATUS_ORDER_APPROVED = 'orderapproval/approval_group/orderstatus_approved';
    const XML_STATUS_ORDER_DISAPPROVED = 'orderapproval/approval_group/orderstatus_disapproved';

    /**#@+
     * Rule's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    const CACHE_TAG = 'commercers_orderapproval_data';

    protected $_cacheTag = 'commercers_orderapproval_data';

    protected $_eventPrefix = 'commercers_orderapproval_daa';

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProductResourceFactory
     */
    protected $_productResourceFactory;
    protected $_date;

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        ProductFactory $productFactory,
        ProductResourceFactory $productResourceFactory,
        DateTime $date,

        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_productFactory = $productFactory;
        $this->_productResourceFactory = $productResourceFactory;
        $this->_date = $date;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    protected function _construct()
    {
        $this->_init('Commercers\OrderApproval\Model\ResourceModel\Data');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    /**
     * Prepare rule's statuses, available event cms_rule_get_available_statuses to order_approval statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function isXmlStatusOrderDisapproved($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_STATUS_ORDER_DISAPPROVED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getOrderStatusForState($state = null)
    {
        if ($state) {
            switch ($state) {

                case 'processing_waiting_for_approval':
                    return $this->scopeConfig(self::XML_STATUS_ORDER_WAITING_FOR_APPROVAL);
                    break;
                case 'pending_waiting_for_approval':
                    return $this->scopeConfig(self::XML_STATUS_ORDER_HAS_ITEM_APPROVAL);
                    break;
                case 'approved':
                    return $this->scopeConfig(self::XML_STATUS_ORDER_APPROVED);
                    break;
                case 'disapproved':
                    return $this->scopeConfig(self::XML_STATUS_ORDER_DISAPPROVED);
                    break;
            }
        }
        return false;
    }

    protected function scopeConfig($template)
    {
        return $this->scopeConfig->getValue(
            $template,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
