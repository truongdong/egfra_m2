<?php
namespace Commercers\OrderApproval\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Data extends AbstractDb {

    public function __construct(
        Context	$context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('commercers_order_approval', 'order_approval_id');
    }
}
