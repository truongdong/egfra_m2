<?php

namespace Commercers\SalesForceIntegrationSuite\Controller\Adminhtml\Contact;

use Magento\Framework\App\Filesystem\DirectoryList;
use Commercers\OnepageCheckout\Logger\Logger;

class Save extends \Magento\Backend\App\Action
{
    protected $_fileUploaderFactory;
    protected $_filesystem;
    protected $_fileCsv;
    protected $_storeManager;
    protected $registry;
    protected $_logger;
    protected $_fileio;

    protected $eavInstall;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\File\Csv $fileCsv,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        Logger $logger,
        \Magento\Framework\Filesystem\Io\File $fileio,
        \Magento\Backend\App\Action\Context $context,
        \Commercers\SalesForceIntegrationSuite\Model\EavInstall $eavInstall
    ) {
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $fileSystem;
        $this->_fileCsv = $fileCsv;
        $this->_storeManager = $storeManagerInterface;
        $this->registry = $registry;
        $this->_logger = $logger;
        $this->_fileio = $fileio;
        $this->eavInstall = $eavInstall;

        parent::__construct($context);
    }

    public function execute()
    {
        $paramOptionImport = $this->_request->getParam('option_import');
        $this->registry->register('isSecureArea', true);
        try {
            $filepath = $this->_uploadFileAndGetName();

            if ($filepath !='' && file_exists($filepath)) {
                chmod($filepath, 0777);
                $data = $this->_fileCsv->getData($filepath);
                if (isset($data[0]) && !empty($data[0])) {
                    try {
                        if($paramOptionImport){
                            $this->eavInstall->replaceSalesForceContact($filepath);
                        }else{
                            $this->eavInstall->addSalesForceContact($filepath);

                        }
                        unlink($filepath);
                        $this->messageManager->addSuccess(__('SalesForceIntegrationSuite Contact has been imported Successfully'));
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                        $this->_logger->error($e->getMessage());
                    }
                    return $this->redirect();
                } else {
                    $this->messageManager->addError('Data Not Found.');
                    return $this->redirect();
                }
            } else {
                $this->messageManager->addError('File not Found.');
                return $this->redirect();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_logger->error($e->getMessage());
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->_logger->error($e->getMessage());
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            $this->messageManager->addError($e->getMessage());
        }

        return $this->redirect();
    }

    protected function _uploadFileAndGetName()
    {
        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'file']);
        $uploader->setAllowedExtensions(['CSV', 'csv']);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $path = $this->_filesystem->getDirectoryRead(DirectoryList::VAR_DIR)
            ->getAbsolutePath('salesforce_contact_import');

        if (!is_dir($path)) {
            $this->_fileio->mkdir($path, '0777', true);
            $this->_fileio->chmod($path, '0777', true);
        }
        $result = $uploader->save($path.'/');
        if (isset($result['file']) && !empty($result['file'])) {
            return $result['path'].$result['file'];
        }
        return false;
    }

    protected function _getKeyValue($row, $headerArray)
    {
        $temp = [];
        foreach ($headerArray as $key => $value) {
            $temp[$value] = $row[$key];
        }
        return $temp;
    }

    protected function redirect(){
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('salesforceintegrationsuite/contact/import');

        return $resultRedirect;
    }
}