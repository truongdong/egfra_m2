<?php

namespace Commercers\Profilers\Service\Profiler\Process\Helper;

use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Json
{


    public function __construct(
            \Commercers\Profilers\Service\Data\Parser\Xml $xmlService,
            \Magento\Framework\DomDocument\DomDocumentFactory $domFactory
            ) {

        $this->xmlService = $xmlService;
        $this->domFactory = $domFactory;
    }


    public function getOutput($profiler, $data)
    {


        $date = new \DateTime();

        //$fileName = "export_" . $profiler->getData('data_source') . "_" . $date->format('dmYHis') . "." . $profiler->getData('format');

        $xslDoc = $profiler->getData('export_output_template');

        $xml = new \SimpleXMLElement($xslDoc);

        $files = $xml->xpath("/files/file");
        $fileName = $this->_getFileName($files,$data);
        if($files && count($files)){
            $xsltTemplate = current($files[0]->xpath('*'))->asXML();

        }else{
            $xsltTemplate = $xslDoc;
        }

        $dom = new \DOMDocument();
        $xsl = new \XSLTProcessor();
        $dom->loadXML($xsltTemplate);
        $xsl->importStyleSheet($dom);
        $xsl->registerPHPFunctions();

        $xmlSource = $this->domFactory->create();

        $xmlSource->loadXML($this->xmlService->arrayToXml($data), LIBXML_PARSEHUGE|LIBXML_NSCLEAN);

        $out = $xsl->transformToXML($xmlSource);
        if($profiler->getDataSource() == 'order_proposal_export'){ 
             $out = $this->_getOutArray($out);
        }else{
            $out = json_encode(simplexml_load_string($out));
        }
        
        $result = array(
            'type' => 'content',
            'files' => array(
                $fileName => $out 
            )
        );
        //echo 1; exit;
        return $result;


    } 
    protected function _getFileName($files,$data){

        foreach ( (array) $files[0] as $index => $node )
          $out[] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
        $fileName = $out[0]['filename'];


        return $fileName;
    }
    /**
     * This function only use for order_proposal_export json
     */
    private function _getOutArray($out){
        $simpleXmlLoadString = json_decode(json_encode(simplexml_load_string($out)),true);
        $articlesValueIsArray = true;
        foreach ($simpleXmlLoadString as $key => $element) {
            if($key == 'articles'){
                foreach ($element as $keyElement => $valueElement){
                    if(!is_numeric($keyElement)) {
                        $articlesValueIsArray = false;
                    }
                }
            }
            
          }
        if(!$articlesValueIsArray){
            
            $simpleXmlLoadString['articles'] = [$simpleXmlLoadString['articles']];
        }
        return str_replace('"null"','null',json_encode($simpleXmlLoadString));
    }
}
