<?php

namespace Commercers\OnepageCheckout\Block\Adminhtml\ShippingAddress\Edit\Tab;

class Import extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Import Shipping Address'),
                'class'  => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'file',
            'file',
            [
                'name'  => 'file',
                'label' => __('Upload File'),
                'title' => __('Upload File'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'option_import',
            'select',
            [
                'values' => ['0' => __('Add/Update'), '1' => __('Replace')],
                'name' => 'option_import',
                'label' => __('Option Import'),
                'title' => __('Option Import'),
                'class' => 'option_import'
            ]
        );
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Import Shipping Address');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
