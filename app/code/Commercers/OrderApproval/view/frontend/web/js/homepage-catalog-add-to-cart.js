require([
    "jquery",
    'mage/translate'
], function (
    $,
    $t
) {

    $('form[data-role="tocart-form"]').submit(function(eve) {
        var self = this;
        eve.preventDefault();
        var valSerialize = $(this).serializeArray();

        // var urlGetAttribute = $(this).context.attributes['data-role'].baseURI + 'orderapproval/product/getattribute';
        var urlGetAttribute = valSerialize[0].value;
        var skuProduct = valSerialize[1].value;
        $.ajax({
            type: "POST",
            url: urlGetAttribute,
            data: {
                'skuProduct':  skuProduct
            },
            dataType: 'json',
            success: function (response) {
                if(response.message){
                    var popup = $('<div class="add-to-cart-dialog"/>').html($t('<span>This product is a release item. The delivery takes place only after approval by the supervisor.</span>')).modal({
                        modalClass: 'add-to-cart-popup',
                        //title: $.mage.__("No Title"),
                        buttons: [
                            {
                                text: $t('Continue Shopping'),
                                click: function () {
                                    this.closeModal();
                                    self.submit();
                                }
                            },
                            {
                                text: $t('Cancel'),
                                click: function () {
                                    this.closeModal();
                                }
                            }
                        ]
                    });
                    popup.modal('openModal');
                }else{
                    self.submit();
                }
            }
        });
        // self.submit();
    });
});


