<?php

namespace Commercers\ExportOrder\Cron;

class ExportOrder
{
	protected $_file;
	protected $_domFactory;
	protected $_directoryList;
	protected $_profilerFactory;
	protected $_order;
	protected $_processLog;

	public function __Construct(
		\Magento\Framework\Filesystem\Io\File $file,
		\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
		\Magento\Framework\DomDocument\DomDocumentFactory $domFactory,
		\Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
		\Magento\Sales\Model\Order $order,
		\Commercers\Profilers\Service\Log $processLog
	){
          $this->_profilerFactory = $profilerFactory;
		$this->_file = $file;
		$this->_directoryList = $directoryList;
		$this->_domFactory = $domFactory;
		$this->_order = $order;
		$this->_processLog = $processLog;
	}

	public function execute($schedule){
          $jobCode = $schedule->getJobCode();
		$profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
		$orderCollection = $this->_order->getCollection();
		foreach($orderCollection as $order){
			foreach ($order->getAllItems() as $item) {
				$items[] = $item->getData();
			}
		}
		// print_r($items);exit;
          foreach($profiler->getData() as $value){
               $xslDoc 			= $value['outputformat'];
			$idProfiler 		= $value['id'];
			$enableLocal 		= $value['enablelocal'];
			$enableFtp 		= $value['enableftp'];
			$localFolder 		= $value['localfolder'];
			$localFolderFtp 	= $value['localfolderftp'];
			$format 			= $value['format'];
		}

		//write log to start export
		$dataProcessLog = $this->_processLog->execute($idProfiler);

		try {
			$dom = new \DOMDocument();
			$xsl = new \XSLTProcessor;
			$dom->loadXML($xslDoc);
			$xsl->importStyleSheet($dom);
			$xmlSource = $this->_domFactory->create();
			// $xsl->loadXML($xslDoc);
			$xmlSource->loadXML($this->arrayToXml($items));

			$out = $xsl->transformToXML( $xmlSource );
			// echo "<pre>";echo $out;exit;
			$fileName = "export_order_" . $idProfiler .".". $format;
			if($enableLocal == 1){
				$filePathLocal = DIRECTORY_SEPARATOR . $localFolder;
				$this->writeFileFromOutputFromat($filePathLocal,$out,$fileName);
			}
			if($enableFtp == 1){
				$filePathFtp = DIRECTORY_SEPARATOR . $localFolderFtp;
				$this->writeFileFromOutputFromat($filePathFtp,$out,$fileName);
			}
			$status = 1;
			$message = "EXPORT ORDER TO FILE SUCCESS!";
		} catch (Exception $e) {
			$status = 0;
			$message = "EXPORT ORDER TO FILE FAILED!";
		}
		//write log to end export
		$this->_processLog->executeAfter($dataProcessLog['process_id'],$status,$message);

	}

	public function writeFileFromOutputFromat($filePath,$out,$fileName){
		$path = $this->_directoryList->getPath('var').$filePath;
		if (!is_dir($path)) {
			$this->_file->mkdir($path, 0775);
		}
		$this->_file->open(array('path'=>$path));
		$this->_file->write($fileName, $out, 0666);
	}

	public function arrayToXml($array, $rootElement = null, $xml = null) {
		$_xml = $xml;

		    // If there is no Root Element then insert root
		if ($_xml === null) {
			$_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<items/>');
		}

		    // Visit all key value pair
		foreach ($array as $k => $v) {

		        // If there is nested array then
			if (is_array($v)) {

		            // Call function for nested array
				$this->arrayToXml($v, '<item/>', $_xml->addChild('item'));
			}

			else {

		            // Simply add child element.
				//echo $k;
				$_xml->addChild($k, $v);
			}
		}

		return $_xml->asXML();
	}
}
