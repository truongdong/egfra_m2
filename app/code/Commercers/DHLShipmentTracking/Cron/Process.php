<?php
/*
Commercers VN
HieuND
*/
namespace Commercers\DHLShipmentTracking\Cron;

class Process
{
     /**
	 * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
	 */
	protected $_orderFactory;

     protected $_log;

     protected $_helper;

     protected $_resource;

     protected $_orderCollectionFactory;

     public function __construct(
          \Commercers\DHLShipmentTracking\Helper\Data $helper,
          \Magento\Framework\App\ResourceConnection $resource,
          \Magento\Sales\Model\OrderFactory $orderFactory,
          \Magento\Sales\Model\ResourceModel\Order\CollectionFactory  $orderCollectionFactory,
          \Commercers\DHLShipmentTracking\Model\DHLShipmentTrackingLogFactory $log
     ) {
          $this->_helper = $helper;
          $this->_resource = $resource;
          $this->_orderFactory = $orderFactory;
          $this->_log = $log;
          $this->_orderCollectionFactory = $orderCollectionFactory;
     }

     public function execute(){
          if(!$this->_helper->isEnabled()){
			return;
		}
          // sandbox: username (entwickler.dhl.de)
          // production: app name (entwickler.dhl.de)
          $cig_user = $this->_helper->getCigUser();

          // sandbox: password (entwickler.dhl.de)
          // production: app token (entwickler.dhl.de)
          $cig_password = $this->_helper->getCigPassword();

          // sandbox: zt12345
          // production: user zt...
          $tnt_user = $this->_helper->getTntUser();

          // sandbox: geheim
          // production: user password
          $tnt_password = $this->_helper->getTntPassword();
          // creates a new Credentials object
          // endpoint: ENDPOINT_SANDBOX / ENDPOINT_PRODUCTION   
          // api: https://entwickler.dhl.de/group/ep/wsapis/sendungsverfolgung
          if($this->_helper->getCredentials() === 'ENDPOINT_SANDBOX'){
               $credentials = new \Commercers\DHLShipmentTracking\Api\Credentials($cig_user, $cig_password, \Commercers\DHLShipmentTracking\Api\Credentials::ENDPOINT_SANDBOX, $tnt_user, $tnt_password);
          }
          if($this->_helper->getCredentials() === 'ENDPOINT_PRODUCTION'){
               $credentials = new \Commercers\DHLShipmentTracking\Api\Credentials($cig_user, $cig_password, \Commercers\DHLShipmentTracking\Api\Credentials::ENDPOINT_PRODUCTION, $tnt_user, $tnt_password);
          }
          $api = new \Commercers\DHLShipmentTracking\Api\ShipmentTracking($credentials);
          
          //  $trackingData = $api->getDetailsAndEvents("00340434161094022115", \Commercers\DHLShipmentTracking\Api\RequestBuilder::LANG_DE);
          
          //  echo '<pre>'; print_r($trackingData['events']);exit;
          
          $log = $this->_log->create()->getCollection();
		$log->getSelect()->order(array('updated_at DESC', 'order_id DESC'));

          $lastOrderRun = $log->getFirstItem()->getOrderId();
          $daysBack = $this->_helper->getConfigValue('dhl_shipment_tracking/general/days_back');
          $orderPerRun = $this->_helper->getConfigValue('dhl_shipment_tracking/general/limit');

          $filterStatuses = explode(',',$this->_helper->getConfigValue('dhl_shipment_tracking/general/order_statuses'));
          
          if($daysBack <= 1){
			$daysBackTime = strtotime('-'.$daysBack." day");
		} else {
			$daysBackTime = strtotime('-'.$daysBack." days");
		}
          $date = date('Y-m-d h:i:s', $daysBackTime);

          $collection = $this->_orderCollectionFactory->create()->addFieldToSelect('*');
		$collection
			// ->addFieldToFilter('entity_id',['gt'=>$lastOrderRun])
			// ->addFieldToFilter('entity_id',['gte'=>$lastOrderRun])
			->addFieldToFilter('status',['in'=>$filterStatuses])
		;

		$collection
			->getSelect()
			->joinLeft(
				['sales_shipment_track'=>$this->_resource->getTableName('sales_shipment_track')],
				'main_table.entity_id = sales_shipment_track.order_id',
				[
					'order_id'=>'sales_shipment_track.order_id',
					'track_number' => 'sales_shipment_track.track_number',
					'created_at' => 'sales_shipment_track.created_at'
				]
			)
			->where('sales_shipment_track.created_at >= ?',$date)
			->where('order_id IS NOT NULL')
			->where('track_number IS NOT NULL')
			->order('entity_id ASC')
			->group('entity_id')
			->limit($orderPerRun)
          ;
          
		if(!$collection->count()){
			$collection->getSelect()->reset(\Zend_Db_Select::WHERE);
			$collection
				->addFieldToFilter('status',['in'=>$filterStatuses])
			;
			$collection
				->getSelect()
				->where('sales_shipment_track.created_at >= ?',$date)
				->where('order_id IS NOT NULL')
				->where('track_number IS NOT NULL')
				->limit($orderPerRun)
			;
          }
          // echo "<pre>";print_r($collection->getData());exit;
          foreach($collection->getData() as $item){
               $this->changeOrderStatus($item,$api);
          }
          // foreach($collection as $item){
          //      $this->changeOrderStatus($item,$api);
          // }
		return $this;
     }

     public function changeOrderStatus($args,$api){
          $trackingNumber = preg_replace('/\s+/', '', $args['track_number']);
          $trackingData = $api->getDetailsAndEvents($trackingNumber, \Commercers\DHLShipmentTracking\Api\RequestBuilder::LANG_DE);
          // print_r(Commercers\DHLShipmentTracking\Model\Config\Source\Status::toOptionArray());exit;
          $status[] = [
               'ZU' => 'Zustellung erfolgreich',
               'PO' => 'In Zustellung',
               'EE' => 'Ziel-Paketzentrum',
               'AE' => 'Abholung erfolgreich',
               'AA' => 'Transport zum Start-Paketzentrum',
               'ES' => 'Einlieferung in PACKSTATION'
          ];
          // print_r($trackingData['ev']);exit;
          $orderStatus = '';
          if(!empty($trackingData['events'])){
               // Process check status
               foreach($status as $value){
                    if(isset($trackingData['events']['0'])){
                         $orderStatus = $trackingData['events']['0']['standard-event-code'];
                         $statusDescription = $value[$trackingData['events']['0']['standard-event-code']];
                    }
               }
               // print_r($args);exit;
			if($orderStatus){
				$order = $this->_orderFactory->create()->load($args['entity_id']);

				$currentStatus = $order['status'];
                    
				if($currentStatus != $orderStatus){
					$order->setStatus($orderStatus);
					$order->addStatusToHistory(false,$statusDescription);
					$order->save();
				}
			}
		}

          $log = $this->_log->create()->load($args['entity_id'],'order_id');
          if(!empty($log->getData())){
               if($orderStatus == 'ZU'){
				$log->delete();
				return $this;
			} else {
				$logData = [
					'updated_at' => date('Y-m-d h:i:s')
				];
				$log->addData($logData);
			}
          }{
               if($orderStatus != 'ZU'){
                    $log->addData([
                         'order_id' => $args['entity_id'],
                         'tracking_number' => $trackingNumber,
                         'status' => $orderStatus,
                         'increment_id' => $args['increment_id'],
                         'updated_at' => date('Y-m-d h:i:s')
                    ]);
               };
          }
          try {
			$log->save();
		} catch (\Exception $e){
			echo $e->getMessage();
		}
     }

     // public function changeOrderStatus($args,$api){
     //      $trackingNumber = preg_replace('/\s+/', '', $args->getTrackNumber()); print_r($trackingNumber);exit;
     //      $trackingData = $api->getDetailsAndEvents($trackingNumber, \Commercers\DHLShipmentTracking\Api\RequestBuilder::LANG_DE);

     //      $status = [
     //           'ZU' => 'Zustellung erfolgreich',
     //           'PO' => 'In Zustellung',
     //           'EE' => 'Ziel-Paketzentrum',
     //           'AA' => 'Start-Paketzentrum',
     //           'AE' => 'Abholung erfolgreich',
     //           'AA' => 'Transport zum Start-Paketzentrum',
     //           'ES' => 'Einlieferung in PACKSTATION'
     //      ];
     //      print_r($trackingData);exit;
     //      $orderStatus = '';
     //      if(!empty($trackingData['events'])){
     //           // Process check status
     //           foreach($status as $value){
     //                echo $value['ZU'];
     //           }exit;
     //           if($trackingData['short-status'] == self::DHL_DELIVERED_STATUS){
     //                $orderStatus = self::DHL_DELIVERED;
     //           };

	// 		if($orderStatus){
	// 			$order = $this->_orderFactory->create()->load($args->getEntityId());

	// 			$currentStatus = $order->getStatus();

	// 			if($currentStatus != $orderStatus){
	// 				$order->setStatus($orderStatus);
	// 				$order->addStatusToHistory(false,$trackingData['status']);
	// 				$order->save();
	// 			}
	// 		}
	// 	}

     //      $log = $this->_log->create()->load($args->getEntityId(),'order_id');
     //      if(!empty($log->getData())){
     //           if($orderStatus == self::DHL_DELIVERED){
	// 			$log->delete();
	// 			return $this;
	// 		} else {
	// 			$logData = [
	// 				'updated_at' => date('Y-m-d h:i:s')
	// 			];
	// 			$log->addData($logData);
	// 		}
     //      }{
     //           $log->addData([
     //                'order_id' => $args->getEntityId(),
     //                'tracking_number' => $trackingNumber,
     //                'status' => $orderStatus,
     //                'increment_id' => $args->getIncrementId(),
     //                'updated_at' => date('Y-m-d h:i:s')
     //           ]);
     //      }
     //      try {
	// 		$log->save();
	// 	} catch (\Exception $e){
	// 		echo $e->getMessage();
	// 	}
     // }
}