define([
    'uiComponent',
    'jquery',
    'ko',
    'underscore',
    'mage/translate',
    'Commercers_MultiOrderCheckout/js/checkout/steps',
    'mage/url',

    'mage/backend/notification'
], function(Component, $, ko, _, $t, steps, urlBuilder) {
    'use strict';

    return Component.extend({
        isVisible: ko.observable(false),
        shippingAddresses: ko.observableArray(),
        chosenAddresses: ko.observableArray(),
        savedAddresses: ko.observableArray(),
        isFormVisible: ko.observable(false),
        addressIdFormEditing: ko.observable(),
        shippingMethods: ko.observableArray(),
        isCheckoutAvailable: ko.observable(false),
        isSaveAddressAvailable: ko.observable(true),
        methods: ko.observableArray(),
        addressShippingMethods: ko.observableArray(),

        initialize: function () {
            this._super();
            steps.registerStep(
                'shipping-addresses',
                this.isVisible,
                _.bind(this.navigate, this),
                1
            );
        },
        navigate: function (step) {
            this.isVisible(true);
        },
        next: function () {
        },
        back: function () {
        },
        submitFilterAddress: function() {
            var check = 0;
            $('.shippingaddresses-filter').each(function(i, e) {
                if($(e).val() != ''){
                    check = 1;
                    return false;
                }else{
                    check = 0;
                }
            });
            if(check == 1){
                $('.submit-filter-address').removeAttr('disabled');
            }else{
                $('.submit-filter-address').attr('disabled','disabled');
            }
        },
        searchFilterAddresses: function() {
            var self = this;
            var data = [];
            data['searchCustomerNumber'] = $('#customer-number').val();
            data['searchCustomerName'] = $('#customer-name').val();
            data['searchPostalcode'] = $('#customer-postal-code').val();
            data['searchCity'] = $('#customer-city').val();
            self.ajaxSearchResult(data);
        },
        ajaxSearchResult: function(data) {
            var self = this;
            var url = urlBuilder.build("onepagecheckout/shippingaddress/searchresult");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'searchCustomerNumber':  data['searchCustomerNumber'],
                    'searchCustomerName':  data['searchCustomerName'],
                    'searchPostalcode':  data['searchPostalcode'],
                    'searchCity':  data['searchCity'],
                },
                showLoader: true,
                dataType: 'json',
                success: function (response) {
                    var html = '';
                    $('#shipping-address-select').html("");
                    if(response.message){
                        $('#sa-notfound').hide();
                        var options = response.options;
                        $.each(options, function(index) {
                            self.shippingAddresses()[options[index].value] = ko.observable(options[index].label);
                            html += '<option value="'+ options[index].value +'">'+options[index].label+'</option>' ;
                        });
                    }else{
                        $('#sa-notfound').show();
                    }
                    $('#shipping-address-select').append(html);
                }
            });
        },
        searchInputEnter : function (){
            var self = this;
            $(document).keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                var fieldEmpty = true;
                if($('#customer-number').val() != '' || $('#customer-name').val() != '' || $('#customer-city').val() != '' || $('#customer-postal-code').val() != ''){
                    fieldEmpty = false;
                }
                if(keycode == '13' && !fieldEmpty){
                    self.searchFilterAddresses();
                }
            });

        },
        fillAddressForm: function(addressId)
        {
            var self = this;
            $("#btn-save-contact").removeAttr("disabled");
            this.isFormVisible(true);
            this.addressIdFormEditing(addressId);

            if(addressId){
                var emailAddress = '';
                var emailContact = '';

                var dataShipping = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; });
                var dataContactAccount = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; }).dataContactAccount();

                emailAddress = dataShipping.email();
                emailContact = dataShipping.email();

                $('select[name="shipping_address_select_alt2"]').find('option').remove();

                if(dataContactAccount != "" && dataContactAccount.length > 1){
                    $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                        value: 0,
                        text : "Bitte wählen"
                    }));
                    $.each(dataContactAccount, function (i, item) {
                        if(item['is_primary'] == 1){
                            $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                value: item['salesforceintegrationsuite_contact_id'],
                                text : item['firstname'] + " " + item['lastname'],
                                selected:true
                            }));
                            if(item['email'] != ''){
                                $('input[name="shipping_email"]').val(item['email']);
                                $('input[name="firstname"]').val(item['firstname']);
                                $('input[name="lastname"]').val(item['lastname']);
                            }
                            if(item['phone'] != null){
                                $('input[name="telephone"]').val(item['phone']);
                            }

                            if(item['shipping_company_name'] != null){
                                $('input[name="company"]').val(item['shipping_company_name']);
                            }

                        }else{
                            $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                                value: item['salesforceintegrationsuite_contact_id'],
                                text : item['firstname'] + " " + item['lastname']
                            }));
                        }
                        $('select[name="shipping_address_select_alt2"]').change(function (e) {
                            $("#btn-save-contact").removeAttr("disabled");
                            var salesforceintegrationsuiteContactId = $(this).val();
                            if(salesforceintegrationsuiteContactId == 0){
                                $('input[name="shipping_email"]').val(emailAddress);
                            }
                            if(salesforceintegrationsuiteContactId == item['salesforceintegrationsuite_contact_id']){
                                $('input[name="firstname"]').val(item['firstname']);
                                $('input[name="lastname"]').val(item['lastname']);
                                $('input[name="shipping_email"]').val(item['email']);
                                if(item['phone'] != null)
                                    $('input[name="telephone"]').val(item['phone']);
                                if(item['shipping_company_name'] != null)
                                    $('input[name="company"]').val(item['shipping_company_name']);
                            }
                        });
                    });
                }else if(dataContactAccount != "" && dataContactAccount.length == 1){
                    $.each(dataContactAccount, function (i, item) {
                        if(item['email'] != '')
                            emailContact = item['email'];
                        $('select[name="shipping_address_select_alt2"]').append($('<option>', {
                            value: item['salesforceintegrationsuite_contact_id'],
                            text : item['firstname'] + " " + item['lastname']
                        }));
                        $('input[name="firstname"]').val(item['firstname']);
                        $('input[name="lastname"]').val(item['lastname']);
                        if(item['phone'] != null)
                            $('input[name="telephone"]').val(item['phone']);
                        if(item['shipping_company_name'] != null)
                            $('input[name="company"]').val(item['shipping_company_name']);
                    });
                } else{
                    $('select[name="shipping_address_select_alt2"]').find('option').remove();
                }
                if(dataContactAccount != ''){
                    if(emailContact != ''){
                        $('input[name="shipping_email"]').val(emailContact);
                    }
                }
                if(dataShipping){
                    $('input[name="commercers_account_number"]').val(dataShipping.account());
                    $('input[name="company"]').val(dataShipping.company());
                    $('input[name="city"]').val(dataShipping.city());
                    $('input[name="street"]').val(dataShipping.street());
                    $('input[name="postcode"]').val(dataShipping.postcode());
                    $('input[name="fax"]').val(dataShipping.fax());
                    $('input[name="telephone"]').val(dataShipping.telephone());
                    $('input[name="firstname"]').val(dataShipping.firstname());
                    $('input[name="lastname"]').val(dataShipping.lastname());
                    $('input[name="shipping_email"]').val(emailAddress);
                }
            }
        },
        addAddressToList: function () {
            var self = this;
            this.isFormVisible(false);
            this.addressIdFormEditing('');

            var isItemAvailable = self.checkItems();
            if(isItemAvailable) {
                var chosenAddressIds = $('#shipping-address-select').val();
                $.each(chosenAddressIds, function(index, addressId){
                    var chosenAddress = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; });
                    if(!chosenAddress) {
                        var url = urlBuilder.build("onepagecheckout/shippingaddress/getAddress");
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                'addressId':  addressId
                            },
                            showLoader: true,
                            dataType: 'json',
                            success: function (response) {
                                if(response.message){
                                    var address = {};
                                    address['addressId'] = addressId;
                                    address['dataContactAccount'] = response.dataContactAccount;
                                    address['dataShipping'] = response.dataShipping;
                                    var addressFormatted = self.addressFormat(address);
                                    self.chosenAddresses.push(addressFormatted);
                                    self.setDefaultShippingMethod(addressId);
                                }
                            }
                        });
                    }
                });
            }
            this.isCheckoutAvailable(true);
            this.isSaveAddressAvailable(true);
        },
        checkItems: function () {
            var self = this;
            var isAvailable = true;
            var chosenAddressIds = $('#shipping-address-select').val();
            $.each(self.chosenAddresses(), function (index, address) {
                if(!_.contains(chosenAddressIds, address.address_id())) {
                    chosenAddressIds.push(address.address_id());
                }
            });
            var url = urlBuilder.build("multiorder/checkout/checkitems");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'address_ids': chosenAddressIds
                },
                dataType: 'json',
                showLoader: true,
                async: false,
                success: function (response) {
                    if(response.is_error){
                        isAvailable = false;
                        self.notifyMessage(response.message, true);
                        setTimeout(function () {
                            self.clearMessage();
                        },3000);
                    }
                }
            });
            return isAvailable;
        },
        editChosenAddress: function (address) {
            this.isCheckoutAvailable(false);
            this.isSaveAddressAvailable(false);
            this.fillAddressForm(address.address_id());
            this.getShippingMethods(address.address_id());
        },
        removeChosenAddress: function (address) {
            if(this.addressIdFormEditing() == address.addressId) {
                this.isFormVisible(false);
            }
            this.chosenAddresses.remove(address);
            this.checkItems();
        },
        updateChosenAddress: function () {
            var self = this;
            var addressId = this.addressIdFormEditing();

            var addressModel = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; });
            addressModel.email($('input[name="shipping_email"]').val());
            addressModel.firstname($('input[name="firstname"]').val());
            addressModel.lastname($('input[name="lastname"]').val());
            addressModel.company($('input[name="company"]').val());
            addressModel.city($('input[name="city"]').val());
            addressModel.postcode($('input[name="postcode"]').val());
            addressModel.fax($('input[name="fax"]').val());
            addressModel.telephone($('input[name="telephone"]').val());
            addressModel.street($('input[name="street"]').val());
            addressModel.country_id($('select[name="country_id"]').val());

            var form = $('#shipping-address-form');

            form.validation();

            var formValid = form.validation('isValid');

            if(formValid) {
                this.isFormVisible(false);
                this.isCheckoutAvailable(true);
                this.isSaveAddressAvailable(true);
                addressModel.isValid(true);
            }

            var shippingMethod = _.find(self.addressShippingMethods(), function (method) { return method.addressId == addressId; });
            shippingMethod.method($('select[name="address_shipping_method"]').val());
            // Update address text here
            //self.shippingAddresses()[addressId]();
        },
        setDefaultShippingMethod: function (addressId) {
            this.addressShippingMethods.push({
                addressId: addressId,
                method: ko.observable($('#default-shipping-method').val())
            });
        },
        getShippingMethods: function (addressId) {
            var self = this;
            var url = urlBuilder.build("multiorder/checkout/shippingmethods");

            var chosenAddress = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; });

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'address': chosenAddress
                },
                dataType: 'json',
                showLoader: true,
                success: function (response) {
                    if(!response.is_error){
                        self.methods(response.methods);
                    }
                }
            });
        },
        getCurrentAddressShippingMethods: function (addressId) {
            var self = this;
            var shippingMethod = _.find(self.addressShippingMethods(), function (method) { return method.addressId == addressId; });
            if(shippingMethod) {
                return shippingMethod.method();
            }
            return '';
        },
        notifyMessage: function (message, error) {
            $('#step-fields').notification().notification('clear').notification('add', {
                error: error,
                message: message
            });
        },
        clearMessage: function () {
            $('#step-fields').notification().notification('clear');
        },
        addressFormat: function (address) {
            return new this.addressModel(address);
        },
        addressModel: function (address) {
            var self = this;
            var dataShipping = address.dataShipping;
            var dataContactAccount = address.dataContactAccount;

            self.address_id = ko.observable(address.addressId);
            self.email = ko.observable('');
            self.firstname = ko.observable('');
            self.lastname = ko.observable('');
            self.account = ko.observable(dataShipping.account);
            self.company = ko.observable(dataShipping.company);
            self.city = ko.observable(dataShipping.city);
            self.postcode = ko.observable(dataShipping.postcode);
            self.fax = ko.observable(dataShipping.fax);
            self.telephone = ko.observable(dataShipping.telephone);
            self.street = ko.observable(dataShipping.street);
            self.country_id = ko.observable(dataShipping.country_id);
            self.dataContactAccount = ko.observableArray(dataContactAccount);
            self.isValid = ko.observable(true);

            // Set default values
            $.each(dataContactAccount, function (i, item) {
                if(item['email']){
                    self.email(item['email']);
                }
                if(item['firstname']){
                    self.firstname(item['firstname']);
                }
                if(item['lastname']){
                    self.lastname(item['lastname']);
                }
                if(item['phone']){
                    self.telephone(item['phone']);
                }
                if(item['shipping_company_name']){
                    self.company(item['shipping_company_name']);
                }
                if(item['is_primary'] == 1){
                    return false;
                }
            });
        },
        placeOrder: function () {
            var self = this;
            var url = urlBuilder.build("multiorder/checkout/placeorder");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'ship': self.chosenAddresses(),
                    'methods': self.addressShippingMethods()
                },
                dataType: 'json',
                showLoader: true,
                success: function (response) {
                    if(response.is_error) {
                        self.notifyMessage(response.message, true);
                        if(response.error == 'address_validate') {
                            self.addErrorMessageToAddresses(response.address_id);
                        }
                    } else {
                        window.location = response.redirect;
                    }
                }
            });
        },
        loadAddressList: function () {
            var self = this;
            var url = urlBuilder.build("multiorder/checkout/loadaddresses");
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: 'json',
                showLoader: true,
                success: function (response) {
                    if(response.is_error) {
                        self.notifyMessage(response.message, true);
                    } else {
                        var html = '';
                        $('#shipping-address-select').html("");
                        $('#sa-notfound').hide();
                        var options = response.options;
                        $.each(options, function(index) {
                            self.shippingAddresses()[options[index].value] = ko.observable(options[index].label);
                            html += '<option value="'+ options[index].value +'">'+options[index].label+'</option>' ;
                        });

                        $('#shipping-address-select').append(html);
                    }
                }
            });
        },
        saveAddressList: function () {
            var self = this;
            var url = urlBuilder.build("multiorder/checkout/saveaddresses");
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'ship': self.chosenAddresses()
                },
                dataType: 'json',
                showLoader: true,
                success: function (response) {
                    if(response.is_error) {
                        self.notifyMessage(response.message, true);
                    } else {
                        self.notifyMessage(response.message);
                    }
                }
            });
        },
        addErrorMessageToAddresses: function (addressIds) {
            var self = this;
            $.each(addressIds, function (index, addressId) {
                var chosenAddress = _.find(self.chosenAddresses(), function (address) { return address.address_id() == addressId; });
                chosenAddress.isValid(false);
            });
        }
    });
});
