<?php

namespace Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer\CustomerGrid\Renderer;

class FullNameColumn extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Renders grid column
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        //print_r($row->getData());exit;
        return $row->getData('firstname') . " " .$row->getData('lastname');
    }
}