<?php
namespace Commercers\OnepageCheckout\Model\Checkout;
use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Commercers\OnepageCheckout\Helper\Data;
class LayoutProcessorPlugin
{
    public function afterProcess(
        LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $optionDivision = $this->getOptions();
//        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][Data::EGFRA_DIVISION] = [
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children'][Data::EGFRA_DIVISION] = [
//            'component' => 'Magento_Ui/js/form/element/select',
            'component' => 'Commercers_OnepageCheckout/js/form/element/select',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Commercers_OnepageCheckout/form/element/select',
//                'elementTmpl' => 'ui/form/element/select'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . Data::EGFRA_DIVISION,
            'label' => __('Egfra Division'),
            'provider' => 'checkoutProvider',
            'visible' => true,
            'optionsCaption' => __("Please select"),
            'filterBy' => null,
            'customEntry' => null,
            'validation' => [
                'required-entry' => true
            ],
            'sortOrder' => 0,
            'options' =>  $optionDivision['allOptions'],
//            'options' =>  $this->getOptions()
        ];
        if($optionDivision["countDivision"] > 1){
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['before-form']['children'][Data::EGFRA_DIVISION]['notice'] = __('Please select a division to continue check-out.');
        }
//        Custom Filed SalesForce Contact
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['shipping-address-select-alt2'] = [
            'component' => 'Magento_Ui/js/form/element/select',
            'additionalClasses' => 'shipping-address-select-alt-custom',
            'config' => [
                'customScope' => 'shippingAddress',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select'
            ],
            'dataScope' => 'shippingAddress.' . 'shipping_address_select_alt2',
            'label' => __('Choose a contact'),
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [],
            'filterBy' => null,
            'customEntry' => null,
            'sortOrder' => 0,
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['commercers-account-number'] = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                'disabled'=> 'disabled',
                'customScope' => 'shippingAddress',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.' . 'commercers_account_number',
            'label' => __('Customer number'),
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [],
            'filterBy' => null,
            'customEntry' => null,
            'sortOrder' => 10
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['firstname']['sortOrder'] = 20;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['firstname']['validation']['required-entry'] = true;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['lastname']['validation']['required-entry'] = true;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['lastname']['sortOrder'] = 30;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['telephone']['sortOrder'] = 40;
        //Remove telephone tooltip
        unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['telephone']['config']['tooltip']);
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['shipping_email'] = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                'customScope' => 'shippingAddress',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.' . 'shipping_email',
            'label' => __('Email Address'),
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [],
            'filterBy' => null,
            'customEntry' => null,
            'sortOrder' => 50,
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['button-contact']['sortOrder'] = 60;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['company']['sortOrder'] = 70;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['street']['sortOrder'] = 80;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['street']['children'][1]['visible'] = false;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['postcode']['sortOrder'] = 90;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['city']['sortOrder'] = 100;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['country_id']['sortOrder'] = 110;
        unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['region']);
        unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['region_id']);
        return $jsLayout;
    }
    public function getOptions(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('\Magento\Customer\Model\Session');
        $customer = $objectManager->create('\Magento\Customer\Model\Customer');
        $attributeOptions = [];
        $allOptions = [];
        $resultData = [];
        if($customerSession->isLoggedIn()) {
            $customerId =$customerSession->getCustomer()->getId();
            $customer = $customer->load($customerId);
            $attributeOptions = explode(",", $customer->getData(Data::EGFRA_DIVISION));
        }
        $countDivision = count($attributeOptions);
        $resultData['countDivision'] = $countDivision;
        if($countDivision > 1){
            $allOptions[] = [
                'label' => __("Please select"),
                'value' => 0,
                'class' => 'option_division'
            ];
        }
        $attributeOptionAll = $objectManager->create('\Magento\Eav\Model\Config')
            ->getAttribute('customer', Data::EGFRA_DIVISION);
        foreach ($attributeOptionAll->getSource()->getAllOptions() as $key => $attribute)
        {
            if(in_array($attribute['value'], $attributeOptions)){
                $allOptions[] = [
                    'label' => $attribute['label'],
                    'value' => $attribute['value'],
                    'class' => 'option_division'
                ];
            }
        }
        $resultData['allOptions'] = $allOptions;
        return $resultData;
    }
}