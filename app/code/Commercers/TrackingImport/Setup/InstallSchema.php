<?php

namespace Commercers\TrackingImport\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface

{
    public function install(
    SchemaSetupInterface $setup, 
            ModuleContextInterface $context) {
        $setup->startSetup();
        $conn = $setup->getConnection();
        $tableName = $setup->getTable('trackingimport_log');
        $context->getVersion();
        if($conn->isTableExists($tableName) != true){
            $table = $setup->getConnection()->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                        'ID'
                        )
                    ->addColumn(
                        'order_number',
                        Table::TYPE_INTEGER,
                        null,
                        [],
                        'Order Number'
                        )
                    ->addColumn(
                        'carrier_code',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'Carrier Code'
                        )
                    ->addColumn(
                        'lot_number',
                        Table::TYPE_INTEGER,
                        null,
                        [],
                        'Lot Number'
                        )
                    ->setOption('charset','utf8');
            $conn->createTable($table);
        }
        
        $setup->endSetup();
    }
}