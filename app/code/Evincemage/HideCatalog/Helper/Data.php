<?php
namespace Evincemage\HideCatalog\Helper;
use Evincemage\ProductInquiry\Model\InquiryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\ScopeInterface;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MODE_HIDE_BY_DEFAULT = 'hide';
    const MODE_SHOW_BY_DEFAULT = 'show';
    const USE_NONE = -1;
    const LABEL_NONE = 'NONE';
    const XML_PATH_MODULE_STATUS = 'hidecatalog_config/general/enable';
    const XML_GUEST_REDIRECT = 'hidecatalog_config/customer_redirect/entity_hidden_behaviour_guest';
    const XML_CUSTOMER_REDIRECT = 'hidecatalog_config/customer_redirect/entity_hidden_behaviour_customer';
    protected $_customerSession;
    public $storeManager;
    private $redirectFactory;
    public function __construct(Context $context, StoreManager $storeManager, \Magento\Customer\Model\SessionFactory $sessionFactory, \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory, \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup)
    {
        $this->_customerSession = $sessionFactory;
        $this->_customerGroup   = $customerGroup;
        $this->storeManager     = $storeManager;
        $this->redirectFactory  = $redirectFactory;
        parent::__construct($context);
    }
    
    public function getCustomerGroupId()
    {
        $customer = $this->_customerSession->create();
        if ($customer->isLoggedIn()) {
            return $customerGroup = $customer->getCustomer()->getGroupId();
        } else {
            return $customerGroup = 0;
        }
    }
    public function getGroups()
    {
        $customerGroups = $this->_customerGroup->load();
        return $customerGroups;
    }
    public function isModuleActive()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_MODULE_STATUS, ScopeInterface::SCOPE_STORE);
    }
    public function catalogRedirect()
    {
         $customer = $this->_customerSession->create();
        if ($customer->isLoggedIn()) {
            $customerRedirect = $this->scopeConfig->getValue(self::XML_CUSTOMER_REDIRECT, ScopeInterface::SCOPE_STORE);
            if ($customerRedirect == 404) {
                return $this->scopeConfig->getValue(self::XML_CUSTOMER_REDIRECT, ScopeInterface::SCOPE_STORE);
            } else {
                return $this->scopeConfig->getValue('hidecatalog_config/customer_redirect/entity_hidden_redirect_customer', ScopeInterface::SCOPE_STORE);
            }
        } else {
            $customerRedirect = $this->scopeConfig->getValue(self::XML_GUEST_REDIRECT, ScopeInterface::SCOPE_STORE);
            if ($customerRedirect == 404) {
                return $this->scopeConfig->getValue(self::XML_GUEST_REDIRECT, ScopeInterface::SCOPE_STORE);
            } else {
                return $this->scopeConfig->getValue('hidecatalog_config/customer_redirect/entity_hidden_redirect_guest', ScopeInterface::SCOPE_STORE);
            }
        }
    }
    
    public function ConfigProductsetting()
    {
        $customerid = $this->scopeConfig->getValue('hidecatalog_config/hidecatalog/product_default_hide', ScopeInterface::SCOPE_STORE);
        $customerid = explode(',', $customerid);
        return ($customerid);
    }
    public function ConfigProductCollectionsetting()
    {
        $customerid = $this->scopeConfig->getValue('hidecatalog_config/hidecatalog/product_default_hide', ScopeInterface::SCOPE_STORE);
        return $customerid;
        
    }
    public function ConfigCategorysetting()
    {
        $customerid = $this->scopeConfig->getValue('hidecatalog_config/hidecategory/category_default_hide', ScopeInterface::SCOPE_STORE);
        $customerid = explode(',', $customerid);
        return ($customerid);
    }
    public function CatalogLoadVisible()
    {
        $ConfigProductsettings = $this->ConfigProductsetting();
        
        if (in_array(self::USE_NONE, $ConfigProductsettings)) {
            
            return false;
            
        } else {
            if (in_array($this->getCustomerGroupId(), $ConfigProductsettings)) {
                
                return true;
                
            } else {
                return false;
            }
        }
        
    }
    public function CategoryLoadVisible()
    {
        $ConfigCategorysetting = $this->ConfigCategorysetting();
        
        if (in_array(self::USE_NONE, $ConfigCategorysetting)) {
            
            return false;
            
        } else {
            if (in_array($this->getCustomerGroupId(), $ConfigCategorysetting)) {
                
                return true;
                
            } else {
                return false;
            }
        }
        
    }
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }
}