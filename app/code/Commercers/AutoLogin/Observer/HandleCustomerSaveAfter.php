<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class HandleCustomerSaveAfter
 * @package Commercers\AutoLogin\Observer
 */
class HandleCustomerSaveAfter implements ObserverInterface {
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;
    /**
     * @var \Commercers\AutoLogin\Model\AutoLoginFactory
     */
    protected $_autoLoginFactory;

    /**
     * HandleCustomerSaveAfter constructor.
     * @param \Magento\Backend\Helper\Js $jsHelper
     * @param \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory
     */
    public function __construct(
        \Magento\Backend\Helper\Js $jsHelper,
        \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory
    )
    {
        $this->_jsHelper = $jsHelper;
        $this->_autoLoginFactory = $autoLoginFactory;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $mainAccount = $observer->getCustomer();

        $customerGridValue = $observer->getRequest()->getParam('customers_grid_value');
        $subAccountIds = $this->_jsHelper->decodeGridSerializedInput($customerGridValue);
        //delete all record of main account
        $this->_deleteAllSubAccountOfMainAccount($mainAccount->getId());
        foreach ($subAccountIds as $subAccountId){
            $autoLoginModel = $this->_autoLoginFactory->create();
            $autoLoginData = [
                'main_account_id' => $mainAccount->getId(),
                'sub_account_id' => $subAccountId,
                'token' => md5(uniqid())
            ];
            $autoLoginModel->addData($autoLoginData)->save();
        }
    }

    /**
     * @param $mainAccountId
     */
    private function _deleteAllSubAccountOfMainAccount($mainAccountId){
        $collection = $this->_autoLoginFactory->create()->getCollection();
        $collection->addFieldToFilter('main_account_id',$mainAccountId);
        foreach ($collection as $item){
            $autoLogin = $this->_autoLoginFactory->create()->load($item['entity_id']);
            $autoLogin->delete();
        }
    }
}