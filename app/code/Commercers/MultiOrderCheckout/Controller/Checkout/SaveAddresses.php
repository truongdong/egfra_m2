<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

class SaveAddresses extends AbstractIndex
{
	public function execute()
	{
		try {
			$addresses = $this->_request->getParam('ship');
			$customerId = $this->_customerSession->getCustomerId();
			$customerEmail = $this->_customerSession->getCustomer()->getEmail();
			foreach ($addresses as $address) {
				$accountId = $address['account'];
				$addressList = $this->_addressListFactory->create()->getCollection()
					->addFieldToFilter('customer_id',$customerId)
					->addFieldToFilter('shipping_address_account_id',$accountId)
				;
				if(!$addressList->count()) {
					$addressModel = $addressList = $this->_addressListFactory->create();
					$addressModel->setCustomerId($customerId);
					$addressModel->setCustomerEmail($customerEmail);
					$addressModel->setShippingAddressAccountId($accountId);
					$addressModel->save();
				}
			}

			$result = [
				'is_error' => false,
				'message' => __('Address list was saved successfully.')
			];
		} catch (LocalizedException $e) {
			$result = [
				'is_error' => true,
				'message' => $e->getMessage()
			];
		} catch (\Exception $e) {
			$this->logger->critical($e->getMessage());
			$result = [
				'is_error' => true,
				'message' => __('We are unable to process your request. Please, try again later.')
			];
		}

		return $this->_returnResultJson($result);
	}
}