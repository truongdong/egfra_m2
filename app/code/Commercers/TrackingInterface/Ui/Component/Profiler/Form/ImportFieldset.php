<?php

namespace Commercers\TrackingInterface\Ui\Component\Profiler\Form;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Ui\Component\Form\Fieldset;


class ImportFieldset extends Fieldset implements ComponentVisibilityInterface
{
    /**
     * CustomFieldset constructor.
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        \Magento\Framework\Registry $registry,
        array $components = [],
        array $data = []
    ) {
        $this->context = $context;
        $this->registry = $registry;
        parent::__construct($context, $components, $data);
    }

    /**
     * @return bool
     */
    public function isComponentVisible(): bool
    {

        $profiler = $this->registry->registry('current_profiler');
        //echo '<pre>'; print_r($profiler->getData());exit;
        $visible = false;
        if($profiler 
        && in_array($profiler->getDataSource(), array('tracking_import')) 
        //&& $profiler->getType() == \Commercers\Profilers\Model\Constant::IMPORT_PROFILER
        ){

            $visible = true;
        }
       // var_dump($visible);exit;
        return (bool)$visible;
    }
}
