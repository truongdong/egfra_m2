<?php

namespace Evincemage\HideCatalog\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Evincemage\HideCatalog\Helper\Data $helper,        
        array $data = []
    ) {
        $this->helper = $helper;
    }

    public function getAllOptions()
    {
        $this->_options = array(

            array(
                'value' =>(string) \Evincemage\HideCatalog\Helper\Data::USE_NONE,
                'label' => \Evincemage\HideCatalog\Helper\Data::LABEL_NONE
            )
        );
        foreach ($this->helper->getGroups() as $group) {
            $this->_options[] = array(
               'value' => (string)$group->getId(),
               'label' => $group->getCode(),
           );
        }
        return $this->_options;
    }
 

}