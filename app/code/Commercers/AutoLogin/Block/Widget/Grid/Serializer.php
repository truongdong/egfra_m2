<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */

namespace Commercers\AutoLogin\Block\Widget\Grid;

/**
 * @api
 * @deprecated 100.2.0 in favour of UI component implementation
 * @method string|array getInputNames()
 * @since 100.0.2
 */
class Serializer extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * @var \Commercers\AutoLogin\Model\AutoLoginFactory
     */
    protected $_autoLoginFactory;

    /**
     * Serializer constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Registry $registry
     * @param \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Registry $registry,
        \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory,
        array $data = []
    )
    {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_coreRegistry = $registry;
        $this->_autoLoginFactory = $autoLoginFactory;
        parent::__construct($context, $data);
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $grid = $this->getGridBlock();
        if (is_string($grid)) {
            $grid = $this->getLayout()->getBlock($grid);
        }
        if ($grid instanceof \Magento\Backend\Block\Widget\Grid) {
            $subAccounts  = $this->__getAllSubAccountOfMainAccount($this->getMainAccountId());
            if(!empty($subAccounts)){
                $this->setGridBlock($grid)->setSerializeData($subAccounts);
            }else{
                $this->setGridBlock($grid)->setSerializeData($grid->{$this->getCallback()}());
            }

        }
        return parent::_prepareLayout();
    }

    /**
     * Set serializer template
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('Commercers_AutoLogin::widget/grid/serializer.phtml');
    }
    /**
     * @return mixed|null
     */
    public function getMainAccountId()
    {
        return $this->_coreRegistry->registry(\Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID);
    }
    /**
     * Get grid column input names to serialize
     *
     * @param bool $asJSON
     * @return string|array
     */
    public function getColumnInputNames($asJSON = false)
    {
        if ($asJSON) {
            return $this->_jsonEncoder->encode((array)$this->getInputNames());
        }
        return (array)$this->getInputNames();
    }


    /**
     * Get object data as JSON
     *
     * @return string
     */
    public function getDataAsJSON()
    {
        $result = [];
        $inputNames = $this->getInputNames();
        if ($serializeData = $this->getSerializeData()) {
            $result = $serializeData;
        } elseif (!empty($inputNames)) {
            return '{}';
        }
        return $this->_jsonEncoder->encode($result);
    }
    /**
     * @param $mainAccountId
     */
    private function __getAllSubAccountOfMainAccount($mainAccountId){
        $collection = $this->_autoLoginFactory->create()->getCollection();
        $collection->addFieldToFilter('main_account_id',$mainAccountId);
        $subAccount = array();
        foreach ($collection as $item){
            $subAccount[] = $item['sub_account_id'];
        }
        return $subAccount;
    }
}
