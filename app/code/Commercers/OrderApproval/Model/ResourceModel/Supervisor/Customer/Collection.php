<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\OrderApproval\Model\ResourceModel\Supervisor\Customer;

/**
 * CMS page collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'supervisor_customer_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'commercers_order_approval_supervisor_customer_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'orderapproval_supervisor_customer_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Commercers\OrderApproval\Model\Supervisor\Customer::class, \Commercers\OrderApproval\Model\ResourceModel\Supervisor\Customer::class);
        $this->_map['fields']['supervisor_customer_id'] = 'main_table.supervisor_customer_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }
}
