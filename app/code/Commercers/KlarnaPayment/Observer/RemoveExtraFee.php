<?php
namespace Commercers\KlarnaPayment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
class RemoveExtraFee implements ObserverInterface
{
    /** @var \Magento\Checkout\Model\SessionFactory */
    protected $_checkoutSessionFactory;

    /** @var \Commercers\KlarnaPayment\Helper\ConfigFactory */
    protected $_configKlarnaFactory;

    /** @var \Magento\Checkout\Helper\CartFactory */
    protected $_cartFactory;

    /* @var $_request  \Magento\Framework\App\RequestInterface */
    protected $_request;

    public function __construct(
        \Magento\Checkout\Model\SessionFactory $checkoutSessionFactory,
        \Commercers\KlarnaPayment\Helper\ConfigFactory $configKlarnaFactory,
        \Magento\Checkout\Helper\CartFactory $cartFactory,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->_checkoutSessionFactory = $checkoutSessionFactory;
        $this->_configKlarnaFactory = $configKlarnaFactory;
        $this->_cartFactory = $cartFactory;
        $this->_request = $request;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $this->_request;
        if (
        ($request->getRouteName() === 'checkout' || $request->getRouteName() === 'klarna') &&
        ($request->getControllerName() === 'onepage' || $request->getControllerName() === 'index' || $request->getControllerName() === 'payment') &&
        ($request->getActionName() === 'index' || $request->getActionName() === 'success' || $request->getActionName() === 'ajaxextrafee')
        ) {
            return true;
        }
        if(
            $this->getFullActionName() === 'customer_section_load' || $this->getFullActionName() === 'captcha_refresh_index' || $this->getFullActionName() === 'checkout_klarna_cookie'
        ) {
            return true;
        }
        $cartHelper = $this->_cartFactory->create();
        $item = $this->_get_applied_fee_item($cartHelper);
        if ($item !== null && !$item->isDeleted()) {
            $itemId = $item->getItemId();
            if ($request->getRouteName() !== 'checkout') {
                $cartHelper->getCart()->removeItem($itemId)->save();
            } else {
                $cartHelper->getCart()->removeItem($itemId);
            }
        }
        return true;
    }
    public function _get_applied_fee_item($cartHelper)
    {
        $items = $cartHelper->getCart()->getItems();
        /* @var $_configKlarnaFactory Commercers\KlarnaPayment\Helper\Config */
        $configHelperKlarna = $this->_configKlarnaFactory->create();
        $products = $configHelperKlarna->get_products_for_fee_klarna_ratenkauf();
        $product_array = explode(',', $products);
        foreach ($items as $item) {
            if (in_array($item->getProductId(), $product_array)) {
                return $item;
            }
        }
        return null;
    }
    public function getFullActionName(){
        return $this->_request->getFullActionName();
    }
}