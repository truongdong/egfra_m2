<?php
namespace Commercers\OnepageCheckout\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteRepository;
class SaveCustomerAttributeToOrderObserver implements ObserverInterface
{
    protected $quoteRepository;
    public function __construct(
        QuoteRepository $quoteRepository
    ){
        $this->quoteRepository = $quoteRepository;
    }    public function execute(EventObserver $observer)
{        $order = $observer->getOrder();
$quote = $this->quoteRepository->get($order->getQuoteId());
$order->setEgfraDivision($quote->getEgfraDivision());
$order->setShippingaddressesAccountId($quote->getShippingaddressesAccountId());
return $this;
}}