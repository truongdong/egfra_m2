<?php

namespace Commercers\MultiOrderCheckout\Block\Checkout;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\ShortcutInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;

class Link extends Template implements ShortcutInterface
{
	const ALIAS_ELEMENT_INDEX = 'alias';

	/**
	 * Multishipping helper
	 *
	 * @var \Magento\Multishipping\Helper\Data
	 */
	protected $helper;

	/**
	 * @var UrlInterface
	 */
	private $urlBuilder;

	public function __construct(
		Context $context,
		UrlInterface $urlBuilder,
		array $data = [],
		\Magento\Multishipping\Helper\Data $helper
	) {
		parent::__construct($context, $data);

		$this->urlBuilder = $urlBuilder;
		$this->helper = $helper;
	}

	protected function _toHtml()
	{
		if (!$this->helper->isMultishippingCheckoutAvailable()) {
			return '';
		}

		return parent::_toHtml();
	}

	/**
	 * Get shortcut alias
	 *
	 * @return string
	 */
	public function getAlias()
	{
		return $this->getData(self::ALIAS_ELEMENT_INDEX);
	}

	/**
	 * Returns container id
	 *
	 * @return string
	 */
	public function getContainerId(): string
	{
		return $this->getData('button_id');
	}

	public function getCheckoutUrl()
	{
		return $this->getUrl('multiorder/checkout', ['_secure' => true]);
	}
}
