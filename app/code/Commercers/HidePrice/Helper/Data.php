<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/24/18
 * Time: 21:14
 */
namespace Commercers\HidePrice\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Commercers\HidePrice\Model\HidePriceFactory;

class Data extends AbstractHelper {

	/**
	 * @var CustomerSession
	 */
	protected $_customerSession;

    /**
	 * @var HidePriceFactory
	 */
	protected $_hidePriceFactory;

	public function __construct(
		Context $context,
		CustomerSession $customerSession,
        HidePriceFactory $hidePriceFactory
	) {
		$this->_customerSession = $customerSession;
        $this->_hidePriceFactory = $hidePriceFactory;

		parent::__construct($context);
	}

	public function isHidePrice($customerId = null)
	{
		if(!$customerId) {
			$customerId = $this->_customerSession->getCustomerId();
		}
		
		$hidePrice = $this->_hidePriceFactory->create()->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
			->getFirstItem()
        ;

		return (bool)$hidePrice->getHidePrice();
	}

}