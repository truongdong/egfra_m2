<?php

namespace Commercers\ExportInvoice\Block\Adminhtml;

class Manual extends \Magento\Framework\View\Element\Template {
    protected function _construct()
    {
        parent::_construct();
        $this->setId('commercers_exportinvoice_manual_index');
    }

}