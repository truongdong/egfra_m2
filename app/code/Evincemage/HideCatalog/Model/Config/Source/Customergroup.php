<?php
namespace Evincemage\HideCatalog\Model\Config\Source;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
class Customergroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function __construct(
    \Magento\Backend\Block\Template\Context $context,
    \Evincemage\HideCatalog\Helper\Data $helper,        
    array $data = []
) {
    $this->helper = $helper;         
}
    public function getAllOptions()
    {
         if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    'value' => \Evincemage\HideCatalog\Helper\Data::USE_NONE,
                    'label' => \Evincemage\HideCatalog\Helper\Data::LABEL_NONE
                )
            );
            foreach ($this->helper->getGroups() as $group) {
                $this->_options[] = array(
                     'value' => $group->getId(),
                    'label' => (string)$group->getCode(),
                );
            }
        }
        return $this->_options;
    }
}