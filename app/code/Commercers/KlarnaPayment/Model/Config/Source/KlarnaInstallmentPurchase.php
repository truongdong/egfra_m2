<?php
namespace Commercers\KlarnaPayment\Model\Config\Source;

class KlarnaInstallmentPurchase {

    /** @var
     * \Commercers\KlarnaPayment\Helper\Config
     */
    protected $_helperConfig;

    /** @var
     * \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /** @var
     * \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Commercers\KlarnaPayment\Helper\Config $helperConfig
    )
    {
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_helperConfig = $helperConfig;
    }
    public function toOptionArray($is_multiselect = false){
        $options = [];

        $attributeSetKlarna = $this->_helperConfig->get_attributeset_for_fee_klarna();
//        return ($attributeSetKlarna);exit;
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToFilter('attribute_set_id', array( 'eq' => $attributeSetKlarna));
        $options = array();
        foreach ($collection as $row) {
            $id=$row->getId();
            $name = $this->_productFactory->create()->load($id)->getName();
            array_push($options, array(
                'value' => $id,
                'label' => $name,
            ));
        }
        return $options;
    }
}