<?php

namespace Commercers\OrderApproval\Block\Email;

class Result extends \Magento\Framework\View\Element\Template
{
    protected $_registry;

    protected $_coreSession;

    protected $_dataFactory;

    protected $_dataCollectionFactory;

    public function __construct(
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\Order\Item $orderItem,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Registry $registry

    )
    {
        parent::__construct($context);
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->_dataFactory = $dataFactory;
        $this->_coreSession = $coreSession;
        $this->_orderItem = $orderItem;
        $this->_order = $order;
        $this->_registry = $registry;
    }

    public function getParams()
    {
        $params = $this->getRequest()->getParams();
        return $params;
    }

    public function getDataApproval()
    {
        $dataModel = $this->_dataFactory->create();
        $params = $this->getParams();
        $approvals = $dataModel->getCollection()
            ->addFieldToFilter('approval_code', array('eq' => $params["approval_code"]))
            ->getFirstItem();
        return $approvals->getData();
    }

    public function getDataApproves()
    {
        $dataModel = $this->_dataFactory->create();
        $params = $this->getParams();
        if(!isset($params["order_id"])){
            return [];
        }
        $approvals = $this->_dataCollectionFactory->create();
        $approvals->addFieldToFilter('order_id', array('eq' => $params["order_id"]))
            ->addFieldToFilter('approval_code', array('in' => (unserialize(base64_decode($params["approval_code"]))) ))
            ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_APPROVED ));
        return $approvals->getData();
    }

    public function getDataDisapproves()
    {
        $dataModel = $this->_dataFactory->create();
        $params = $this->getParams();
        if(!isset($params["order_id"])){
            return [];
        }
        $approvals = $this->_dataCollectionFactory->create();
        $approvals->addFieldToFilter('order_id', array('eq' => $params["order_id"]))
            ->addFieldToFilter('approval_code', array('in' => (unserialize(base64_decode($params["approval_code"]))) ))
        ->addFieldToFilter('status', array('eq' => $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED ));
        return $approvals->getData();
    }
}
