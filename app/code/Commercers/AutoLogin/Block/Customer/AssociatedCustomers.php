<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */

namespace Commercers\AutoLogin\Block\Customer;

use Magento\Framework\View\Element\Template;

/**
 * Class AssociatedCustomers
 * @package Commercers\AutoLogin\Block\Customer
 */
class AssociatedCustomers extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'Commercers_AutoLogin::customer/associated_customers.phtml';
    protected $_customerSession;
    protected $_autoLoginFactory;
    /**
     * AssociatedCustomers constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory,
        array $data = []
    )
    {
        $this->_autoLoginFactory = $autoLoginFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }
    public function prepareCollection(){
        $currentCustomerId  = $this->_customerSession->getCustomerId();
        $collection = $this->_autoLoginFactory->create()->getCollection();
        $collection->getSelect()
            ->join(array('customer_entity' => 'customer_entity'),'main_table.sub_account_id= customer_entity.entity_id',
                array('*')
            );
        $collection->addFieldToFilter('main_account_id',$currentCustomerId);
        return $collection;
    }
}