<?php
namespace Commercers\OrderApproval\Controller\Index;

use Commercers\OrderApproval\Controller\Index\AbstractIndex;

class Disapprove extends AbstractIndex {

    public function execute()
    {
        return $this->changeItemStatus('disapprove');
    }
}
