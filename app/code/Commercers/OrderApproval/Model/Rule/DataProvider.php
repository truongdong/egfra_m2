<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\OrderApproval\Model\Rule;

use Commercers\OrderApproval\Model\Rule;
use Commercers\OrderApproval\Model\ResourceModel\Rule\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Framework\AuthorizationInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \Commercers\OrderApproval\Model\ResourceModel\Rule\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var AuthorizationInterface
     */
    private $auth;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $ruleCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     * @param AuthorizationInterface|null $auth
     * @param RequestInterface|null $request
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $ruleCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null,
        ?AuthorizationInterface $auth = null,
        ?RequestInterface $request = null
    ) {
        $this->collection = $ruleCollectionFactory->create();
        $this->collectionFactory = $ruleCollectionFactory;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->auth = $auth ?? ObjectManager::getInstance()->get(AuthorizationInterface::class);
        $this->meta = $this->prepareMeta($this->meta);
        $this->request = $request ?? ObjectManager::getInstance()->get(RequestInterface::class);
    }

    /**
     * Find requested rule.
     *
     * @return Rule|null
     */
    private function findCurrentRule(): ?Rule
    {
        if ($this->getRequestFieldName() && ($ruleId = (int)$this->request->getParam($this->getRequestFieldName()))) {
            //Loading data for the collection.
            $this->getData();
            echo 12333213;exit;
            return $this->collection->getItemById($ruleId);
        }

        return null;
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->collection = $this->collectionFactory->create();
        $items = $this->collection->getItems();
        /** @var $rule \Commercers\OrderApproval\Model\Rule */
        foreach ($items as $rule) {
            $this->loadedData[$rule->getId()] = $rule->getData();
            if ($rule->getCustomLayoutUpdateXml() || $rule->getLayoutUpdateXml()) {
                //Deprecated layout update exists.
                $this->loadedData[$rule->getId()]['layout_update_selected'] = '_existing_';
            }
        }

        $data = $this->dataPersistor->get('commercers_orderapproval');
        if (!empty($data)) {
            $rule = $this->collection->getNewEmptyItem();
            $rule->setData($data);
            $this->loadedData[$rule->getId()] = $rule->getData();
            if ($rule->getCustomLayoutUpdateXml() || $rule->getLayoutUpdateXml()) {
                $this->loadedData[$rule->getId()]['layout_update_selected'] = '_existing_';
            }
            $this->dataPersistor->clear('commercers_orderapproval');
        }

        return $this->loadedData;
    }

    /**
     * @inheritDoc
     */
    // public function getMeta()
    // {
    //     $meta = parent::getMeta();

    //     if (!$this->auth->isAllowed('Commercers_OrderApproval::save_design')) {
    //         $designMeta = [
    //             'design' => [
    //                 'arguments' => [
    //                     'data' => [
    //                         'config' => [
    //                             'disabled' => true
    //                         ]
    //                     ]
    //                 ]
    //             ],
    //             'custom_design_update' => [
    //                 'arguments' => [
    //                     'data' => [
    //                         'config' => [
    //                             'disabled' => true
    //                         ]
    //                     ]
    //                 ]
    //             ]
    //         ];
    //         $meta = array_merge_recursive($meta, $designMeta);
    //     }

    //     //List of custom layout files available for current rule.
    //     $options = [['label' => 'No update', 'value' => '_no_update_']];
    //     if ($rule = $this->findCurrentRule()) {
    //         //We must have a specific rule selected.
    //         //If custom layout XML is set then displaying this special option.
    //         if ($rule->getCustomLayoutUpdateXml() || $rule->getLayoutUpdateXml()) {
    //             $options[] = ['label' => 'Use existing layout update XML', 'value' => '_existing_'];
    //         }
    //     }
    //     $customLayoutMeta = [
    //         'design' => [
    //             'children' => [
    //                 'custom_layout_update_select' => [
    //                     'arguments' => [
    //                         'data' => ['options' => $options]
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ];
    //     $meta = array_merge_recursive($meta, $customLayoutMeta);

    //     return $meta;
    // }
}
