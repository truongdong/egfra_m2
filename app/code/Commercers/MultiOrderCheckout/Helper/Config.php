<?php

namespace Commercers\MultiOrderCheckout\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Config extends AbstractHelper {

	const SYS_XML_SECTION = 'multi_order';

	const SYS_XML_GENERAL_GROUP = 'general_group';

	public function __construct(
		Context $context
	) {
		parent::__construct($context);
	}

	public function getConfigValue($field, $group = self::SYS_XML_GENERAL_GROUP, $section = self::SYS_XML_SECTION, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$section.'/'.$group.'/'.$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfigValue($field, $storeId = null){
		return $this->getConfigValue($field, self::SYS_XML_GENERAL_GROUP, self::SYS_XML_SECTION, $storeId);
	}

	public function isModuleActive(){
		return $this->getGeneralConfigValue('is_active') === '1';
	}
}