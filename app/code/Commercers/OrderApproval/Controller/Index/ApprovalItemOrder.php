<?php
namespace Commercers\OrderApproval\Controller\Index;

use Commercers\OrderApproval\Controller\Index\AbstractIndex;
use Magento\Framework\Controller\ResultFactory;

class ApprovalItemOrder extends AbstractIndex {

    public function execute()
    {
        $getParams = $this->getRequest()->getParams();
        $code = $getParams["approvalCode"];
        $supervisorName = $getParams["supervisorName"];
        $actionSubmit = $getParams["actionSubmit"];
        if ($code) {
            //find item need to be approve by code and email
            $dataModel = $this->_dataFactory->create();
            $approvals = $dataModel->getCollection()
                ->addFieldToFilter('approval_code', array('eq' => $code));
            $approval = $approvals->getFirstItem();
            if ($approvals->getSize()) {
                // Action = approve
                if ($actionSubmit == 'approve') {
                    $orderId = $approval->getOrderId();
                    $order = $this->_order->load($orderId);
                    $result['approved'] = true;
                    $result['approved_code'] = $code;
                    $result['message'] = __("The item %1 in your order # %2 has been approved",
                        [
                            $approval->getOrderItemName(),
                            $order->getIncrementId()
                        ]
                    );
                    $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                    $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_APPROVED);
                    $approval->save();
                    $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                        'order_approval' => $approval,
                        'supervisor_name' => $supervisorName
                    ]);
                }
//              Action = disapprove
                if ($actionSubmit == 'disapprove') {
                    $orderId = $approval->getOrderId();
                    $order = $this->_order->load($orderId);
                    if(intval($order->getTotalQtyOrdered() == 1) && $order->getState() == \Magento\Sales\Model\Order::STATE_NEW){
                        $result['approve_all'] = 6;
                        $result['disapproved'] = 1;
                        $result['approval_code'] = $approval->getApprovalCode();
                        $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED;
                        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                        $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                        $approval->save();
                        try {
                            $order->cancel()->save();
                            return $this->responseResult($result);
                        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                            if ($this->_objectManager->get('Magento\Checkout\Model\Session')->getUseNotice(true)) {
                                $this->messageManager->addNotice($e->getMessage());
                            } else {
                                $this->messageManager->addError($e->getMessage());
                            }
                        }
                    }
                    $result['approved_code'] = $code;
                    $result['approved'] = false;
                    $result['message'] = __("The item %1 in your order # %2 has not been approved",
                        [
                            $approval->getOrderItemName(),
                            $order->getIncrementId()
                        ]
                    );
                    $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                    $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                    $approval->save();
                    $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                        'order_approval' => $approval,
                        'supervisor_name' => $supervisorName
                    ]);
                }
            }
        }
        return $this->responseResult($result);
    }
    public function responseResult($result)
    {
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData($result);
        return $response;
    }
}
