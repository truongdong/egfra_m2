<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\OrderApproval\Model\Rule\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var \Commercers\OrderApproval\Model\Rule
     */
    protected $cmsRule;

    /**
     * Constructor
     *
     * @param \Commercers\OrderApproval\Model\Rule $cmsRule
     */
    public function __construct(\Commercers\OrderApproval\Model\Rule $cmsRule)
    {
        $this->cmsRule = $cmsRule;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->cmsRule->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
