<?php

namespace Commercers\DHLShipmentTracking\Model\Config\Source;

class Status implements \Magento\Framework\Option\ArrayInterface
{
     public function toOptionArray()
     {
          return [
               ['value' => 'ZU', 'label' => __('Zustellung erfolgreich')],
               ['value' => 'PO', 'label' => __('In Zustellung')],
               ['value' => 'EE', 'label' => __('Ziel-Paketzentrum')],
               ['value' => 'AE', 'label' => __('Abholung erfolgreich')],
               ['value' => 'AA', 'label' => __('Transport zum Start-Paketzentrum')],
               ['value' => 'ES', 'label' => __('Einlieferung in PACKSTATION')]
          ];
     }
}