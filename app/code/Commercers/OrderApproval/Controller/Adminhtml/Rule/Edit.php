<?php
namespace Commercers\OrderApproval\Controller\Adminhtml\Rule;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Commercers\OrderApproval\Model\RuleFactory;
use Magento\Framework\Registry;

class Edit extends Action
{
	/**
	 * @var RuleFactory
	 */
	protected $_ruleFactory;

	/**
	 * @var Registry
	 */
	protected $_registry;

	protected $_session;

	public function __construct(
		Context $context,
		RuleFactory $ruleFactory,
		Registry $registry
	) {
		parent::__construct($context);

		$this->_ruleFactory = $ruleFactory;
		$this->_registry = $registry;
		$this->_session = $context->getSession();
	}

	public function execute()
	{
		$id = $this->getRequest()->getParam('order_approval_rule_id');

		$model = $this->_ruleFactory->create();
		$ruleName = '';

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
		if ($id) {
			$model->load($id);
            $ruleName = $model->getName();
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Order Approval Rule %1',$ruleName));
            $rule = $model->getData();
            $this->_registry->register('orderapproval_rule', $rule);
		}else{
            $resultPage->getConfig()->getTitle()->prepend(__('Add A New Order Approval Rule'));
        }

		$data = $this->_session->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		return $resultPage;
	}

	protected function _isAllowed()
	{
		return $this->_authorization->isAllowed('Commercers_OrderApproval::index');
	}
}
