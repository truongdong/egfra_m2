<?php

namespace Commercers\OnepageCheckout\Model;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Setup\Context;
use Magento\Framework\App\CacheInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
use Magento\Eav\Setup\EavSetup;
class  EavInstall extends EavSetup {

    protected $csvReader;

    protected $_shippingAddressFactory;

    protected $_resource;
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Commercers\OnepageCheckout\Model\ShippingAddressFactory $shippingAddressFactory,
        ModuleDataSetupInterface $setup,
        Context $context,
        CacheInterface $cache,
        CollectionFactory $attrGroupCollectionFactory,
        \Magento\Framework\File\Csv $csvReaderFactory
    ) {
        parent::__construct($setup, $context, $cache, $attrGroupCollectionFactory);
        $this->_resource = $resource;
        $this->_shippingAddressFactory = $shippingAddressFactory;
        $this->csvReader = $csvReaderFactory;

    }

    public function addShippingAddress($filePath)
    {
        $csvData = $this->readCsvData($filePath);
        foreach ($csvData as $key => $data) {
            //print_r($data);exit;
            $shippingAddressModel = $this->_shippingAddressFactory->create();
            $shippingAddressModel->addData($data);
            $shippingAddressModel->save();
        }
        return true;
    }
    public function replaceShippingAddress($filePath)
    {
        $this->deleteAllRecord();
        $csvData = $this->readCsvData($filePath);
        foreach ($csvData as $key => $data) {
            $shippingAddressModel = $this->_shippingAddressFactory->create();
            $shippingAddressModel->addData($data);
            $shippingAddressModel->save();
        }
        return true;
    }

    public function deleteAllRecord()
    {
        $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $tablename = $connection->getTableName('commercers_shippingaddresses');
        $query = "DELETE from ".$tablename;
        $connection->query($query);
    }


    function readCsvData($filePath){
        $file = fopen($filePath,"r");
        while(!feof($file)){
            $csv[] = fgetcsv($file, 0, ',');
        }
        $keys = array_shift($csv);
        foreach ($csv as $data){
            if(is_array($data)){
                $returnValue[] = array_combine($keys,$data);
            }
        }
        if(isset($returnValue))
            return $returnValue;
        return false;
    }
}