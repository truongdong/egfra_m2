<?php

namespace Commercers\OnepageCheckout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {
    const EGFRA_DIVISION = 'egfra_division';
}