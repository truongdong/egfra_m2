<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Model;
/**
 * Class AutoLogin
 * @package Commercers\AutoLogin\Model
 */
class AutoLogin extends \Magento\Framework\Model\AbstractModel
{

    protected function _construct()
    {
        $this->_init('Commercers\AutoLogin\Model\ResourceModel\AutoLogin');
    }
}
