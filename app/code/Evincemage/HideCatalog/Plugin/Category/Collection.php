<?php
namespace Evincemage\HideCatalog\Plugin\Category;

class Collection
{
    
    public function __construct(\Evincemage\HideCatalog\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
    public function aroundAddAttributeToSort(\Magento\Catalog\Model\ResourceModel\Category\Collection $collection, \Closure $proceed)
    {
        $CustomerGroupID = $this->helper->getCustomerGroupId();
        if ($this->helper->isModuleActive()) {
            if ($this->helper->CategoryLoadVisible()) {
                $CustomerGroupID = $this->helper->ConfigCategorysetting();
                $collection->addAttributeToFilter('entity_id', array(
                    'eq' => 0
                ));
            } else {
                $collection->addAttributeToFilter(array(
                    array(
                        'attribute' => 'hide_category_groups',
                        'null' => true
                    ),
                    array(
                        'attribute' => 'hide_category_groups',
                        'nin' => array($CustomerGroupID)
                    )
                ), '', 'left');
            }
            
        }
        return $collection;
    }
} 