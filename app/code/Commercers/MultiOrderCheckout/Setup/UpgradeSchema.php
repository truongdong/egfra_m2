<?php

namespace Commercers\MultiOrderCheckout\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();

		$setup->getConnection()
			->addColumn(
				$setup->getTable('quote_address'),
				'commercers_shippingaddress_id',
				[
					'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					'comment' =>'Commercers Shipping Address ID'
				]
			)
		;

		$setup->endSetup();
	}
}