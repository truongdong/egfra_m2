<?php

namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Commercers\OnepageCheckout\Helper\Data;

class Index extends AbstractIndex
{
	public function execute()
	{
        $customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
        if($customerSession->isLoggedIn()){
            $customer = $this->_customerFactory->create();
            $customer = $customer->load($this->_customerSession->getCustomerId());
            $egfraDivison = $customer->getData(Data::EGFRA_DIVISION);
            if($egfraDivison == ''){
                $this->messageManager->addErrorMessage(__('You cannot ordered as your account is not assigned to any devision - please contact your admin'));
                return $this->resultRedirectFactory->create()->setPath('checkout/cart');
            }
        }
		$this->_getCheckoutSession()->setCartWasUpdated(false);
		$resultPage = $this->_pageFactory->create();
		$resultPage->getConfig()->getTitle()->set(__('Multi Order Check-Out'));
		return $resultPage;
	}
}