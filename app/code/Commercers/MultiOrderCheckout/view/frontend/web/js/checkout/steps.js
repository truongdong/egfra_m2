define([
    'jquery',
    'ko'
], function($, ko) {
    'use strict';

    var steps = ko.observableArray();

    return {
        registerStep: function (code, isVisible, navigate, sortOrder) {
            steps.push({
                code: code,
                isVisible: isVisible,
                navigate: navigate,
                sortOrder: sortOrder
            });
            steps.each(function (elem, index) {
                if (index !== 0) {
                    elem.isVisible(false);
                }
            });
        },
        navigateTo: function (code) {
            steps().forEach(function (element) {
                if (element.code == code) {
                    element.isVisible(true);
                } else {
                    element.isVisible(false);
                }
            });
        }
    }
});
