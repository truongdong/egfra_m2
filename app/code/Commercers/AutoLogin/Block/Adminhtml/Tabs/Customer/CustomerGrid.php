<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */

namespace Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer;
/**
 * Class CustomerGrid
 * @package Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer
 */
class CustomerGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_objectManager;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Commercers\AutoLogin\Model\AutoLoginFactory
     */
    protected $_autoLoginFactory;
    protected $_mainAccountId = false;

    /**
     * CustomerGrid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Commercers\AutoLogin\Model\AutoLoginFactory $autoLoginFactory,
        array $data = []
    )
    {
        parent::__construct($context, $backendHelper, $data);
        $this->_coreRegistry = $registry;
        $this->_objectManager = $objectManager;
        $this->_customerFactory = $customerFactory;
        $this->_autoLoginFactory = $autoLoginFactory;
        if(!empty($this->__getAssociatedAccounts($this->getMainAccountId(),true))){
            $this->setDefaultFilter(array('in_customers' => 1));
        }
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('autologin_customer_edit_tab_associated_customer_grid');
        $this->setUseAjax(true);

    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this|CustomerGrid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_customers') {
            $customerIds = $this->getSelectedCustomers();
            //echo "<pre>";print_r($customerIds);exit;
            if (empty($customerIds)) {
                $customerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $customerIds]);
            } else {
                if ($customerIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $customerIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return CustomerGrid
     */
    protected function _prepareCollection()
    {
        $currentCustomer = $this->_customerFactory->create()->load($this->getMainAccountId());
        $collection = $this->_customerFactory->create()->getCollection();
        $collection->addFieldToFilter('website_id', $currentCustomer->getWebsiteId());
        $collection->addFieldToFilter('entity_id', array('nin' => implode(',',$this->__getAssociatedAccounts($this->getMainAccountId(),false))));
        $collection->addFieldToFilter('entity_id', array('nin' => $this->getMainAccountId()));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return CustomerGrid
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_customers',
            [
//                'header' => __('Select'),
                'type' => 'checkbox',
                'name' => 'in_products',
                'values' => $this->getSelectedCustomers(),
                'index' => 'entity_id',
                'sortable' => false,
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );
        $this->addColumn(
            'entity_id',
            [
                'header' => __('Customer Id'),
                'sortable' => true,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'type' => 'range'
            ]
        );


        $this->addColumn(
            'firstname',
            [
                'header' => __('Full Name'),
                'index' => 'firstname',
                'renderer' => '\Commercers\AutoLogin\Block\Adminhtml\Tabs\Customer\CustomerGrid\Renderer\FullNameColumn',
                'filter_condition_callback' => [$this, '_filterFullnameCondition']
            ]
        );

        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
            ]
        );

        $this->addColumn(
            'store_id',
            [
                'header' => __('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'skipEmptyStoresLabel' => 1,
                'sortable' => true,
            ]
        );
        $this->addColumn(
            'action',
            [
                'header' => __('Actions'),
                'index' => 'action',
                'filter' => false,
                'sortable' => false,
                'frame_callback' => [$this, 'showActions'],
            ]
        );


        return parent::_prepareColumns();
    }

    /**
     * @return array
     */
    protected function getSelectedCustomers()
    {
        $customers = $this->getRequest()->getPost('customers_grid_value', []);
        $subAccounts = $this->__getAssociatedAccounts($this->getMainAccountId(),true);
        if (!empty($subAccounts)) {
            return $subAccounts;
        }
        return $customers;
    }

    /**
     * @param $value
     * @param $row
     * @param $column
     * @return string
     */
    public function showActions($value, $row, $column)
    {
        $edit = '<a href="' . $this->_backendHelper
                ->getUrl('customer/index/edit', ['id' => $row->getId()]) . '">' . __('Show') . '</a>';
        return $edit;
    }

    /**
     * @param array $params
     * @return string
     */
    public function getAbsoluteGridUrl($params = [])
    {
        return $this->getUrl('commercers_autologin/customer/loadcustomergrid', ['id' => $this->getRequest()->getParam('id')]);
    }

    /**
     * @return mixed|null
     */
    public function getMainAccountId()
    {
        if ($this->_mainAccountId) {
            return $this->_mainAccountId;
        }
        $customerId = $this->getRequest()->getParam('id');
        if ($customerId) {
            $this->_mainAccountId = $customerId;
        }
        return $this->_mainAccountId;
    }

    /**
     * @param $accountId
     * @param false $isGetSubAccount
     * @return array
     */
    private function __getAssociatedAccounts($accountId, $isGetSubAccount = false){
        $collection = $this->_autoLoginFactory->create()->getCollection();
        $fieldToFilter = 'sub_account_id';
        $resultField = 'main_account_id';
        if($isGetSubAccount){
            $fieldToFilter = 'main_account_id';
            $resultField = 'sub_account_id';
        }
        $collection->addFieldToFilter($fieldToFilter, $accountId);
        $result = array();
        if(!$collection->getSize()) return $result;
        foreach ($collection as $item) {
            $result[] = $item[$resultField];
        }
        return $result;
    }
    protected function _filterFullnameCondition($collection, $column)
    {
        $fullname = $column->getFilter()->getValue();
        $collection->getSelect()->where('CONCAT(firstname, \' \' ,lastname) like ?', '%' . $fullname . '%');
    }
}