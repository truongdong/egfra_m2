<?php

namespace Commercers\MultiOrderCheckout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class AddMultiOrderShortcutsObserver implements ObserverInterface {

	public function execute(EventObserver $observer)
	{
		$shortcutButtons = $observer->getEvent()->getContainer();
		$shortcut = $shortcutButtons->getLayout()->createBlock('Commercers\MultiOrderCheckout\Block\Checkout\Link');
		$shortcut->setIsInCatalogProduct(false)
		         ->setShowOrPosition('after')
		         ->setIsShoppingCart(true);
		$shortcutButtons->addShortcut($shortcut);
	}
}
