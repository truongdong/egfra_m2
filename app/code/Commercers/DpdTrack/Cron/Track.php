<?php
/**
 * Created by PhpStorm.
 * User: niken
 * Date: 9/25/18
 * Time: 22:00
 */
namespace Commercers\DpdTrack\Cron;

use Magento\Sales\Model\Order;

class Track {

	/**
	 * @var \Commercers\DpdTrack\Helper\Data
	 */
	protected $_helper;

	/**
	 * @var \Commercers\DpdTrack\Model\LogFactory
	 */
	protected $_logFactory;

	/** @var \Magento\Framework\Model\ResourceModel\Iterator $iterator */

	protected $_iterator;

	/**
	 * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
	 */
	protected $_orderFactory;

	protected $_resource;

	protected $_dpdParcelTrack;

	/**
	 * @var \Commercers\DpdTrack\Logger\Logger
	 */
	private $_logger;

	public function __construct(
		\Commercers\DpdTrack\Helper\Data $helper,
		\Commercers\DpdTrack\Model\DpdParcelTrack $dpdParcelTrack,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Framework\Model\ResourceModel\Iterator $iterator,
		\Magento\Framework\App\ResourceConnection $resource,
		\Commercers\DpdTrack\Model\LogFactory $logFactory,
		\Commercers\DpdTrack\Logger\Logger $logger
	) {
		$this->_helper = $helper;
		$this->_orderFactory = $orderFactory;
		$this->_dpdParcelTrack = $dpdParcelTrack;
		$this->_iterator = $iterator;
		$this->_resource = $resource;
		$this->_logFactory = $logFactory;
		$this->_logger = $logger;
	}

	public function execute()
	{
		if(!$this->_helper->isEnabled()){
			return;
		}
		$daysBack = $this->_helper->getConfigValue('dpdtrack/general/days_back');
		$orderPerRun = $this->_helper->getConfigValue('dpdtrack/general/limit');

		$log = $this->_logFactory->create()->getCollection();
		$log->getSelect()->order(array('updated_at DESC', 'order_id DESC'));

		$lastOrderRun = $log->getFirstItem()->getOrderId();

		if($daysBack <= 1){
			$daysBackTime = strtotime('-'.$daysBack." day");
		} else {
			$daysBackTime = strtotime('-'.$daysBack." days");
		}
		$date = date('Y-m-d h:i:s', $daysBackTime);

		$filterStatuses = explode(',',$this->_helper->getConfigValue('dpdtrack/general/order_statuses'));

		$collection = $this->_orderFactory->create()->getCollection();
		$collection
			->addFieldToFilter('entity_id',['gt'=>$lastOrderRun])
			->addFieldToFilter('status',['in'=>$filterStatuses])
		;

		$collection
			->getSelect()
			->joinLeft(
				['sales_shipment_track'=>$this->_resource->getTableName('sales_shipment_track')],
				'main_table.entity_id = sales_shipment_track.order_id',
				[
					'order_id'=>'sales_shipment_track.order_id',
					'track_number' => 'sales_shipment_track.track_number',
					'created_at' => 'sales_shipment_track.created_at'
				]
			)
			->where('sales_shipment_track.created_at >= ?',$date)
			->where('order_id IS NOT NULL')
			->where('track_number IS NOT NULL')
			->order('entity_id ASC')
			->group('entity_id')
			->limit($orderPerRun)
		;
		if(!$collection->count()){
			$collection->getSelect()->reset(\Zend_Db_Select::WHERE);
			$collection
				->addFieldToFilter('status',['in'=>$filterStatuses])
			;
			$collection
				->getSelect()
				->where('sales_shipment_track.created_at >= ?',$date)
				->where('order_id IS NOT NULL')
				->where('track_number IS NOT NULL')
				->limit($orderPerRun)
			;
		}

		$this->_iterator
			->walk( $collection->getSelect(), [ [$this, 'changeOrderStatus'] ] );
		return $this;
	}

	public function changeOrderStatus($args)
	{
		$trackingNumber = preg_replace('/\s+/', '', $args['row']['track_number']);
		if(strlen($trackingNumber)>14){
			$trackingNumber = substr($trackingNumber, 0, 14);
		}

		$trackingData = $this->_dpdParcelTrack->getStatus($trackingNumber);
		$orderStatus = '';
		if(!empty($trackingData)){
			$orderStatus = $this->_helper->getOrderConfigStatus($trackingData['statusCode']);

			if($orderStatus){
				$order = $this->_orderFactory->create()->load($args['row']['entity_id']);

				$currentStatus = $order->getStatus();

				if($currentStatus != $orderStatus){
					$order->setStatus($orderStatus);
					$order->addStatusToHistory(false,$trackingData['statusDescription']);
					$order->save();
				}
			}
		}
		$log = $this->_logFactory->create()->load($args['row']['entity_id'],'order_id');

		if(!empty($log->getData())){

			if($orderStatus=='DPD_DELIVERED'){
				$log->delete();
				return $this;
			} else {
				$logData = [
					'updated_at' => date('Y-m-d h:i:s')
				];
				$log->addData($logData);
			}
		} else {
			$logData = [
				'order_id' => $args['row']['entity_id'],
				'increment_id' => $args['row']['increment_id'],
				'tracking_number' => $args['row']['track_number'],
				'status' => $orderStatus,
				'updated_at' => date('Y-m-d h:i:s')
			];
			$log->setData($logData);

		}
		try {
			$log->save();
		} catch (\Exception $e){
			$this->_logger->err($e->getMessage());
		}
	}

}