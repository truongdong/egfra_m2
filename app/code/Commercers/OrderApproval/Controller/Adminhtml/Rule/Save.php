<?php
namespace Commercers\OrderApproval\Controller\Adminhtml\Rule;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action;
use Commercers\OrderApproval\Model\Rule;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save CMS page action.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
// class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Commercers_OrderApproval::save';

    /**
     * @var PostDataProcessor
     */
    // protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * \Commercers\OrderApproval\Model\RuleFactory
     * @var \Commercers\OrderApproval\Model\RuleFactory
     */
    private $ruleFactory;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     * @param DataPersistorInterface $dataPersistor
     * @param \Commercers\OrderApproval\Model\RuleFactory|null $ruleFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface|null $pageRepository
     */
    public function __construct(
        Action\Context $context,
        // PostDataProcessor $dataProcessor,
        DataPersistorInterface $dataPersistor,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory = null,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository = null
    ) {
        // $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->ruleFactory = $ruleFactory ?: ObjectManager::getInstance()->get(\Commercers\OrderApproval\Model\RuleFactory::class);
        $this->pageRepository = $pageRepository
            ?: ObjectManager::getInstance()->get(\Magento\Cms\Api\PageRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        echo 123;exit;
        $data = $this->getRequest()->getPostValue();
        var_dump($data);exit;
        echo "<pre>";
        print_r($data);exit;
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            // $data = $this->dataProcessor->filter($data);
            if (isset($data['active']) && $data['active'] === 'true') {
                $data['active'] = Rule::STATUS_ENABLED;
            }
            if (empty($data['order_approval_rule_id'])) {
                $data['order_approval_rule_id'] = null;
            }

            /** @var \Commercers\OrderApproval\Model\Rule $model */
            $model = $this->ruleFactory->create();

            $id = $this->getRequest()->getParam('order_approval_rule_id');
            // var_dump($id);exit;
            if ($id) {
                try {
                    $model = $model->load($id,'order_approval_rule_id');
                    // $barcodeModel->load($product_sku,'product_sku');
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This rule no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $data['layout_update_xml'] = $model->getLayoutUpdateXml();
            $data['custom_layout_update_xml'] = $model->getCustomLayoutUpdateXml();
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the rule.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the rule.'));
            }

            $this->dataPersistor->set('commercers_orderapproval', $data);
            return $resultRedirect->setPath('*/*/edit', ['order_approval_rule_id' => $this->getRequest()->getParam('order_approval_rule_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process result redirect
     *
     * @param \Magento\Cms\Api\Data\PageInterface $model
     * @param \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     * @param array $data
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    private function processResultRedirect($model, $resultRedirect, $data)
    {
        if ($this->getRequest()->getParam('back', false) === 'duplicate') {
            $newPage = $this->ruleFactory->create(['data' => $data]);
            $newPage->setId(null);
            $identifier = $model->getIdentifier() . '-' . uniqid();
            $newPage->setIdentifier($identifier);
            $newPage->setIsActive(false);
            $this->pageRepository->save($newPage);
            $this->messageManager->addSuccessMessage(__('You duplicated the page.'));
            return $resultRedirect->setPath(
                '*/*/edit',
                [
                    'order_approval_rule_id' => $newPage->getId(),
                    '_current' => true
                ]
            );
        }
        $this->dataPersistor->clear('commercers_orderapproval');
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['order_approval_rule_id' => $model->getId(), '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
