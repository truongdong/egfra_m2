<?php
namespace Commercers\OrderApproval\Controller\Adminhtml\Rule;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action;
use Commercers\OrderApproval\Model\Rule;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
/**
 * Save CMS page action.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SaveRule extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Commercers_OrderApproval::save';
    /**
     * @var PostDataProcessor
     */
    // protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * \Commercers\OrderApproval\Model\RuleFactory
     * @var \Commercers\OrderApproval\Model\RuleFactory
     */
    private $ruleFactory;
    /**
     * \Commercers\OrderApproval\Model\SupervisorFactory
     * @var \Commercers\OrderApproval\Model\SupervisorFactory
     */
    private $supervisorFactory;
    /**
     * \Commercers\OrderApproval\Model\Supervisor\CustomerFactory
     * @var \Commercers\OrderApproval\Model\Supervisor\CustomerFactory
     */
    private $supervisorCustomerFactory;
    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    private $pageRepository;
    protected $messageManager;
    protected $supervisorCollectionFactory;
    /**
     * \Magento\Backend\Helper\Js;
    */
    protected $_backendJsHelper;
    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     * @param DataPersistorInterface $dataPersistor
     * @param \Commercers\OrderApproval\Model\RuleFactory|null $ruleFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface|null $pageRepository
     */
    protected $resource;
    public function __construct(
        \Magento\Backend\Helper\Js $backendJsHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        Action\Context $context,
        // PostDataProcessor $dataProcessor,
        DataPersistorInterface $dataPersistor,
        \Commercers\OrderApproval\Model\RuleFactory $ruleFactory = null,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Commercers\OrderApproval\Model\Supervisor\CustomerFactory $supervisorCustomerFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Commercers\OrderApproval\Model\ResourceModel\Supervisor\CollectionFactory $supervisorCollectionFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository = null
    ) {
        $this->_backendJsHelper = $backendJsHelper;
        $this->supervisorCollectionFactory = $supervisorCollectionFactory;
        $this->messageManager = $messageManager;
        // $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->resource = $resource;
        $this->supervisorFactory = $supervisorFactory;
        $this->supervisorCustomerFactory = $supervisorCustomerFactory;
        $this->ruleFactory = $ruleFactory ?: ObjectManager::getInstance()->get(\Commercers\OrderApproval\Model\RuleFactory::class);
        $this->pageRepository = $pageRepository
            ?: ObjectManager::getInstance()->get(\Magento\Cms\Api\PageRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute(){
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['active']) && $data['active'] === 'true') {
                $data['active'] = Rule::STATUS_ENABLED;
            }
            /** @var \Commercers\OrderApproval\Model\Rule $model */
            $modelRule = $this->ruleFactory->create();
            $id = $this->getRequest()->getParam('order_approval_rule_id');
            if ($id) {
                try {
                    $modelRule = $modelRule->load($id,'order_approval_rule_id');
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This rule no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $data['layout_update_xml'] = $modelRule->getLayoutUpdateXml();
            $data['custom_layout_update_xml'] = $modelRule->getCustomLayoutUpdateXml();
            $modelRule->addData(array(
                'rule_name' => $data['rule_name'],
                'email' => $data['email_supervisor'],
                'approver_name' => $data['name_supervisor'],
                'active' => $data['active_general']
            ));
            try {
                $modelRule->save();
                $this->saveRuleItems($modelRule->getOrderApprovalRuleId(),$data);
                $this->messageManager->addSuccessMessage(__('You saved the rule.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the rule.'));
            }
            $this->dataPersistor->set('commercers_orderapproval', $data);
            return $resultRedirect->setPath('*/*/edit', ['order_approval_rule_id' => $modelRule->getOrderApprovalRuleId()]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    public function saveRuleItems($ruleId,$data)
    {
        $approvalItems = $this->getRequest()->getParam('approval_items');
        $approvalItems = json_decode($approvalItems,true);
        $idx = 0;
        if($approvalItems){
            //Remove Item supervisor
            $supervisorCollection = $this->supervisorCollectionFactory->create();
            $supervisorCollection->addFieldToFilter('rule_id', $ruleId);

            $ids = $supervisorCollection->getAllIds();
            $saveIds = [];
            foreach($approvalItems as $approvalItem){
                $supervisorModel = $this->supervisorFactory->create();
                if($approvalItem['supervisorId'] != '') {
                    $supervisorModel->load($approvalItem['supervisorId']);
                }
                $supervisorModel->addData(array(
                    'supervisor_name' => $approvalItem['supervisorName'],
                    'supervisor_email' => $approvalItem['supervisorEmail'],
                    'supervisor_status' => $approvalItem['supervisorStatus'],
                    'rule_id' => $ruleId
                ));
                $supervisorModel->save();
                $supervisorId =  $supervisorModel->getSupervisorId();
                array_push($saveIds, $supervisorId);
                // ADD item Table = commercers_order_approval_supervisor_customer
                $arrCustomerIds = $data['customers_grid_value_'.$idx.''];
                if($arrCustomerIds){
                    $arrCustomerIds = explode("&",$arrCustomerIds);
                    // Remove item Table = commercers_order_approval_supervisor_customer
                    $connection= $this->resource->getConnection();
                    $customerTable = $this->resource->getTableName('commercers_order_approval_supervisor_customer');
                    $sql = "DELETE  FROM $customerTable WHERE supervisor_id=$supervisorId";
                    $connection->query($sql);
                    foreach($arrCustomerIds as $customerId){
                        $supervisorCustomerModel = $this->supervisorCustomerFactory->create();
                        $supervisorCustomerModel->addData(array(
                            'supervisor_id' => $supervisorId,
                            'customer_id' => $customerId
                        ));
                        $supervisorCustomerModel->save();
                    }
                }
                $idx++;
            }
            foreach ($ids as $id) {
                if(!in_array($id,$saveIds)){
                    $supervisorModel = $this->supervisorFactory->create();
                    $supervisorModel->load($id);
                    $supervisorModel->delete();
                }
            }
        }
        return;
    }
    /**
     * Process result redirect
     *
     * @param \Magento\Cms\Api\Data\PageInterface $model
     * @param \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     * @param array $data
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    private function processResultRedirect($model, $resultRedirect, $data)
    {
        if ($this->getRequest()->getParam('back', false) === 'duplicate') {
            $newPage = $this->ruleFactory->create(['data' => $data]);
            $newPage->setId(null);
            $identifier = $model->getIdentifier() . '-' . uniqid();
            $newPage->setIdentifier($identifier);
            $newPage->setIsActive(false);
            $this->pageRepository->save($newPage);
            $this->messageManager->addSuccessMessage(__('You duplicated the page.'));
            return $resultRedirect->setPath(
                '*/*/edit',
                [
                    'order_approval_rule_id' => $newPage->getId(),
                    '_current' => true
                ]
            );
        }
        $this->dataPersistor->clear('commercers_orderapproval');
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['order_approval_rule_id' => $model->getId(), '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
