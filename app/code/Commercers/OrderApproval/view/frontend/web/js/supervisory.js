define([
    'jquery',
    'mage/translate'
], function (
    $,
    $t
) {
    'use strict';
    $.widget(
        'mage.supervisory',
        {
            _create: function () {
                var self = this;
                var options = self.options;
                self.actionInit(options);
            },
            actionInit: function (options) {
                var self = this;
                $('.btn-approve-submit').click(function() {
                    $('#loading-mask').show();
                    var approvalCode = $(this).val();
                    var actionSubmit = 'approve';
                    self.ajaxApprovalItemOrder(approvalCode,options,actionSubmit);
                });
                $('.btn-disapprove-submit').click(function() {
                    $('#loading-mask').show();
                    var approvalCode = $(this).val();
                    var actionSubmit = 'disapprove';
                    self.ajaxApprovalItemOrder(approvalCode,options,actionSubmit);
                });
            },
            ajaxApprovalItemOrder: function(approvalCode,options,actionSubmit){
                $.ajax({
                    url: options.urlApprovalItemOrder,
                    data: {
                        approvalCode: approvalCode,
                        supervisorName: options.supervisorName,
                        actionSubmit: actionSubmit
                    },
                    async: false,
                    type: "POST",
                    dataType: 'json',
                }).done(function(response) {
                    $("#message-approval").append("<div class='text-message'><p>"+response.message+"</p></div>");
                    $(".row-item."+response.approved_code).remove();
                    if($("#table-supervisory-order-item > tbody > tr").length <= 1){
                        $("#table-supervisory-order-item > tbody > tr").remove();
                        $("#message-approval-check-item-exists").append("<div class='div-notice'><p class='text-notice'>"+$t('You haven\'t any item need approval')+"</p></div>");
                    };
                    $('#loading-mask').hide();
                    setTimeout(function() {
                        $("#message-approval").html('');
                    }, 3000);
                })
            },
        }
    );
    return $.mage.supervisory;
});
