<?php

namespace Commercers\OrderApproval\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('commercers_order_approval')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_order_approval')
            )
                ->addColumn(
                    'order_approval_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'auto_increment' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'length' => 11
                    ],
                    'Identity'
                )
                ->addColumn(
                    'order_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 11
                    ],
                    'Order Id'
                )
                ->addColumn(
                    'order_item_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 11
                    ],
                    'Order Item Id'
                )
                ->addColumn(
                    'approval_code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255,
                    ],
                    'Approval Code'
                )
                ->addColumn(
                    'approval_email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Approval Email'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    0,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 1
                    ],
                    'Approval Status 0: not approved 1: approval 2 : cancle'
                )
                ->addColumn(
                    'email_sent',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    0,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 1
                    ],
                    'Is send first email'
                )
                ->addColumn(
                    'approval_rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'length' => 11
                    ],
                    'Approval Rule Id'
                )
                ->addColumn(
                    'approval_rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'length' => 11
                    ],
                    'Approval Rule Id'
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => true,
                        'length' => 11
                    ],
                    'Customer id'
                )
                ->addColumn(
                    'supervisor_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => true,
                        'length' => 11
                    ],
                    'Supervisor Id'
                )
                ->addColumn(
                    'shipping_method',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'length' => 255
                    ],
                    'Shipping Method'
                )
                ->addColumn(
                    'approval_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    255,
                    [
                        'unsigned' => true,
                        'nullable' => true,
                        'length' => 11
                    ],
                    'Approval Type'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Created at'
                )
                ->addColumn(
                    'last_send_reminder_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Last send reminder at'
                );
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('commercers_order_approval_rule')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_order_approval_rule')
            )
                ->addColumn(
                    'order_approval_rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'auto_increment' => true,
                    ],
                    'Identity'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [
                        'length' => 255,
                        'nullable' => false,
                    ],
                    'Name'
                )
                ->addColumn(
                    'email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [
                        'length' => 255,
                        'nullable' => false,
                    ],
                    'Email'
                )
                ->addColumn(
                    'approver_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'length' => 255,
                        'nullable' => false,
                    ],
                    'Approval name'
                )
                ->addColumn(
                    'active',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    1,
                    [
                        'length' => 1,
                        'nullable' => false,
                    ],
                    'Active'
                );
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('commercers_order_approval_supervisor')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_order_approval_supervisor')
            )
                ->addColumn(
                    'supervisor_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'auto_increment' => true,
                    ],
                    'Identity'
                )
                ->addColumn(
                    'rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    1,
                    [
                        'unsigned' => true,
                        'length' => 11,
                        'nullable' => false,
                    ],
                    'Name'
                )
                ->addColumn(
                    'email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [
                        'length' => 255,
                        'nullable' => false,
                    ],
                    'Email'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'length' => 255,
                        'nullable' => false,
                    ],
                    'Name'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    0,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 1
                    ],
                    'Status'
                );
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('commercers_order_approval_supervisor_customer')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('commercers_order_approval_supervisor_customer')
            )
                ->addColumn(
                    'supervisor_customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'auto_increment' => true,
                    ],
                    'Identity'
                )
                ->addColumn(
                    'supervisor_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    1,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 11
                    ],
                    'Name'
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'length' => 11
                    ],
                    'Email'
                );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
