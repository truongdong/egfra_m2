<?php

namespace Commercers\MultiOrderCheckout\Block\Checkout;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Directory\Model\Config\Source\Country;
use Magento\Directory\Model\AllowedCountries;
use Commercers\MultiOrderCheckout\Model\ShippingMethods;

class Steps extends Template
{
	/**
	 * @var CustomerSession
	 */
	protected $_customerSession;

	/**
	 * @var CheckoutSession
	 */
	protected $_checkoutSession;

	/**
	 * @var Country
	 */
	protected $_country;

	/**
	 * @var AllowedCountries
	 */
	protected $_allowedCountries;

	/**
	 * @var ShippingMethods
	 */
	protected $_shippingMethods;

	public function __construct(
		Context $context,
		CustomerSession $customerSession,
		CheckoutSession $checkoutSession,
		Country $country,
		AllowedCountries $allowedCountries,
		ShippingMethods $shippingMethods,
		array $data = []
	) {
		$this->_customerSession = $customerSession;
		$this->_checkoutSession = $checkoutSession;
		$this->_country = $country;
		$this->_allowedCountries = $allowedCountries;
		$this->_shippingMethods = $shippingMethods;

		parent::__construct($context, $data);
	}

	protected function getQuote() {
		return $this->_checkoutSession->getQuote();
	}

	protected function getStoreId(){
		return $this->_storeManager->getStore()->getId();
	}

	protected function getWebsiteId(){
		return $this->_storeManager->getWebsite()->getId();
	}
}
