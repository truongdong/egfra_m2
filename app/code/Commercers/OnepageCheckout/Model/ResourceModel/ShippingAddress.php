<?php
namespace Commercers\OnepageCheckout\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ShippingAddress extends AbstractDb {

    public function __construct(
        Context	$context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('commercers_shippingaddresses', 'shippingaddresses_id');
    }
}
