<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Commercers\OrderApproval\Preference\Block\Account;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Html\Links;
use Magento\Customer\Block\Account\SortLinkInterface;

/**
 * Class for sorting links in navigation panels.
 *
 * @api
 * @since 101.0.0
 */
class Navigation extends Links
{
    const XML_STATUS_ACTIVE = 1;
    const XML_STATUS_DISACTIVE = 0;
    const XML_SHOW_HIDE_SUPERVISORY_OVERVIEW = 'orderapproval/general/yesno_allow_supervisor_frontend';
    protected $_customerSession;

    protected $_dataCollectionFactory;

    protected $_order;

    protected $_supervisorCollectionFactory;

    public function __construct(
        \Commercers\OrderApproval\Model\ResourceModel\Supervisor\CollectionFactory $supervisorCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        Template\Context $context
    ) {
        $this->_supervisorCollectionFactory = $supervisorCollectionFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }
    /**
     * @inheritdoc
     * @since 101.0.0
     */
    public function getLinks()
    {
        $links = $this->_layout->getChildBlocks($this->getNameInLayout());

        if (!$this->_scopeConfig->getValue(self::XML_SHOW_HIDE_SUPERVISORY_OVERVIEW)){
            unset($links['orderapproval-supervisory']);
        }
        if(!$this->checkSupervisorCustomer()){
            unset($links['orderapproval-supervisory']);
        }
        $sortableLink = [];
        foreach ($links as $key => $link) {
            if ($link instanceof SortLinkInterface) {
                $sortableLink[] = $link;
                unset($links[$key]);
            }
        }

        usort($sortableLink, [$this, "compare"]);
        return array_merge($sortableLink, $links);
    }

    /**
     * Compare sortOrder in links.
     *
     * @param SortLinkInterface $firstLink
     * @param SortLinkInterface $secondLink
     * @return int
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function compare(SortLinkInterface $firstLink, SortLinkInterface $secondLink): int
    {
        return $secondLink->getSortOrder() <=> $firstLink->getSortOrder();
    }
//    public function checkSupervisorCustomer(){
//        $currEmailCustomer = $this->getEmailCustomer();
//        $supervisor = $this->_supervisorCollectionFactory->create();
//        $supervisor->addFieldToFilter('supervisor_email', array('eq' => $currEmailCustomer));
//        return $supervisor->getData();
////            ->getSelect()->group('order_id')->order('last_send_reminder_at DESC');
//    }
    public function checkSupervisorCustomer(){
        $currEmailCustomer = $this->getEmailCustomer();
        $supervisorsCollection = $this->_supervisorCollectionFactory->create();

        $supervisorsCollection->getSelect()->join(
            array('rule' => 'commercers_order_approval_rule'),
            'main_table.rule_id = rule.order_approval_rule_id',
            array()
        );
        $supervisorsCollection->addFieldToFilter('supervisor_email', array('eq' => $currEmailCustomer));
        $supervisorsCollection->addFieldToFilter('supervisor_status', array('eq' => self::XML_STATUS_ACTIVE));
        $supervisorsCollection->getSelect()->where('rule.active = ?', self::XML_STATUS_ACTIVE);
        return $supervisorsCollection->getData();
    }


//    protected function filterOrderApprovalSupervisor($customerId,$orderApprovalRuleId)
//    {
//        $supervisorsCollection = $this->_supervisorCollectionFactory->create();
//        $supervisorsCollection->getSelect()->join(
//            array('customers' => 'commercers_order_approval_supervisor_customer'),
//            'main_table.supervisor_id = customers.supervisor_id',
//            array()
//        );
//        $supervisorsCollection->addFieldToFilter('customer_id', $customerId);
//        $supervisorsCollection->getSelect()->where('main_table.rule_id = ?', $orderApprovalRuleId);
//        return $supervisorsCollection->getData();
//    }
//
    public function getEmailCustomer()
    {
        return $this->_customerSession->getCustomer()->getEmail();
    }
}
