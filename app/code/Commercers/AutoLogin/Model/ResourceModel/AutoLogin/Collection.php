<?php
/**
 * Commercers Viet Nam
 * Commercers_AutoLogin
 * ChucVB
 */
namespace Commercers\AutoLogin\Model\ResourceModel\AutoLogin;
/**
 * Class Collection
 * @package Commercers\AutoLogin\Model\ResourceModel\AutoLogin
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init("Commercers\AutoLogin\Model\AutoLogin", "Commercers\AutoLogin\Model\ResourceModel\AutoLogin");
    }
}