<?php

namespace Evincemage\HideCatalog\Model\Category\Attribute\Backend;

class Options extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    public function __construct(\Evincemage\HideCatalog\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
    public function beforeSave($object)
    {
        
        $attributeCode    = $this->getAttribute()->getAttributeCode();
        $data             = $object->getData($attributeCode);
        $customerGroupIds = array(
            \Evincemage\HideCatalog\Helper\Data::USE_NONE
        );
        $customerGroups   = $this->helper->getGroups();
        $customerGroupIds = array_merge($customerGroupIds, array_keys($customerGroups->getItems()));
        
        if (!$data) {
            $data = array(
                \Evincemage\HideCatalog\Helper\Data::USE_NONE
            );
        }
        
        if (!is_array($data)) {
            $data = explode(',', $data);
        }
        
        if (1 < count($data)) {
            
            
            // if USE_NONE is selected remove all other groups
            if (in_array(\Evincemage\HideCatalog\Helper\Data::USE_NONE, $data)) {
                $data = array(
                    \Evincemage\HideCatalog\Helper\Data::USE_NONE
                );
            }
        }
        
        // validate all customer groups ids are valid
        foreach ($data as $key => $groupId) {
            if (!in_array($groupId, $customerGroupIds)) {
                unset($data[$key]);
            }
        }
        sort($data);
        
        $object->setData($attributeCode, implode(',', $data));
        return parent::beforeSave($object);
    }
    
    public function afterSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data          = $object->getData($attributeCode);
        if (is_string($data)) {
            $object->setData($attributeCode, explode(',', $data));
        }
        return parent::afterSave($object);
    }
    public function afterLoad($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data          = $object->getData($attributeCode);
        
        // only explode and set the value if the attribute is set on the model
        if (null !== $data && is_string($data)) {
            $data = explode(',', $data);
            $object->setData($attributeCode, $data);
        }
        return parent::afterLoad($object);
    }
    
} 