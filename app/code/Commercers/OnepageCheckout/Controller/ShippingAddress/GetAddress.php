<?php

namespace Commercers\OnepageCheckout\Controller\ShippingAddress;

use Commercers\SalesForceIntegrationSuite\Model\ResourceModel\Contact;
use Magento\Framework\App\Action\Action;

class GetAddress extends Action{

    protected $_pageFactory;

    protected $_shippingAddressFactory;

    protected $_shippingAddressCollectionFactory;

    protected $_contactCollectionFactory;

    protected $_checkoutSession;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Commercers\SalesForceIntegrationSuite\Model\ResourceModel\Contact\CollectionFactory $contactCollectionFactory,
        \Commercers\OnepageCheckout\Model\ShippingAddressFactory $shippingAddressFactory,
        \Commercers\OnepageCheckout\Model\ResourceModel\ShippingAddress\CollectionFactory $shippingAddressCollectionFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_contactCollectionFactory = $contactCollectionFactory;
        $this->_shippingAddressFactory = $shippingAddressFactory;
        $this->_shippingAddressCollectionFactory = $shippingAddressCollectionFactory;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $addressId = $this->getRequest()->getParam('addressId');
        $addressModel = $this->_shippingAddressFactory->create()->load($addressId);
        $address = false;
        $data['message'] = false;
        if ($addressModel && $addressModel->getId()){
            $address = $addressModel->getData();
        }
        $dataShipping = [];
        $dataContactAccount = [];
        if ($address) {
            $accountId = $address['account_id'];
            $dataContactAccount = $this->getDataContactAccount($accountId);
            $data['message'] = true;
            $dataShipping = array (
                'account' => $address['account_id'],
                'firstname' => '',
                'lastname' => '',
                'company' => $address ['account_name'],
                'city' => $address ['shipping_city'],
                'country_id' => $address ['shipping_country'],
                'region_id' => $address ['shipping_region'],
                'region' => $address ['shipping_region'],
                'postcode' => $address ['shipping_postal_code'],
                'telephone' => $address ['shipping_telephone'],
                'shipping_company_name' => $address ['shipping_company_name'],
                'fax' => $address ['shipping_fax'],
                'street' => $address ['shipping_street'],
                'street1' => $address ['shipping_street'],
                'email' => $address ['shipping_email']
            );
        }
        $data['dataContactAccount'] = $dataContactAccount;
        $data['dataShipping'] = $dataShipping;
        try {
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData($data);
            return $response;
        } catch (Exception $e) {
        }
    }

    public function getDataContactAccount($accountId)
    {
        $contactCollection = $this->_contactCollectionFactory->create();
        $contactCollection->addFieldToSelect('*');
        $contactCollection->getSelect()->where("account_id = '". $accountId ."'");
        $quote = $this->_checkoutSession->getQuote();
        if($contactCollection->getSize()){
            $selectedFlag = false;
            foreach($contactCollection as $contact){
                //$firstContact = $collection->getFirstItem();
                if($contact->getIsPrimary()){
                    $quote->getShippingAddress()->setFirstname($contact->getFirstname());
                    $quote->getShippingAddress()->setLastname($contact->getLastname());
                    $selectedFlag = true;
                    break;
                }
            }
            if($selectedFlag == false){
                foreach($contactCollection as $contact){
                    $quote->getShippingAddress()->setFirstname($contact->getFirstname());
                    $quote->getShippingAddress()->setLastname($contact->getLastname());
                    break;
                }
            }
        }
        return $contactCollection->getData();
    }
}
