<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\StockLog\Ui\Component\StockLog\Form;

use Commercers\StockLog\Model\ResourceModel\StockLog\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;
    protected $addFieldStrategies;
    protected $addFilterStrategies;
    private $modifiersPool;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Commercers\StockLog\Ui\Component\Listing\Collection $collection,
        CollectionFactory $collectionFactory,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = [],
        PoolInterface $modifiersPool = null
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collection;
        //echo print_r($collection);exit;
        /*
        $this->collection = $collectionFactory->create();
        $this->collection->getConnection();
        $this->collection->join(
            ['catalog_product_entity' => 'catalog_product_entity'],
            'main_table.product_id = catalog_product_entity.entity_id'
        );
         * 
         */
        $this->addFieldStrategies = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;
        $this->modifiersPool = $modifiersPool ?: ObjectManager::getInstance()->get(PoolInterface::class);
    }
    
    /*
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }

        $items = $this->getCollection()->toArray();

        //print_r($this->collection->getData());exit;

        $data = [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items),
        ];

        
        foreach ($this->modifiersPool->getModifiersInstances() as $modifier) {
            $data = $modifier->modifyData($data);
        }

        return $data;
    }*/

    public function addField($field, $alias = null)
    {
        if (isset($this->addFieldStrategies[$field])) {
            $this->addFieldStrategies[$field]->addField($this->getCollection(), $field, $alias);
        } else {
            parent::addField($field, $alias);
        }
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        if (isset($this->addFilterStrategies[$filter->getField()])) {
            $this->addFilterStrategies[$filter->getField()]
                ->addFilter(
                    $this->getCollection(),
                    $filter->getField(),
                    [$filter->getConditionType() => $filter->getValue()]
                );
        } else {
            parent::addFilter($filter);
        }
    }

    public function getMeta()
    {
        $meta = parent::getMeta();

        /** @var ModifierInterface $modifier */
        foreach ($this->modifiersPool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        return $meta;
    }
}

// use Magento\Framework\Api\Filter;
// use Magento\Framework\Api\FilterBuilder;
// use Magento\Framework\Api\Search\SearchCriteriaBuilder;
// use Magento\Framework\App\ObjectManager;
// use Magento\Framework\App\RequestInterface;
// use Magento\Framework\AuthorizationInterface;
// use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;

// class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
// {
//     //khu vuc cua products
//     public function load($printQuery = false, $logQuery = false)
//     {
//         if (!$this->isLoaded()) {
//             $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//             $stockLog = $objectManager->create("\Commercers\StockLog\Model\ResourceModel\StockLog\CollectionFactory");
//             $stockLog->addAttributeToSelect("*");

//             $this->_setIsLoaded(true);
//         }
//         return $this;
//     }
// }