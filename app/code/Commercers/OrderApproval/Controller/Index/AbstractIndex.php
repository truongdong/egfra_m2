<?php
namespace Commercers\OrderApproval\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Controller\ResultFactory;
abstract class AbstractIndex extends Action {

    protected $_registry;

    protected $_dataCollectionFactory;

    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $_order;

    protected $_dataFactory;

    /**
     * @var EventManager
     */
    protected $eventManager;

    /*
     * @param \Magento\Framework\Event\ManagerInterface as EventManager
     */

    protected $_resultFactory;

    protected $_coreSession;

    protected $_orderFactory;

    protected $_orderItemFactory;

    protected $_supervisorFactory;

    protected $_helperEmailFactory;

    public function __construct(
        \Commercers\OrderApproval\Helper\EmailFactory $helperEmailFactory,
        \Commercers\OrderApproval\Model\SupervisorFactory $supervisorFactory,
        \Magento\Sales\Model\Order\ItemFactory $orderItemFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        ResultFactory $resultFactory,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory,
        EventManager $eventManager,
        \Commercers\OrderApproval\Model\DataFactory $dataFactory,
        \Magento\Sales\Model\Order $order,
        \Commercers\OrderApproval\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        StoreManagerInterface $storeManager,
        Context $context,
        PageFactory $pageFactory,
        \Magento\Framework\Registry $registry
    ){
        $this->_helperEmailFactory = $helperEmailFactory;
        $this->_supervisorFactory = $supervisorFactory;
        $this->_orderItemFactory = $orderItemFactory;
        $this->_orderFactory = $orderFactory;
        $this->_coreSession = $coreSession;
        $this->_resultFactory = $resultFactory;
        $this->_forwardFactory = $forwardFactory;
        $this->eventManager = $eventManager;
        $this->_dataFactory = $dataFactory;
        $this->_storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        $this->_registry = $registry;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->_order = $order;
        return parent::__construct($context);
    }

    public function changeItemStatus($action = null)
    {
        if ($action == null){
            return;
        }
        $result = [];
        $getParams = $this->getRequest()->getParams();
        $code = $getParams["approval_code"];
        $supervisorName = $getParams["supervisor_name"];
        if ($code) {
            //find item need to be approve by code and email
            $dataModel = $this->_dataFactory->create();
            $approvals = $dataModel->getCollection()
                ->addFieldToFilter('approval_code', array('eq' => $code));
//                ->addFieldToFilter('approval_email', array('eq' => $email));
            $approval = $approvals->getFirstItem();
            if ($approvals->getSize()) {
                // Action = approve
                if ($action == 'approve') {
                    if ($approval->getStatus() != $dataModel::ORDER_APPROVAL_STATUS_INIT) {
                        $result['approved'] = 2;
                        $result['approval_code'] = $code;
                    } else {
                        $result['approved'] = 1;
                        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                        $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_APPROVED);
                        $approval->save();
                        $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                            'order_approval' => $approval,
                            'supervisor_name' => $supervisorName
                        ]);
                    }
                }
//              Action = disapprove
                if ($action == 'disapprove') {
                    if ($approval->getStatus() != $dataModel::ORDER_APPROVAL_STATUS_INIT) {
                        $result['disapproved'] = 2;
                        $result['approval_code'] = $code;
                    } else {
                        $orderId = $approval->getOrderId();
                        $order = $this->_order->load($orderId);
                        if(intval($order->getTotalItemCount() == 1) && $order->getState() == \Magento\Sales\Model\Order::STATE_NEW){
                            $result['approve_all'] = 6;
                            $result['disapproved'] = 1;
                            $result['approval_code'] = $approval->getApprovalCode();
                            $result['status'] = $dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED;
                            $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                            $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                            $approval->save();
                            try {
                                $order->cancel()->save();
                                return $this->redirectResult($result);
                            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                if ($this->_objectManager->get('Magento\Checkout\Model\Session')->getUseNotice(true)) {
                                    $this->messageManager->addNotice($e->getMessage());
                                } else {
                                    $this->messageManager->addError($e->getMessage());
                                }
                            }
                        }
                        $result['disapproved'] = 1;
                        $approval->setLastSendReminderAt(date('Y-m-d H:i:s'));
                        $approval->setStatus($dataModel::ORDER_APPROVAL_STATUS_DISAPPROVED);
                        $approval->save();
                        $this->eventManager->dispatch('commercers_orderapproval_change_status', [
                            'order_approval' => $approval,
                            'supervisor_name' => $supervisorName
                        ]);
                    }
                }
            } else {
                $result['approved'] = 4;
            }
            $result['approval_code'] = $code;
        }
        return $this->redirectResult($result);
    }

    public function redirectResult($result)
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $resultRedirect->setPath('orderapproval/index/result',$result);
        return $resultRedirect;
    }
}
