<?php
namespace Commercers\OrderApproval\Controller\Index;

use Commercers\OrderApproval\Controller\Index\AbstractIndex;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class CheckEmail extends Action {

    protected $_createOrderAfter;
    public function __construct(
        Context $context,
      \Commercers\OrderApproval\Observer\CreateOrderAfter $createOrderAfter
    )
    {
        $this->_createOrderAfter = $createOrderAfter;
        return parent::__construct($context);
    }
        public function execute()
    {
        $this->_createOrderAfter->callMe();
        return;
    }
}
