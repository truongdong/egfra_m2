<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\MultiOrderCheckout\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Commercers\MultiOrderCheckout\Model\Checkout\Type\Multishipping;

/**
 * Multishipping checkout success controller.
 */
class Success extends Action
{
    /**
     * @var Multishipping
     */
    private $multishipping;

    /**
     * @param Context $context
     * @param State $state
     * @param Multishipping $multishipping
     */
    public function __construct(
        Context $context,
        Multishipping $multishipping
    ) {
        $this->multishipping = $multishipping;

        parent::__construct($context);
    }

    /**
     * Multishipping checkout success page
     *
     * @return void
     */
    public function execute()
    {
	    $ids = $this->multishipping->getOrderIds();
        if (empty($ids)) {
            $this->_redirect('multiorder/checkout/index');
            return;
        }
        $this->_view->loadLayout();
        $ids = $this->multishipping->getOrderIds();
        $this->_eventManager->dispatch('commercers_orderapproval_after_save_order_multishipping', ['order_ids' => $ids]);
        $this->_eventManager->dispatch('multishipping_checkout_controller_success_action', ['order_ids' => $ids]);
        $this->_view->renderLayout();
    }
}
