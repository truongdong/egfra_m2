<?php

namespace Commercers\OrderApproval\Block\Adminhtml\Rule\Customer;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory as GroupCollectionFactory;
use Magento\Framework\App\ResourceConnection;

class Grid extends Extended
{
    /**
     * @var ResourceConnection
     */

    protected $_resource;

    /**
     * @var CustomerCollectionFactory
     */
    public $_customerCollectionFactory;

    /**
     * @var GroupCollectionFactory
     */
    public $_groupCollectionFactory;

    /**
     * @var Registry
     */
    public $coreRegistry;

    protected $_request;

    protected $supervisorCustomerCollectionFactory;

    public function __construct(
        \Commercers\OrderApproval\Model\ResourceModel\Supervisor\Customer\CollectionFactory $supervisorCustomerCollectionFactory,
        ResourceConnection $resource,
        \Magento\Framework\App\Request\Http $request,
        Context $context,
        Registry $coreRegistry,
        Data $backendHelper,
        CustomerCollectionFactory $customerCollectionFactory,
        GroupCollectionFactory $groupCollectionFactory,
        array $data = []
    ) {
        $this->supervisorCustomerCollectionFactory = $supervisorCustomerCollectionFactory;
        $this->_resource = $resource;
        $this->_request = $request;
        $this->coreRegistry = $coreRegistry;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_groupCollectionFactory = $groupCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $idx = $this->getRequest()->getParam('idx',0);
        $this->setId('commercers_orderapproval_rule_customer_grid_'.$idx);
        $supervisorId = $this->getRequest()->getPost('supervisor_id', null);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
//        $this->setTemplate('Commercers_OrderApproval::rule/grid_customers_data.phtml');
    }

    protected function _prepareCollection()
    {
        // $orderapproval = $this->getOrderApproval();
        $collection = $this->_customerCollectionFactory->create()
            ->addNameToSelect()
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('created_at')
            ->addAttributeToSelect('group_id');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_customers', array(
            'type'      => 'checkbox',
            'values'    => $this->getSelectedCustomers(),
            'align'     => 'center',
            'width'     => '50',
            'index'     => 'entity_id',
            'header_css_class' => 'col-select',
            'column_css_class' => 'col-select'
        ));

        $this->addColumn('entity_id', array(
            'header'    => __('ID'),
            'sortable'  => true,
            'width'     => '60px',
            'index'     => 'entity_id',
            'type'             => 'number',
            'header_css_class' => 'col-customer-id'
        ));

        $this->addColumn('name', array(
            'header'    => __('Name'),
            'index'     => 'name',
            'header_css_class' => 'col-customer-name'
        ));

        $this->addColumn('email', array(
            'header'    => __('Email'),
            'width'     => '150',
            'index'     => 'email',
            'header_css_class' => 'col-customer-email'
        ));


        $groups =  $this->_groupCollectionFactory->create()
            ->addFieldToFilter('customer_group_id', array('gt' => 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  __('Group'),
            'width'     =>  '100',
            'index'     =>  'group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
            'header_css_class' => 'col-customer-group'
        ));
        return $this;
    }
    /**
     * @return string
     */
    public function getRowUrl($item)
    {
        return '#';
    }
    public function getApprovalSupervisor()
    {
        return $this->coreRegistry->registry('approval_supervisor_data');
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        $idx = $this->getRequest()->getParam('idx',0);
        return $this->getUrl('*/*/customersgrid', ['idx' => $idx]);
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function getSelectedCustomers()
    {
        $idx = $this->getRequest()->getParam('idx',0);
        $supervisorId = $this->getRequest()->getParam('supervisor_id');
        $arrCustomerIds = [];
        if(isset($supervisorId)){
            $customerIds = $this->getSelectedRuleCustomers($supervisorId);
            if($customerIds){
                foreach ($customerIds as $customerId){
                    array_push($arrCustomerIds,$customerId['customer_id']);
                }
            }
        }
        $customers = $this->getRequest()->getPost('customers_grid_value_'.$idx,$arrCustomerIds);
//        $customers = $this->getRequest()->getPost('customers_grid_value_'.$idx,[]);
        return $customers;
    }

    /**
     * Retrieve grouped products
     *
     * @return array
     */
    public function getSelectedRuleCustomers($supervisorId)
    {
        //return false;
        $connection= $this->_resource->getConnection();
        $customerTable = $this->_resource->getTableName('commercers_order_approval_supervisor_customer');
        $sql = "SELECT main_table.customer_id FROM $customerTable AS main_table WHERE supervisor_id=$supervisorId";
        $result = $connection->query($sql);
        return $result;
    }

    public function getOrderApprovalRule()
    {
        return $this->coreRegistry->registry('orderapproval_rule');
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in customer flag
        if ($column->getId() == 'in_customers') {
            $customerIds = $this->getSelectedCustomers();
            if (empty($customerIds)) {
                $customerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id',['in' => $customerIds] );
            }
            else {
                $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $customerIds]);
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

}
