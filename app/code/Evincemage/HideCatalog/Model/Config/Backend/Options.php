<?php

namespace Evincemage\HideCatalog\Model\Config\Backend;

class Options extends \Magento\Framework\App\Config\Value
{
    public function __construct(
    \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Evincemage\HideCatalog\Helper\Data $helper,
        array $data = []    
    
    ) {
        $this->helper = $helper;         
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }
    public function beforeSave()
    {
        $value = $this->getValue();
        if (is_string($value)) {
            $value = explode(',', $value);
        }
        if (is_array($value) && 1 < count($value)) {
            // if USE_NONE is selected remove all other selected groups
            if (in_array(\Evincemage\HideCatalog\Helper\Data::USE_NONE, $value)) {
                $value = array(\Evincemage\HideCatalog\Helper\Data::USE_NONE);
                $this->setValue($value);
            }
        }

        parent::beforeSave();
    }
}