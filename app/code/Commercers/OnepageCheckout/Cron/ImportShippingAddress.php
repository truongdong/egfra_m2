<?php

namespace Commercers\OnepageCheckout\Cron;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\ManagerInterface as EventManager;
class ImportShippingAddress
{
    const XML_TIME_DAILY = 1;
    const XML_TIME_WEEK = 7;
    const XML_TIME_MONTH = 30;
    const XML_IMPORT_SHIPPING_ADDRESS = 'onepagecheckout/shipping_address/import';
    const XML_IS_ENABLE_OR_DISABLE_IMPORT = 'onepagecheckout/shipping_address/yesno_import';
    const XML_FILE_PATH_IMPORT = 'onepagecheckout/shipping_address/file_path';
    protected $_pageFactory;
    protected $_scopeConfig;
    protected $_shippingAddressFactory;
    public function __construct(
        \Commercers\OnepageCheckout\Model\ShippingAddressFactory $shippingAddressFactory,
        EventManager $eventManager,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_shippingAddressFactory = $shippingAddressFactory;
        $this->eventManager = $eventManager;
        $this->resultFactory = $resultFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_pageFactory = $pageFactory;
    }
    public function execute()
    {
        if (!$this->_scopeConfig->getValue(self::XML_IS_ENABLE_OR_DISABLE_IMPORT)){
            return;
        }
        $filePath = $this->_scopeConfig->getValue(self::XML_FILE_PATH_IMPORT);
        $csvData = $this->readCsvData($filePath);
        foreach ($csvData as $key => $data) {
            $shippingAddressModel = $this->_shippingAddressFactory->create();
            $shippingAddressModel->addData($data);
            $shippingAddressModel->save();
        }
        return;
    }
    function readCsvData($filePath){
        $file = fopen($filePath,"r");
        while(!feof($file)){
            $csv[] = fgetcsv($file, 0, ',');
        }
        $keys = array_shift($csv);
        foreach ($csv as $data){
            if(is_array($data)){
                $returnValue[] = array_combine($keys,$data);
            }
        }
        if(isset($returnValue))
            return $returnValue;
        return false;
    }
}

