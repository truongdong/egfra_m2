<?php

namespace Commercers\OnepageCheckout\Model\Source;

class DelayDays extends \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend
{
    protected $_options = null;

    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label' => __('Daily')),
            array('value' => 7, 'label' => __('Week')),
            array('value' => 30, 'label' => __('Month')),
        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }
}
