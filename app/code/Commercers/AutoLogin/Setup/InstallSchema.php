<?php

namespace Commercers\AutoLogin\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context) {
        $setup->startSetup();
        $conn = $setup->getConnection();
        $tableNameProfilers = $setup->getTable('commercers_auto_login');
        $context->getVersion();

        if($conn->isTableExists($tableNameProfilers) != true){
            $table = $setup->getConnection()
                ->newTable($setup->getTable('commercers_auto_login'))
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['identity' => true, 'unsigned' => true, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'main_account_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['identity' => true, 'unsigned' => true, 'primary' => true],
                    'Main Account ID'
                )
                ->addColumn(
                    'sub_account_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['identity' => true, 'unsigned' => true, 'primary' => true],
                    'Sub Account ID'
                )
                ->addColumn(
                    'token',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Auto Login Token'
                )

                ->setComment("commercers_auto_login");
            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}